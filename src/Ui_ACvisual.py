# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ACvisual.ui'
#
# Created: Sun Jul  9 15:43:47 2017
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!
from Ui_PolicyComposer import Ui_PolicyComposer
from Ui_PolicyRatifier import Ui_PolicyRatifier
from PyQt4 import QtCore, QtGui, QtGui
from PyQt4.QtGui import *
from policyIO import rbac_role_res_mat

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

# class Ui_addRoleDialog(QDialog):
#     
#     def __init__(self, parent=None):
#         QDialog.__init__(self, parent)
#         self.resize(300, 100)
#         layout = QVBoxLayout(self)
#         label_4 = QLabel('Enter a role name:')
#         layout.addWidget(label_4)
#         self.addRoleLineEdit = QLineEdit()
#         layout.addWidget(self.addRoleLineEdit)
#         self.addRoleBtn = QPushButton('Add')
#         layout.addWidget(self.addRoleBtn)
#         self.setLayout(layout)
# 
# class Ui_delRoleDialog(QDialog):
#     def __init__(self, main, parent=None):
#         QDialog.__init__(self, parent)
#         self.resize(300, 100)
#         layout = QVBoxLayout(self)
#         label_4 = QLabel('Roles:')
#         layout.addWidget(label_4)
#         self.delRoleCB = QComboBox()
#         layout.addWidget(self.delRoleCB)
#         self.delRoleBtn = QPushButton('Delete')
#         layout.addWidget(self.delRoleBtn)
#         self.setLayout(layout)
#         
# class Ui_assignUser2RoleDialog(QDialog):
#     
#     def __init__(self, main, parent=None):
#         QDialog.__init__(self, parent)
#         self.resize(300, 100)
#         layout = QGridLayout()
#         lab = QLabel('User')
#         layout.addWidget(lab, 0,0,1,1)
#         self.userCB = QComboBox()
#         layout.addWidget(lab, 1,0,1,1)
#         label_4 = QLabel('Role:')
#         layout.addWidget(label_4, 0,1,1,1)
#         self.roleCB = QComboBox()
#         layout.addWidget(self.roleCB, 1,1,1,1)
#         self.assignBtn = QPushButton('Assign')
#         layout.addWidget(self.assignBtn, 2,0,1,2)
#         self.setLayout(layout)
    
# class Ui_addRoleSODDialog(QDialog):
#     
#     def __init__(self, main, parent=None):
#         QDialog.__init__(self, parent)
#         self.resize(300, 100)
#         layout = QGridLayout()
#         lab = QLabel('Exclusive Roles')
#         layout.addWidget(lab, 0,0,1,1)
#         self.SODTextEdit = QTextEdit()
#         layout.addWidget(self.SODTextEdit, 1,0,1,1)
#         lab = QLabel('Use one line for a group of roles')
#         lab1 = QLabel('Use "," to separate exclusive roles within a group')
#         layout.addWidget(lab,2,0,1,1)
#         layout.addWidget(lab1,3,0,1,1)
#         self.addSODBtn = QPushButton('Update')
#         layout.addWidget(self.addSODBtn, 4,0,1,1)
#         self.setLayout(layout)
#         
# class Ui_modifRoleInheritanceDialog(QDialog):
#     
#     def __init__(self, main, parent=None):
#         QDialog.__init__(self, parent)
#         self.resize(300, 100)
#         layout = QGridLayout()
#         lab = QLabel('Role')
#         layout.addWidget(lab, 0,0,1,1)
#         self.rcomboBox = QComboBox()
#         layout.addWidget(self.rcomboBox, 1,0,1,1)
#         lab = QLabel('Parent Roles')
#         layout.addWidget(lab, 2,0,1,1)
#         self.parentsTexEdit = QTextEdit()
#         layout.addWidget(self.parentsTexEdit, 3,0,1,1)
#         lab = QLabel('Children Roles')
#         layout.addWidget(lab, 4,0,1,1)
#         self.childrenTexEdit = QTextEdit()
#         layout.addWidget(self.childrenTexEdit, 5,0,1,1)
#         self.confirmInheritanceBtn = QPushButton('OK')
#         layout.addWidget(self.confirmInheritanceBtn, 6,0,1,1)
#         self.setLayout(layout)
#         
#     def updateRoleList(self):
#         self.rcomboBox.clear()
#         for r in rbac_role_res_mat.keys():
#             self.rcomboBox.additem(r)
        
class Ui_MainWindow(object):
        
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1126, 847)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.policyComposerWidget = Ui_PolicyComposer(QtCore.Qt.Horizontal, self.centralwidget, MainWindow)
        self.policyRatifierWidget = Ui_PolicyRatifier(QtCore.Qt.Vertical, self.centralwidget, MainWindow)
        
        '''MENUBAR'''
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1126, 27))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.menu_File = QtGui.QMenu(self.menubar)
        self.menu_File.setObjectName(_fromUtf8("menu_File"))
        self.menu_Mode = QtGui.QMenu(self.menubar)
        self.menu_Mode.setObjectName(_fromUtf8("menu_Mode"))
        self.menu_Settings = QtGui.QMenu(self.menubar)
        self.menu_Settings.setObjectName(_fromUtf8("menu_Settings"))
        self.menu_Help = QtGui.QMenu(self.menubar)
        self.menu_Help.setObjectName(_fromUtf8("menu_Help"))
        MainWindow.setMenuBar(self.menubar)
        '''STATUSBAR'''
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        
        '''TOOLBAR'''
        self.toolBar = QtGui.QToolBar(MainWindow)
        self.toolBar.setObjectName(_fromUtf8("toolBar"))
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        
        '''IO'''
        self.actionNew = QtGui.QAction(MainWindow)
        self.actionNew.setObjectName(_fromUtf8("actionNew"))
        self.actionNew.setToolTip("Create a blank policy")
        
        self.actionImport = QtGui.QAction(MainWindow)
        self.actionImport.setObjectName(_fromUtf8("actionImport"))
        self.actionImport.setToolTip("Import a specification file")
        
        self.actionExport = QtGui.QAction(MainWindow)
        self.actionExport.setObjectName(_fromUtf8("actionExport"))
        self.actionExport.setToolTip("Export as a specification file")

        '''MALLECIOUS'''
        self.actionExit = QtGui.QAction(MainWindow)
        self.actionExit.setObjectName(_fromUtf8("actionExit"))
        
        self.actionHelp_Contents = QtGui.QAction(MainWindow)
        self.actionHelp_Contents.setObjectName(_fromUtf8("actionHelp_Contents"))
        
        '''VIEWS and GRAPHS'''
        self.actionPolicy_Composer = QtGui.QAction(MainWindow)
        self.actionPolicy_Composer.setObjectName(_fromUtf8("actionPolicy_Composer"))
        self.actionPolicy_Composer.setCheckable(True)
        self.actionPolicy_Composer.setChecked(True)
        self.actionPolicy_Composer.setObjectName(_fromUtf8("actionPolicy_Composer"))
        self.actionPolicy_Composer.setToolTip("Policy writing mode")
        
        self.actionPolicy_DetailChecker = QtGui.QAction(MainWindow)
        self.actionPolicy_DetailChecker.setCheckable(True)
        self.actionPolicy_DetailChecker.setObjectName(_fromUtf8("actionPolicy_DetailChecker"))
        self.actionPolicy_DetailChecker.setToolTip("Policy model detail mode")
        
        self.actionPolicy_Ratifier = QtGui.QAction(MainWindow)
        self.actionPolicy_Ratifier.setCheckable(True)
        self.actionPolicy_Ratifier.setObjectName(_fromUtf8("actionPolicy_Ratifier"))
        self.actionPolicy_Ratifier.setToolTip("Policy ratification mode")
 
        self.actionPolicy_VisAnalyzer = QtGui.QAction(MainWindow)
        self.actionPolicy_VisAnalyzer.setCheckable(True)
        self.actionPolicy_VisAnalyzer.setObjectName(_fromUtf8("actionPolicy_VisAnalyzer"))
        self.actionPolicy_VisAnalyzer.setToolTip("Policy analysis with visualization")
        
        self.actionZoomIn = QtGui.QAction(MainWindow)
        self.actionZoomIn.setObjectName(_fromUtf8("actionZoomIn"))
        
        self.actionZoomOut = QtGui.QAction(MainWindow)
        self.actionZoomOut.setObjectName(_fromUtf8("actionZoomOut"))

        self.actionPan = QtGui.QAction(MainWindow)
        self.actionPan.setObjectName(_fromUtf8("actionPan"))
        '''role View Context Menu Callback Dialogs'''
#         self.addRoleDialog = Ui_addRoleDialog(MainWindow)
#         self.delRoleDialog = Ui_delRoleDialog(MainWindow)
#         self.assignUser2RoleDialog = Ui_assignUser2RoleDialog(MainWindow)
#         self.addRoleSODDialog = Ui_addRoleSODDialog(MainWindow)
#         self.modifRoleInheritanceDialog = Ui_modifRoleInheritanceDialog(MainWindow)
        '''SPEC'''
        self.actionSpecification = QtGui.QAction(MainWindow)
        self.actionSpecification.setObjectName(_fromUtf8("actionSpecification"))
        self.actionSpecification.setToolTip("Show the specification file")
        '''Change Root Directory'''
        self.actionChangeRoot= QtGui.QAction(MainWindow)
        self.actionChangeRoot.setObjectName(_fromUtf8("actionChangeRoot"))
        self.actionChangeRoot.setToolTip("Change the root directory")
        
        '''add actions to menubar'''
        self.menu_File.addAction(self.actionNew)
        self.menu_File.addAction(self.actionImport)
        self.menu_File.addAction(self.actionExit)
        
        self.menu_Mode.addAction(self.actionPolicy_Composer)
        self.menu_Mode.addAction(self.actionPolicy_DetailChecker)
        self.menu_Mode.addAction(self.actionPolicy_Ratifier)
        self.menu_Mode.addAction(self.actionPolicy_VisAnalyzer)

        self.menu_Mode.addSeparator()
        self.menu_Mode.addAction(self.actionSpecification)
        self.menu_Mode.addSeparator()
        
        self.menu_Help.addAction(self.actionHelp_Contents)
        
        self.menubar.addAction(self.menu_File.menuAction())
        self.menubar.addAction(self.menu_Mode.menuAction())
        self.menubar.addAction(self.menu_Help.menuAction())
        
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.toolBar.setWindowTitle(_translate("MainWindow", "toolBar", None))

        self.menu_File.setTitle(QtGui.QApplication.translate("MainWindow", "&File", None))
        self.menu_Mode.setTitle(QtGui.QApplication.translate("MainWindow", "&Mode", None))
        self.menu_Help.setTitle(QtGui.QApplication.translate("MainWindow", "&Help", None))
        
        self.actionHelp_Contents.setText(QtGui.QApplication.translate("MainWindow", "Help Contents", None))
        self.actionHelp_Contents.setShortcut(QtGui.QApplication.translate("MainWindow", "F1", None))
        
        self.actionExit.setText(QtGui.QApplication.translate("MainWindow", "Exit", None))
        self.actionExit.setShortcut(QtGui.QApplication.translate("MainWindow", "Ctrl+Q", None))
        
        self.actionNew.setText(QtGui.QApplication.translate("MainWindow", "New", None))
        self.actionImport.setText(QtGui.QApplication.translate("MainWindow", "Import", None))
        self.actionExport.setText(QtGui.QApplication.translate("MainWindow", "Export", None))
        
#         self.actionPolicy_DetailChecker.setText(QtGui.QApplication.translate("MainWindow", "Policy Detail Checker", None))
        self.actionPolicy_Ratifier.setText(QtGui.QApplication.translate("MainWindow", "Policy Ratifier", None))
        self.actionPolicy_VisAnalyzer.setText(QtGui.QApplication.translate("MainWindow", "Policy Analyzer", None))
        
        self.actionZoomIn.setText(QtGui.QApplication.translate("MainWindow", "Zoom In", None))
        self.actionZoomOut.setText(QtGui.QApplication.translate("MainWindow", "Zoom Out", None))
        self.actionPan.setText(QtGui.QApplication.translate("MainWindow", "Pan", None))
        
        self.actionSpecification.setText(QtGui.QApplication.translate("MainWindow", "Specification", None))
        self.actionChangeRoot.setText(QtGui.QApplication.translate("MainWindow", "Change Root Directory", None))
        self.actionPolicy_Composer.setText(QtGui.QApplication.translate("MainWindow", "Policy Composer", None))
