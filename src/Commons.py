'''
Created on Jul 23, 2018

@author: manw
'''
from PyQt4.QtGui import *
from PyQt4.QtCore import QRect, Qt
from PyQt4.QtGui import QFont
import Commons

def listIntersec2(l1, l2):
    return set(l1).intersection(l2)

'''---------------------GUI---------------------'''
def groupBoxTitle(group, fontsize = 15):
    font = QFont()
    font.setPointSize(fontsize)
    group.setFont(font)
    group.setAlignment(Qt.AlignHCenter)
        
def listAddDelGroup(labelTex, col, plyGridLayout, isBrowseDlg=False):
    group = QGroupBox(labelTex)
    Commons.groupBoxTitle(group)
    gridLayout = QGridLayout(group)
    listw = QListWidget(group)
    gridLayout.addWidget(listw, 0, 0, 1, 2)
    if isBrowseDlg:
        pbtn_1 = QPushButton("Browse...", group)
    else:
        pbtn_1 = QPushButton("Add", group)
    pbtn_del = QPushButton("Delete", group)
    gridLayout.addWidget(pbtn_1, 1, 0, 1, 1)
    gridLayout.addWidget(pbtn_del, 1, 1, 1, 1)
    gridLayout.setContentsMargins(0,0,0,0)
    plyGridLayout.addWidget(group, 0,col,1,1)
    return listw, pbtn_1, pbtn_del 

'''---------------------------------------------'''
