'''
Created on Apr 16, 2015

@author: manwang
'''
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'greetingWin.ui'
#
# Created: Fri Feb 28 02:57:19 2014
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Greeting(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(560, 300)
        self.verticalLayoutWidget = QtGui.QWidget(Dialog)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 540, 281))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
#         self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label_4 = QtGui.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("NanumGothic"))
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout.addWidget(self.label_4)
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label.setFont(font)
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        font.setPointSize(13)
        self.label_2 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout.addWidget(self.label_2)
        self.label_3 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.verticalLayout.addWidget(self.label_3)
#         self.label_5 = QtGui.QLabel(self.verticalLayoutWidget)
#         self.label_5.setAlignment(QtCore.Qt.AlignCenter)
#         self.label_5.setObjectName(_fromUtf8("label_5"))
#         self.verticalLayout.addWidget(self.label_5)
        self.label_2.setFont(font)
        self.label_3.setFont(font)
#         self.label_5.setFont(font)
        self.pushButton = QtGui.QPushButton(self.verticalLayoutWidget)
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.verticalLayout.addWidget(self.pushButton)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        
    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "", None))
        self.label_4.setText(QtGui.QApplication.translate("Dialog", "ACvisual v1.0", None))
        self.label.setText(QtGui.QApplication.translate("Dialog", "Michigan Technological University", None))
#         self.label.setText(QtGui.QApplication.translate("Dialog", "Accesssible Access Control v1.0", None))
        self.label_2.setText(QtGui.QApplication.translate("Dialog", "Developer:\n"
"Man Wang", None))
        self.label_3.setText(QtGui.QApplication.translate("Dialog", "Advisors:\n"
"Dr. Jean Mayo, Dr. Ching-Kuang Shene, Dr. Chaoli Wang and Dr. Steven Carr", None))
#         self.label_5.setText(QtGui.QApplication.translate("Dialog", "Michigan Technological University", None))
        self.pushButton.setText(QtGui.QApplication.translate("Dialog", "OK", None))
        
