'''
Created on Jul 4, 2018

@author: manw
'''
from grandalf.graphs import Vertex, Graph
from RoleNodeView import RoleNodeView

class RoleNode(Vertex):
    def __init__(self, role, scene, main):
        Vertex.__init__(self, role)
        self.view = RoleNodeView(self, scene, main)
        self.main = main
        self.scene = scene
        self.parents = set()
        self.children = set()
        self.edges = {} # (end node: edge item <EdgeView>) pairs
  
class RoleNodeHier(Graph):
    def __init__(self, main, V=None, E=None, directed=True):
        Graph.__init__(self, V, E, directed)
        self.main = main
        self.cycles = []
        self.sug = None
        self.data2RoleNode = {}
    
#     def DFS(self, node):
#         self.seenStack.push(node)
#           
#         for c in node.children:
#             if not c.visited:
#                 self.DFS(c)
#         
#         if node.id == node.low:
#             top = self.seenStack.top()
#             top.onStack = False
#             top.low = node.id
#             self.seenStack.pop()
#             scc.append(top)
#             while top!=node:
#                 top = self.seenStack.top()
#                 self.seenStack.pop()
#                 scc.append(top)
#                 top.onStack = False
#                 top.low = node.id
#             if len(scc)>1:
#                 self.cycle.append(scc)

    def findAllChildren_BFS(self, rn):
        for c in self.cycles:
            if rn in c:
                return c
        toVisit = [rn]
        visited = set()
        while toVisit:
            curr = toVisit.pop(0)
            if curr not in visited:
                toVisit += curr.children
                visited.add(curr)
        visited = visited - set([rn])
        return visited
            
    def findAllParents_BFS(self, rn):
        for c in self.cycles:
            if rn in c:
                return c
        toVisit = [rn]
        visited = set()
        while toVisit:
            curr = toVisit.pop(0)
            if curr not in visited:
                visited.add(curr)
                toVisit += curr.parents
        visited -= set([rn])
        return visited
                
