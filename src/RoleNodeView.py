'''
Created on Jul 15, 2018

@author: manw
'''
from PyQt4.QtGui import QGraphicsEllipseItem, QGraphicsItem
from PyQt4.QtGui import QColor, QPen, QPainter, QFont
from PyQt4.QtCore import Qt, QRectF, QPointF, QRect
import copy, PolicyContent
import policyUOAIO, PermissionChecker

def multiply(color, ratio):
        r,g,b = color.red(), color.green(), color.blue()
        return QColor(max(0, min(255, r*ratio)), \
                      max(0, min(255, g*ratio)), \
                      max(0, min(255, b*ratio)))
ROLE_OUTLINE_CONTENT_RATIO = 0.9
ROLE_INH_ORG_RATIO = 1.3
ROLE_COLOR = QColor(186, 186, 186)
#for original, use id to choose the color for highlight: 0: for both users, 1: for user 1, and 2: for user 2.
ROLE_USER_ORG = [QColor(102,194,165), QColor(252,141,98), QColor(0, 153, 255)]#QColor(141,160,203)]
#QColor(251, 180, 174)
# ROLE_USER_INH = [QColor(204, 235, 197), QColor(253, 205, 172), QColor(203, 213, 222)] 
ROLE_USER_INH = [multiply(ROLE_USER_ORG[0], ROLE_INH_ORG_RATIO), \
                multiply(ROLE_USER_ORG[1], ROLE_INH_ORG_RATIO), \
                multiply(ROLE_USER_ORG[2], ROLE_INH_ORG_RATIO)]

class RoleNodeView(QGraphicsEllipseItem):
    NODESIZE = 24
    NODE_BOUNDARY_SIZE = NODESIZE+3

    def __init__(self, rolenode, scene, main):
        QGraphicsEllipseItem.__init__(self)
        self.rectContent = QRectF(-0.5*self.NODESIZE, -0.5*self.NODESIZE, self.NODESIZE, self.NODESIZE)
        self.rectbound = QRectF(-0.5*self.NODE_BOUNDARY_SIZE, -0.5*self.NODE_BOUNDARY_SIZE, self.NODE_BOUNDARY_SIZE, self.NODE_BOUNDARY_SIZE)
        self.setRect(self.rectContent)
        
        self.main = main
        self.rnode = rolenode
        self.name = rolenode.data
        '''
        if 0, then not used to access target node; 
        if 1, then it means that the this role have the set permission to the target node.
        '''
        self.roleAccess = 0 # red frame
        self.roleId = 1# directly assigned: 0; inherited: 1
        
        self.scene = scene
        self.highlighted = False
        self.inhHighlighted = False
        
        self.roleInhHighlighted = False
        self.xorColorIndex = -1
        
        self.xy = [0,0]
        self.w, self.h = self.NODESIZE, self.NODESIZE
        self.relativeX, self.relativeY = 0, 0
        # parents and children are stored in the RoleNode class
        '''For Visualization Animation'''
        self.visited = False

        self.label = self.name
        self.font = QFont("Calibri")
        self.font.setPixelSize(self.main.FONT_SIZE)
        self.pen = QPen()
        self.setFlag(QGraphicsItem.ItemIsMovable, True)
        self.setFlag(QGraphicsItem.ItemIsSelectable, True)
        self.setVisible(True)
        self.setToolTip(self.name)
       
    def setPos(self, rx, ry):
        self.relativeX, self.relativeY = rx, ry
        self.xy = [self.scene.roleHierX + rx*self.scene.roleHierW, \
                   self.scene.roleHierY + ry*self.scene.roleHierH]
        QGraphicsEllipseItem.setPos(self, QPointF(self.xy[0],self.xy[1]))
        
    def getRolePermsForTextEdit(self):
        existingPerm = {}
        direc_rolePerms = PermissionChecker.retrieveRBACPermInfo('', self.name, True)
        for d in direc_rolePerms:
            if d.obj not in existingPerm.keys():
                existingPerm[d.obj] = policyUOAIO.makepermset(d.permset, None) if d.recursive else policyUOAIO.makepermset(None, d.permset)
            else:
                perm = existingPerm[d.obj]
                if d.recursive:
                    existingPerm[d.obj] = policyUOAIO.makepermset(perm.recur_perms.union(d.permset), perm.nonrecur_perms)
                else:
                    existingPerm[d.obj] = policyUOAIO.makepermset(perm.recur_perms, perm.nonrecur_perms.union(d.permset))
        inh_rolePerms = []
        desc = self.scene.roleHier.findAllChildren_BFS(self.rnode)
        for d in desc:
            d.view.roleInhHighlighted = True
            aRolePerm = PermissionChecker.retrieveRBACPermInfo('', d.data, False)
            for d in aRolePerm:
                '''check if already covered'''
                permission = policyUOAIO.makepermset(d.permset, None) if d.recursive else policyUOAIO.makepermset(None, d.permset)
                newPerm = {d.obj: permission}
                if not policyUOAIO.isPermSubsetOfRole2(newPerm, existingPerm): #if not covered
                    existingPerm = policyUOAIO.combinePermsForOne(existingPerm, d.obj, permission)
                    inh_rolePerms.append(d)
        inputperm = {'':direc_rolePerms + inh_rolePerms}
        return inputperm
            
    def mousePressEvent(self, event):
        QGraphicsEllipseItem.mousePressEvent(self, event)
        if self.main.visAnimation.perspective != self.main.visAnimation.ROLEVIEW:
            if self.scene.clickedRoleNode: 
                self.scene.clickedRoleNode = None 
                self.main.visAnimation.resetRoleHighlight()
                self.main.visAnimation.resetRoleXorColorIndex()
            return
        self.main.visAnimation.resetRoleXorColorIndex()
        self.main.visAnimation.resetRoleHighlight()
        if self.scene.clickedRoleNode != self:
            self.scene.clickedRoleNode = self
            inputperm = self.getRolePermsForTextEdit()
            self.main.visualizerTextEdit.fillinPermDetail(inputperm, False)
        else:
            self.scene.clickedRoleNode = None
        self.scene.update()

    def paintBorder(self, painter):
        painter.setPen(QPen(Qt.red,2))
        r = self.boundingRect()
        painter.drawRect(QRect(r.x()-0.04*r.width(), r.y()-0.04*r.height(), 1.04*r.width(), 1.04*r.height()))
        
    def paint(self, painter, option, widget=None):
        '''draw frame'''
        if self.roleAccess:
            self.paintBorder(painter)
        '''
        draw outline
        '''
        if self.highlighted:
            self.pen.setColor(multiply(ROLE_USER_ORG[self.roleId], ROLE_OUTLINE_CONTENT_RATIO))
            self.setBrush(ROLE_USER_ORG[self.roleId])
#             self.pen.setColor(QColor(127,0, 0))
#             self.setBrush(QColor(255, 127, 127))
        elif self.inhHighlighted:
            self.pen.setColor(multiply(ROLE_USER_INH[self.roleId], ROLE_OUTLINE_CONTENT_RATIO))
            self.setBrush(ROLE_USER_INH[self.roleId])
#             self.pen.setColor(QColor(0,0,127))
#             self.setBrush(QColor(127, 127, 255))
        else:
            self.pen.setColor(multiply(ROLE_COLOR, ROLE_OUTLINE_CONTENT_RATIO))
            self.setBrush(ROLE_COLOR)
        
        if self.main.radioBtn_roleView.isChecked():
            if self.xorColorIndex>-1:
                color = self.main.visAnimation.XOR_ROLE_COLORS[self.xorColorIndex]
                self.pen.setColor(multiply(color, ROLE_OUTLINE_CONTENT_RATIO))
                self.setBrush(color)
        
        if self.scene.clickedRoleNode == self:
            self.pen.setColor(multiply(ROLE_USER_ORG[0], ROLE_OUTLINE_CONTENT_RATIO))
            self.setBrush(ROLE_USER_ORG[0])
        if self.roleInhHighlighted:
            self.pen.setColor(multiply(ROLE_USER_INH[0], ROLE_OUTLINE_CONTENT_RATIO))
            self.setBrush(ROLE_USER_INH[0])
            
        self.pen.setWidth(3.0)
        self.setPen(self.pen)
        painter.setRenderHint(QPainter.Antialiasing, True)
        QGraphicsEllipseItem.paint(self, painter, option, widget)
        '''
        display name as label
        '''
        painter.setPen(QPen(Qt.black))
        painter.setFont(self.font)
        self.labelRect = painter.fontMetrics().boundingRect(self.name)
        # left top
#         x = int(self.rect().bottomLeft().x()-(self.labelRect.width()))
#         y = int(self.rect().topLeft().y()-0.5*self.labelRect.height())
        # right top
        x = int(self.rect().bottomLeft().x()+0.3*self.labelRect.width())
        y = int(self.rect().topLeft().y()-self.labelRect.height())
        self.paintLabel(painter, x, y)
                
    def paintLabel(self, painter, x, y):
        rect = self.labelRect
        rect.moveTo(x, y)
        painter.drawText(rect, Qt.AlignLeft, self.label)
        
    def mouseMoveEvent(self, evt):
        QGraphicsEllipseItem.mouseMoveEvent(self, evt)
        rx=(evt.scenePos().x()-self.scene.roleHierX)/self.scene.roleHierW
        ry=(evt.scenePos().y()-self.scene.roleHierY)/self.scene.roleHierH
        self.setPos(rx, ry)
        self.scene.update()
        
    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemPositionChange:
            for edge in self.edgeList:
                edge.updatePosition()
        return value 
