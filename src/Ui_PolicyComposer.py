'''
Created on Oct 20, 2018

@author: manw
'''

from PyQt4.QtGui import *
from PyQt4.QtCore import QRect, Qt
from PyQt4.QtGui import QFont
import Commons
from PyQt4.Qt import QVBoxLayout, QGridLayout
from PolicyVerifier import PERMISSIONS
from policyParse import RECURSIVE
import PolicyContent
from policyUOAIO import makepermset, policyUOAIO

class Ui_PolicyComposer_RuleTable(QTableWidget):
    
    def __init__(self, main, rowCount=0, colCount=4, parent=None):
        QTableWidget.__init__(self, rowCount, colCount, parent)
        self.main = main
        labels = ['User', 'Object', 'Action', 'Recursive']
        self.setHorizontalHeaderLabels(labels)
        self.horizontalHeader().setResizeMode(1, QHeaderView.Stretch)
        self.setStyleSheet("QTableView{ selection-background-color: rgba(255, 0, 0, 50); selection-color: black}")
        self.setUpdatesEnabled(True)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)

    def delRule(self):
        rows = set()
        for i in self.selectedItems():
            rows.add(i.row())
        for r in rows:
            self.removeRow(r)
        self.main.policyChanged = True
        
    def delRuleOfUser(self, user):
        rows = set()
        for i in xrange(self.rowCount()):
            if str(self.item(i, 0).text())==user:
                rows.add(i)
        for r in rows:
            self.removeRow(r)
        self.main.policyChanged = True
            
    def delRuleOfObject(self, obj):
        rows = set()
        for i in xrange(self.rowCount()):
            if str(self.item(i, 1).text())==obj:
                rows.add(i)
        for r in rows:
            self.removeRow(r)
        self.main.policyChanged = True
            
class Ui_PoliComposer_AddSubject(QDialog):
    '''
    addSubject:
    - Enter a user name
    - Confirm
    '''
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.resize(300, 100)
        layout = QVBoxLayout(self)
        label_4 = QLabel('Enter a user name or a series of users separated by comma:')
        layout.addWidget(label_4)
        self.composer_addSubLineEdit = QLineEdit()
        layout.addWidget(self.composer_addSubLineEdit)
        self.composer_addSubBtn = QPushButton('Add')
        layout.addWidget(self.composer_addSubBtn)
        self.setLayout(layout)

class Ui_PoliComposer_AddObject(QDialog):
    '''
    addObject:
    - Enter path of a object
    - Confirm
    '''
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi()
        
    def setupUi(self):
        self.resize(300, 100)
        layout = QVBoxLayout(self)
        label_4 = QLabel('Enter an object path or a series of paths separated by comma:')
        layout.addWidget(label_4)
        self.composer_addObjLineEdit = QLineEdit()
        layout.addWidget(self.composer_addObjLineEdit)
        self.composer_addObjBtn = QPushButton('Add')
        layout.addWidget(self.composer_addObjBtn)
        self.setLayout(layout)

class Ui_PoliComposer_AddRule(QDialog):
    '''
    addObject:
    - Enter path of a object
    - Confirm
    '''
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi()
        
    def createComboBox(self, content=None):
        w = QComboBox()
        if content:
            w.addItems(content)
        return w

    def setupUi(self):
        self.resize(300, 100)
        layout = QGridLayout()
        groupSub = QGroupBox('User')
        self.sublist = self.createComboBox()
        l1 = QVBoxLayout()
        l1.addWidget(self.sublist)
        groupSub.setLayout(l1)
        layout.addWidget(groupSub, 0,0,1,1)
        
        groupObj = QGroupBox('Object')
        self.objlist = self.createComboBox()
        l2 = QVBoxLayout()
        l2.addWidget(self.objlist)
        groupObj.setLayout(l2)
        layout.addWidget(groupObj, 1,0,1,1)
        
        self.recurCheck = QCheckBox('Recursive')
        self.recurCheck.setCheckable(True)
        self.recurCheck.setChecked(False)
        layout.addWidget(self.recurCheck, 2,0,1,1)
        
        groupAct = QGroupBox('Action')
        ll = QGridLayout()
        ll.setContentsMargins(0, 0, 0, 0)
        
        g2 = QGroupBox()
        l2 = QVBoxLayout()
        self.cb_permR = QCheckBox('Read')
        self.cb_permW = QCheckBox('Write')
        self.cb_permX = QCheckBox('Execute')
        l2.addWidget(self.cb_permR)
        l2.addWidget(self.cb_permW)
        l2.addWidget(self.cb_permX)
        g2.setLayout(l2)
        ll.addWidget(g2, 0,0,1,1)
        groupAct.setLayout(ll)
        layout.addWidget(groupAct, 0,1,3,1)
        
        self.composer_addRuleBtn = QPushButton('Add')
        layout.addWidget(self.composer_addRuleBtn, 3,0,1,2)
        self.setLayout(layout)
        
    def retrieveRuleInfo(self):
        recursive = 'True' if self.recurCheck.isChecked() else 'False'
        permissions = set()
        if self.cb_permR.isChecked(): permissions.add('r')
        if self.cb_permW.isChecked(): permissions.add('w')
        if self.cb_permX.isChecked(): permissions.add('x')
        perms = ','.join(permissions)
        if not perms: return None
        user = str(self.sublist.currentText())
        obj = str(self.objlist.currentText())
        return [user, obj, perms, recursive]
        
    def clearInterface(self):
        self.sublist.clear()
        self.objlist.clear()
        
class Ui_PolicyComposer(QSplitter):

    def __init__(self, orientation, parent, main):
        QSplitter.__init__(self, orientation, parent)
        self.main = main
        self.setupUi()
        
    def setupUi(self):
        '''PolicyComposerWidget'''
        policyAttrWidget = QWidget()
        plyGridLayout = QGridLayout(policyAttrWidget)
        self.listSub, self.addSubBtn, self.delSubBtn = Commons.listAddDelGroup("Users", 0, plyGridLayout)
        self.listObj, self.addObjBtn, self.delObjBtn = Commons.listAddDelGroup("Objects", 1, plyGridLayout)
        attrLabel = QLabel("Rules <b>(Redundant Rules in Gray)</b>")
        plyGridLayout.addWidget(attrLabel, 1, 0, 1, 2)
        self.ruleTable = Ui_PolicyComposer_RuleTable(self.main)
        plyGridLayout.addWidget(self.ruleTable, 2,0,1,2)
        self.addRuleBtn, self.delRuleBtn = QPushButton('Add'), QPushButton('Delete')
        plyGridLayout.addWidget(self.addRuleBtn, 3,0,1,1)
        plyGridLayout.addWidget(self.delRuleBtn, 3,1,1,1)
        self.addWidget(policyAttrWidget)
        '''
        connect to the reaction widgets
        '''
        self.policycomp_addSub = Ui_PoliComposer_AddSubject()
        self.addSubBtn.clicked.connect(self.policycomp_addSub.show)
        self.policycomp_addObj = Ui_PoliComposer_AddObject()
        self.addObjBtn.clicked.connect(self.policycomp_addObj.show)
        self.policycomp_addRule = Ui_PoliComposer_AddRule()
        self.addRuleBtn.clicked.connect(self.policycomp_addRule.show)
        self.delSubBtn.clicked.connect(self.delSubject)
        self.delObjBtn.clicked.connect(self.delObject)
        self.delRuleBtn.clicked.connect(self.ruleTable.delRule)
        self.policycomp_addRule.composer_addRuleBtn.clicked.connect(self.addRule)
        self.policycomp_addSub.composer_addSubBtn.clicked.connect(self.addSubjects)
        self.policycomp_addObj.composer_addObjBtn.clicked.connect(self.addObjects)
        
    def clearInterface(self):
        self.listSub.clear()
        self.listObj.clear()
        self.ruleTable.model().removeRows(0, self.ruleTable.rowCount())
        self.policycomp_addRule.clearInterface()
        
    def addSubjects(self):
        import re
        subjects = str(self.policycomp_addSub.composer_addSubLineEdit.text())
        subjects = re.sub(r'\s+', '', subjects)
        subjects = subjects.split(',')
        for s in subjects:
            self.addSubject(s)
        self.policycomp_addSub.composer_addSubLineEdit.clear()
        self.policycomp_addSub.hide()
        
    def addSubject(self, s):
        self.main.policyChanged = True
        self.listSub.addItem(s)
        self.policycomp_addRule.sublist.addItem(s)
        
    def addObjects(self):
        import re
        obj = str(self.policycomp_addObj.composer_addObjLineEdit.text()).strip()
        obj = re.sub('\s+', '', obj)
        obj = obj.split(',')
        for s in obj:
            self.addObject(s)
        self.policycomp_addObj.composer_addObjLineEdit.clear()
        self.policycomp_addObj.hide()
        
    def addObject(self, o):
        self.main.policyChanged = True
        self.listObj.addItem(o)
        self.policycomp_addRule.objlist.addItem(o)
        self.main.addRolePermDialog.objlist.addItem(o)
        
    def addRule(self):
        '''
        elements of a rule: save in a list (subject, object, action, recursive)
                            each element is a string
        '''
        rule = self.policycomp_addRule.retrieveRuleInfo()
        if not rule:
            QMessageBox.warning(self.main, '', 'Please select at least one permission.')
            return
        self.main.policyChanged = True
        self.insertRule2Table(rule)
        self.policycomp_addRule.hide()
        
    def insertRule2Table(self, rule, bkcolor=None):
        row = self.ruleTable.rowCount() 
        col = 0
        self.ruleTable.insertRow(row)
        for e in rule:
            item = QTableWidgetItem(e)
            if bkcolor: item.setBackground(bkcolor)
            item.setTextAlignment(Qt.AlignCenter)
            self.ruleTable.setItem(row, col, item)
            col += 1
            
    def delSubject(self):
        if not self.listSub: return
        self.main.policyChanged = True
        s = str(self.listSub.currentItem().text())
        ruleSublist = self.policycomp_addRule.sublist
        ruleSublist.removeItem(ruleSublist.findText(s))
        item = self.listSub.takeItem(self.listSub.currentRow())
        del item
        self.ruleTable.delRuleOfUser(s)
        
    def delObject(self):
        if not self.listObj: return
        self.main.policyChanged = True
        ruleObjlist = self.policycomp_addRule.objlist
        obj = str(self.listObj.currentItem().text())
        ruleObjlist.removeItem(ruleObjlist.findText(obj))
        rolepermObjlist = self.main.addRolePermDialog.objlist
        rolepermObjlist.removeItem(rolepermObjlist.findText(obj))
        item  = self.listObj.takeItem(self.listObj.currentRow())
        del item
        self.ruleTable.delRuleOfObject(obj)
        
    def pushPolicyInfo2UI(self):
        for u in PolicyContent.uoa_user_set: self.addSubject(u)
        for o in PolicyContent.uoa_object_set: self.addObject(o)
        for r in PolicyContent.user_rule_list:
            arule = [r.user, r.obj, ','.join(r.permset), 'True' if r.recursive else 'False']
            self.insertRule2Table(arule)
        for r in PolicyContent.user_rule_list_dup:
            arule = [r.user, r.obj, ','.join(r.permset), 'True' if r.recursive else 'False']
            self.insertRule2Table(arule, Qt.lightGray)
#         for u, p in PolicyContent.user_obj_perm_mat.iteritems():
#             for obj, permset in p.iteritems():
#                 if permset.recur_perms:
#                     arule=[u, obj, ','.join(permset.recur_perms), 'True']
#                     self.insertRule2Table(arule)
#                 if permset.nonrecur_perms:
#                     arule=[u, obj, ','.join(permset.nonrecur_perms), 'False']
#                     self.insertRule2Table(arule)
                
    def retrievePolicyInfoFromUI(self):
        userset = PolicyContent.uoa_user_set = set()
        objset = PolicyContent.uoa_obj_set = set()
        user_obj_perm_mat = PolicyContent.user_obj_perm_mat = {}
        user_obj_perm_mat_dup = PolicyContent.user_obj_perm_mat_dup = {}
        rulelist = []
        
        for i in xrange(self.listSub.count()):
            userset.add(str(self.listSub.item(i).text()))
        for i in xrange(self.listObj.count()):
            objset.add(str(self.listObj.item(i).text()))
        for row in xrange(self.ruleTable.rowCount()):
            arule = []
            for col in xrange(self.ruleTable.columnCount()):
                arule.append(str(self.ruleTable.item(row, col).text()))
            rulelist.append(arule)
#         for rule in rulelist:
#             user, obj, permset, recursive = rule[0], rule[1], set(rule[2].split(',')), rule[3]
#             if user in user_obj_perm_mat.keys() and obj in user_obj_perm_mat[user].keys():
#                 recur_perms = user_obj_perm_mat[user].recur_perms
#                 nonrecur_perms = user_obj_perm_mat[user].nonrecur_perms
#                 if recursive == 'True':
#                     recur_perms = recur_perms.union(permset)
#                 else: 
#                     nonrecur_perms = nonrecur_perms.union(permset)
#                 user_obj_perm_mat[user] = makepermset(recur_perms, nonrecur_perms-recur_perms)
#             else:
#                 permnode = makepermset(permset, None) if recursive=='True' else makepermset(None, permset)
#                 user_obj_perm_mat.setdefault(user, {}).update({obj: permnode})
        return userset, objset, rulelist
