'''
Created on Aug 10, 2015

@author: carr
'''
from pyparsing import *
from utils import *

# tokens
UNIXLIT = Literal('UNIX')
RBACLIT = Literal('RBAC')

COMMA = Literal(',').suppress()
COLON = Literal(':').suppress()

COMMENT = Optional(pythonStyleComment).suppress()

READ = Literal('r')
WRITE = Literal('w')
EXECUTE = Literal('x')
RECURSIVE = Optional(Literal('-r '))

ID = Regex(idregex)

USER = ID
PATH = Regex(pathregex)

USERLIST = delimitedList(USER, delim=',')
ROOT = Literal('root:') - PATH
'''UNIX'''
DIR = Literal('d')
NOPERM = Literal('-')
SETID = Literal('s') | Literal('S')
 
USERSTMT = Literal('user:') - USERLIST + COMMENT
GROUP = ID
GROUPLIST = delimitedList(GROUP, delim=',')
GROUPSTMT = Literal('group:') - GROUP - USERLIST + COMMENT
PERMBITS = (READ | NOPERM) + (WRITE | NOPERM) + (EXECUTE | SETID | NOPERM)
UNIXPERMBITS = (DIR | NOPERM) + And([PERMBITS]*3)
ACCESSCREDENTIAL = USER + COLON + GROUP
RES = RECURSIVE - PATH
UNIXOBJSTMT = Literal('object:') - UNIXPERMBITS - ACCESSCREDENTIAL - RES + COMMENT
UNIXDEF = USERSTMT ^ GROUPSTMT ^ UNIXOBJSTMT ^ pythonStyleComment.suppress()
UNIX = UNIXLIT + OneOrMore(UNIXDEF)
UNIX.setParseAction(nest)
'''RBAC'''
GT = Literal('>')
ROLE = ID
ROLELIST = delimitedList(ROLE, delim=',')
PERM = READ | WRITE | EXECUTE
PERMLIST = delimitedList(PERM, delim=',')

USERDEF = Literal('user:') - ROLELIST - USERLIST + COMMENT
OBJECTDEF = Literal('object:') - ROLELIST - PERMLIST - RECURSIVE - PATH + COMMENT
INHERITDEF = Literal('inheritance:') - ROLE - GT - ROLE + COMMENT
'''mandy'''
NONEUSERDEF = Literal('free_user:') - USERLIST + COMMENT
ROLEDEF = Literal('inheritance:') - ROLE + COMMENT
PATHDEF = Literal('object:') - PATH + COMMENT
RBACDEF = USERDEF ^ NONEUSERDEF ^ OBJECTDEF ^ INHERITDEF ^ ROLEDEF ^ PATHDEF ^ pythonStyleComment.suppress()
RBAC = RBACLIT + OneOrMore(RBACDEF)
RBAC.setParseAction(nest)
POLICY = UNIX ^ RBAC
SPEC = ROOT + OneOrMore(POLICY)
# parse actions
USERDEF.setParseAction(nest)
OBJECTDEF.setParseAction(nest)
INHERITDEF.setParseAction(nest)
'''mandy'''
NONEUSERDEF.setParseAction(nest)
ROLEDEF.setParseAction(nest)
PATHDEF.setParseAction(nest)
'''mandy'''
ROLELIST.setParseAction(nest)
USERLIST.setParseAction(nest)
PERMLIST.setParseAction(nest)

ROOT.setParseAction(nest)
GROUPLIST.setParseAction(nest)
USERSTMT.setParseAction(nest)
GROUPSTMT.setParseAction(nest)
UNIXPERMBITS.setParseAction(nest)
UNIXOBJSTMT.setParseAction(nest)
PERMBITS.setParseAction(nest)

def policyParse(text):
    return SPEC.parseString(text, parseAll=True)

def printthing(x, depth=1):
    if isinstance(x, list):
        if len(x) > 1:
            for e in x:
                printthing(e, depth+1)
        else:
            print '{0}{1}'.format(' ' * 4 * depth, x)
    else:
        print '{0}{1}'.format(' ' * 4 * depth, x)

if __name__ == '__main__':
    import sys
    #with open(sys.argv[1], 'r') as f:
    with open('./policies/ZNOUSE_test.ac', 'r') as f:
        text = f.read()
#     print text
    result = policyParse(text)
    for i, stmt in enumerate(result):
        print i, stmt[0]
        rest = stmt[1:]
        for thing in rest:
            printthing(thing)
    print result
