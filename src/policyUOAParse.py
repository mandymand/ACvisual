'''
Created on Aug 10, 2015

@author: mandy
'''
from pyparsing import *
from utils import *

# tokens
COMMA = Literal(',').suppress()
COLON = Literal(':').suppress()

COMMENT = Optional(pythonStyleComment).suppress()

READ = Literal('r')
WRITE = Literal('w')
EXECUTE = Literal('x')
RECURSIVE = Optional(Literal('-r '))

ID = Regex(idregex)

USER = ID
PATH = Regex(pathregex)

USERLIST = delimitedList(USER, delim=',')
PATHLIST = delimitedList(PATH, delim=',')
ROOT = Literal('root:') - PATH
'''OS Crawl File'''
CRAWL = Literal('oscrawlfile:') - PATH
'''UOA'''
PERM = READ | WRITE | EXECUTE
PERMLIST = delimitedList(PERM, delim=',')

USERDEF = Literal('user:') - USERLIST + COMMENT
OBJECTDEF = Literal('object:') - PATHLIST + COMMENT
RULEDEF = Literal('rule:') - USER - PERMLIST - RECURSIVE - PATHLIST + COMMENT

UOADEF = USERDEF ^ OBJECTDEF ^ RULEDEF ^ pythonStyleComment.suppress()
SPEC = Optional(CRAWL) + ROOT + OneOrMore(UOADEF)
# parse actions
USERDEF.setParseAction(nest)
OBJECTDEF.setParseAction(nest)
RULEDEF.setParseAction(nest)

USERLIST.setParseAction(nest)
PATHLIST.setParseAction(nest)
PERMLIST.setParseAction(nest)
ROOT.setParseAction(nest)
CRAWL.setParseAction(nest)

def policyUOAParse(text):
    return SPEC.parseString(text, parseAll=True)

def printthing(x, depth=1):
    if isinstance(x, list):
        if len(x) > 1:
            for e in x:
                printthing(e, depth+1)
        else:
            print '{0}{1}'.format(' ' * 4 * depth, x)
    else:
        print '{0}{1}'.format(' ' * 4 * depth, x)

if __name__ == '__main__':
    with open('./policies/InheritanceFlat.uoa', 'r') as f:
        text = f.read()
#     print text
    result = policyUOAParse(text)
    for i, stmt in enumerate(result):
        print i, stmt[0]
        rest = stmt[1:]
        for thing in rest:
            printthing(thing)
    print result
