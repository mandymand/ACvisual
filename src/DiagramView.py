'''
Created on May 8, 2017

@author: manwang
'''
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QPainter
from PyQt4.QtGui import QGraphicsView
from PyQt4.Qt import QCoreApplication

class DiagramView(QGraphicsView):
    
    def __init__(self, parent = None, scrollOn = True):
        QGraphicsView.__init__(self, parent)
        self.setRenderHint(QPainter.HighQualityAntialiasing, True)
        if not scrollOn:
            self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        
    def scrollContentsBy(self, dx, dy):
        self.scene.bkrectChanged = True
        QGraphicsView.scrollContentsBy(self, dx, dy)
        self.scene.update()
