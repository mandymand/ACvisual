'''
Created on May 8, 2017

@author: manwang
'''
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4.QtGui import QGraphicsScene, QMessageBox, QGraphicsView, QGraphicsProxyWidget,\
                            QLabel, QGroupBox, QMenu
from RoleNode import RoleNode, RoleNodeHier
from EdgeView import EdgeView
from RoleNodeView import RoleNodeView
from grandalf.graphs import Edge
from grandalf.layouts import SugiyamaLayout
from spaceTree.Graph import Graph
from spaceTree.STLayout import STLayout
from spaceTree.DirNode import DirNode
from spaceTree.DirEdge import DirEdge
from collections import namedtuple
import PolicyContent
import policyUOAIO

UNIXPerm = namedtuple('UNIXPerm', ('perms', 'user', 'group'))

class SpaceTreeLegend(QGraphicsProxyWidget):
    
    def __init__(self, scene):
        QGraphicsProxyWidget.__init__(self, parent=None)
        self.scene = scene
        self.main = scene.main
        self.group = QGroupBox()
        self.layout = QGridLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.group.setLayout(self.layout)
        self.setWidget(self.group)
        
    def createUI(self):
        l1 = QLabel("Node Outline: Access to object")
        l2 = QLabel("Node Fill: Solid = Access to children objects")
        l3 = QLabel("         : Hollow = A file node, No children objects")
        cgreen = QLabel()
        cgreen.resize(50,50)
        cgreen.setStyleSheet("QLabel { background-color : QColor(43,140,190)}")
        clgihtgreen = QLabel()
        clgihtgreen.resize(50,50)
        clgihtgreen.setStyleSheet("QLabel { background-color : QColor(158,202,225)}")
        cgray = QLabel()
        cgray.resize(50,50)
        cgray.setStyleSheet("QLabel { background-color : gray}")
        self.layout.addWidget(l1)
        self.layout.addWidget(l2)
        self.layout.addWidget(l3)
        self.layout.addWidget(self.main.addLineToLayout())
        self.layout.addWidget(cgreen)
        self.layout.addWidget(clgihtgreen)
        self.layout.addWidget(cgray)
    
class DiagramScene(QGraphicsScene):
    
    def __init__(self, mainWindow):
        QGraphicsScene.__init__(self, mainWindow)
        self.main = mainWindow
        '''Set pos for canvas'''
        self.translateOffsetX, self.translateOffsetY = 0, 0
        self.scaleOffsetX, self.scaleOffsetY = 1,1
        self.scaleOffset = 1
        self.margin_vertical = 20
        self.margin_horizontal = 20
        self.msg = ''
        self.msgFont = QFont( "Arial", 20, QFont.Normal)
        self.msg_ui = QGraphicsTextItem()
        self.msg_ui.setFont(self.msgFont)
        self.msg_ui.setPlainText('This is a test for long strings that needs word wrap in effect')
        self.bkrectChanged = True
        self.msgs = []
        self.roleHier = None
        self.connComps = []
        self.resetScene()
        self.addItem(self.msg_ui)
        
    def updateCanvasDimension(self):   
        self.roleHierW = self.width()-2*self.margin_horizontal
        self.roleHierH = self.height()-2*self.margin_vertical
           
        self.roleHierX = self.margin_horizontal
        self.roleHierY = self.margin_vertical
        '''-------------------------------------------'''
        '''For object tree visualization'''
        self.canvasX, self.canvasY = self.margin_horizontal,\
                                     self.margin_vertical
        self.canvasW, self.canvasH = self.width()-2*self.margin_horizontal,\
                                     self.height()-2*self.margin_vertical  
        
    def printRects(self):
        print 'role', self.roleHierX, self.roleHierY, self.roleHierW, self.roleHierH
        print 'canvas', self.canvasX, self.canvasY, self.canvasW, self.canvasH
        
    def resetScene(self):
        self.clear()
        self.clickedNode = None
        self.clickedRoleNode = None
        self.clickedRelatedNodes = set()
        
    def createRoleGraph(self):
        self.clear()
        self.msg = '"RoleA -> RoleB": RoleA inherits RoleB'
        '''for the policy vis section'''
        self.roleHier = RoleNodeHier(self.main)
        self.constructRoleHier()
        self.drawRoleHier()
        if self.roleHier.sug:
            self.roleHier.sug.draw()
        self.fitRoleHier()
        
    def clear(self):
        for i in self.items():
            if i == self.msg_ui: continue
            self.removeItem(i)
            del i
            
    def clearScreen(self):
        for i in self.items():
            if isinstance(i, DirNode):
                i.drawn = False
                i.setVisibile(False)
        
    def processCrawlerResult(self, filename):
        import os.path
        filename = os.path.abspath(filename)
        if not os.path.exists(filename): return {}
        directories = {}
        f = open(filename, 'r')
        for line in f.readlines():
            path, perm, uowner, gowner= line.split('\t')
            if path!='/' and path[-1]=='/': path = path[:-1]
            directories[path] = UNIXPerm(perm, uowner, gowner)
        return directories
    
    def createObjGraph(self, objfilename=''):
        self.view = self.main.view_obj
        self.view.setTransformationAnchor(QGraphicsView.NoAnchor)
        self.graph = Graph(self) #Access a <Graph> instance.
#         directories = ['/', '/tools', '/home', '/classes/os', '/classes/security', '/classes/security/public', '/classes/os/public']
        #direckeys = ['/', '/tools/t2/mine', '/tools/t1/mine', '/tools/t3/school/mine' ,'/home', '/classes/os', '/classes/security', '/classes/security/public', '/classes/os/public']
#         directories = ['/Users/manw/Documents/interview', 
#                      '/Users/manw/Documents/interview/gdbiblio.pdf', 
#                      '/Users/manw/Documents/interview/notes',
#                      '/Users/manw/Documents/interview/cv_temp', 
#                      '/Users/manw/Documents/interview/notes/main.pdf', 
#                      '/Users/manw/Documents/interview/cv_temp/cv.pdf']
        if objfilename:
            directories = self.processCrawlerResult(objfilename)
#             self.userSysList, self.groupSysList, self.user_group_sys_mat = \
#             PermissionChecker.getUserAndGroupListOnSystem('/', os.path.abspath('./'))
            PolicyContent.unix_object_set = directories.keys()
        else:
            directories = PolicyContent.uoa_object_set
        self.graph.createGraph(directories)
        self.layout = STLayout(self.graph.root, self.graph.nodeDict, self)
        self.layout.onClick(self.graph.root.path)
        self.fitObjectGraph()
        self.graph.root.selected = False
        
    def createRoleNode(self, r):
        role = RoleNode(r, self, self.main)
        self.roleHier.add_vertex(role)
        self.roleHier.data2RoleNode.update({r:role})
        self.addItem(role.view)
        return role
    
    def createEdgeForRoleHier(self, r, child):
        edge = Edge(r, child)
        edge.view = EdgeView(r, child, self.main)
        self.roleHier.add_edge(edge)
        self.addItem(edge.view)
        return edge

    def constructRoleHier(self):
        for r in PolicyContent.rbac_role_res_mat.keys():
            self.createRoleNode(r)
        for r in PolicyContent.rbac_role_inher_mat.keys():
            rn = self.roleHier.data2RoleNode[r]
            for p in PolicyContent.rbac_role_inher_mat[r].parents:
                pn = self.roleHier.data2RoleNode[p]
                rn.parents.add(pn)
                e = self.createEdgeForRoleHier(pn, rn)
                pn.edges[rn] = e
            for c in PolicyContent.rbac_role_inher_mat[r].children:
                cn = self.roleHier.data2RoleNode[c]
                rn.children.add(cn)
        
    def drawRoleHier(self):
        if len(self.roleHier.C) == 1:
            gr = self.roleHier.C[0]
            self.roleHier.sug = SugiyamaLayout(gr)
            self.roleHier.cycles = self.roleHier.sug.init_all(roots=[gr.sV[0]],inverted_edges=None)
            for l in self.roleHier.cycles:
                for v in l:
                    for e in v.e:
                        if e.v[0] == v and e.v[1] in l:
                            e.view.highlighted = True
    
    def fitRoleHier(self):
        topLeftX = 5000
        topLeftY = 5000
        bottomRightX = 0
        bottomRightY = 0
        
        for item in self.roleHier.V():
            p = item.view.pos()
            if p.x() < topLeftX:
                topLeftX = p.x()
            if p.y() < topLeftY:
                topLeftY = p.y()
            if p.x() > bottomRightX:
                bottomRightX = p.x()
            if p.y() > bottomRightY:
                bottomRightY = p.y()
        topLeftX *= 0.9
        topLeftY *= 0.9
        bottomRightX *= 1.1
        bottomRightY *= 1.1
        bottomRightY += 20
        center = [0.5*(bottomRightX-topLeftX),0.5*(bottomRightY-topLeftY)]
        self.main.view.centerOn(center[0], center[1])
        self.main.view.fitInView(topLeftX, topLeftY, bottomRightX-topLeftX, bottomRightY-topLeftY, mode = Qt.KeepAspectRatio)
        self.update()
        
    def computePermForRoleWithInheritance(self, rolePerm, rn, exceptionRole=None):
        exceptionRoleNode = None
        children = self.roleHier.findAllChildren_BFS(rn)
        if exceptionRole: exceptionRoleNode = self.roleHier.data2RoleNode[exceptionRole]
        if exceptionRoleNode in children: children.remove(exceptionRoleNode)
        for c in children:
            crole = c.data
            for obj, perms in PolicyContent.rbac_role_res_mat[crole].iteritems():
                policyUOAIO.combinePermsForOne(rolePerm, obj, perms)
        policyUOAIO.removeRedundantPermForARole(rolePerm)
        return rolePerm
           
    def recomputeRoleResForRoles(self, exceptionRole):
        import copy
        rbac_role_res_mat = {}
        '''Recompute the role permissions with inheritance and get the new relationship'''
        '''except the roles that was in a cycle with the current role'''
        for r in PolicyContent.rbac_role_res_mat.keys():
            if r==exceptionRole: continue
            rolenode = self.roleHier.data2RoleNode[r]
            rolePerm = copy.deepcopy(PolicyContent.rbac_role_res_mat[r])
            exception = None
            if self.isInSameCycle(r, exceptionRole):
                exception = exceptionRole
            rbac_role_res_mat[r] = self.computePermForRoleWithInheritance(rolePerm, rolenode, exception)
        for r, p in rbac_role_res_mat.iteritems():
            PolicyContent.rbac_role_res_mat.update({r:p})
        
    def getRoleInCyclesToConnect(self, connComps):
        cycleRoles = set()
        for compset in connComps: 
            cycleRoles = cycleRoles.union(compset)
        for compset in connComps:
            comp = list(compset)
            '''find the most dominant role'''
            parent, child = policyUOAIO.findNearestAncestorNChild2Conn(comp)
            policyUOAIO.buildConnection(parent, child, PolicyContent.rbac_role_inher_mat)
#             print 'cycle', parent,child

    def renewInheritanceForCycle(self, roles, connComps):
        cycleRoles = set()
        randomCycleRoles = set()
        rbac_role_inher_mat = {}
        for compset in connComps:
            cycleRoles = cycleRoles.union(compset)
            comp = list(compset)
            randomCycleRoles.add(comp[0])
        '''build inheritance for the rest roles'''
        roles2Build = set(roles) - cycleRoles
        rolesWithOneCompRole = roles2Build.union(randomCycleRoles)
        for r in rolesWithOneCompRole:
            for nr in rolesWithOneCompRole:
                if r==nr: continue
                if policyUOAIO.isPermSubsetOfRole(r,nr,PolicyContent.rbac_role_res_mat):
                    policyUOAIO.buildConnection(nr, r, rbac_role_inher_mat)
        policyUOAIO.removeTransitiveRoleInheritance(rbac_role_inher_mat)
        for compset in connComps:
            comp = list(compset)
            '''connect the roles within a cycle, and build the inheritance'''
            for i in xrange(len(comp)-1):
                role1 = comp[i]
                role2 = comp[i+1]
                policyUOAIO.buildConnection(role1, role2, rbac_role_inher_mat)
            '''connect last node to the first node'''
            policyUOAIO.buildConnection(comp[-1], comp[0], rbac_role_inher_mat)
        return rbac_role_inher_mat
    
    def isInSameCycle(self, r1,r2):
        for cycle in self.connComps:
            if (r1 in cycle) and (r2 in cycle):
                return True
        return False
    
    def contructRoleInheritanceWithCycle(self, roles, clickedRolePerm):
        import copy
        rolenode = self.clickedRoleNode.rnode
        role = rolenode.data
        self.recomputeRoleResForRoles(role)
        '''deal with cycles'''
        connComps = policyUOAIO.findCycles(roles)
        self.connComps = copy.deepcopy(connComps)
        if not connComps:
            rbac_role_inher_mat = policyUOAIO.contructRoleInheritanceNoCycle(roles)
            policyUOAIO.removeTransitiveRoleInheritance(rbac_role_inher_mat)
        else:
            '''connect the roles in connected components'''
            self.getRoleInCyclesToConnect(connComps)
            for compset in connComps:
                if not role in compset: continue
                for c in list(compset):
                    PolicyContent.rbac_role_res_mat[c] = copy.deepcopy(clickedRolePerm)
                    policyUOAIO.removeRedundantPermForARole(PolicyContent.rbac_role_res_mat[c])
            '''renew relationship between roles that cycle roles inherited from'''
            rbac_role_inher_mat = self.renewInheritanceForCycle(roles, connComps)
        return rbac_role_inher_mat

    def updateItemPos(self):
        for i in self.items():
            if isinstance(i, RoleNodeView):
                i.setPos(i.relativeX, i.relativeY)
            elif isinstance(i, EdgeView):
                i.updatePosition()
            elif isinstance(i, DirNode):
                i.setPos(i.relativeX, i.relativeY)
            elif isinstance(i, DirEdge):
                i.updatePosition()
        self.update()
        
    def updateScene(self):
        for i in self.items():
            if isinstance(i, EdgeView):
                i.updatePosition()
            elif isinstance(i, DirEdge):
                i.updatePosition()
        self.update()
        
    '''
    Method: translate
      
      Applies a translation to the canvas.
      
      Parameters:
      
      x - (number) x offset.
      y - (number) y offset.
      disablePlot - (boolean) Default's *false*. Set this to *true* if you don't want to refresh the visualization. 
    '''
    def translate(self, x, y, disablePlot=False):
        self.translateOffsetX += x*self.scaleOffsetX
        self.translateOffsetY += y*self.scaleOffsetY
        self.view.translate(x, y)
        if not disablePlot:
            self.update()

    '''Method: scale
       
      Scales the canvas.
       
      Parameters:
       
      x - (number) scale value.
      y - (number) scale value.
      disablePlot - (boolean) Default's *false*. Set this to *true* if you don't want to refresh the visualization.
    '''
    def scale(self, x, disablePlot=False):
        y = x
        px, py = self.scaleOffsetX * x, self.scaleOffsetY * y
#         dx, dy = self.translateOffsetX*(x-1)/px, self.translateOffsetY*(y-1)/py
        self.scaleOffsetX, self.scaleOffsetY = px, py
        self.scaleGraph(1/x)
        self.update()
    
    def scaleObjectNodes(self, ratio):
        for i in self.items():
            if isinstance(i, RoleNodeView):
                i.setScale(ratio)
        self.update()
    
    def fitObjectGraph(self):
        topLeftX = 5000
        topLeftY = 5000
        bottomRightX = 0
        bottomRightY = 0
        
        for item in self.layout.nodesDraw2:
            p = item.pos()
            if p.x() < topLeftX:
                topLeftX = p.x()
            if p.y() < topLeftY:
                topLeftY = p.y()
            if p.x() > bottomRightX:
                bottomRightX = p.x()
            if p.y() > bottomRightY:
                bottomRightY = p.y()
        topLeftX *= 0.9
        topLeftY *= 0.9
        bottomRightX *= 1.1
        bottomRightY *= 1.1
        bottomRightY += 20
#         topLeftX -= self.margin_horizontal
#         topLeftY += self.margin_vertical
#         bottomRightX += self.margin_horizontal
#         bottomRightY += self.margin_vertical
        center = [0.5*(bottomRightX-topLeftX),0.5*(bottomRightY-topLeftY)]
        self.main.view_obj.centerOn(center[0], center[1])
        self.main.view_obj.fitInView(topLeftX, topLeftY, bottomRightX-topLeftX, bottomRightY-topLeftY, mode = Qt.KeepAspectRatio)
        self.update()
        
    def drawBackground(self, painter, rect):
        QGraphicsScene.drawBackground(self, painter, rect)
#         if str(self.msg_ui.toPlainText())!=self.msg:
#             self.msg_ui.setPlainText(self.msg)
#         self.msg_ui.setTextWidth(rect.width()-20)
#         self.msg_ui.setPos(rect.x()+10, rect.y()+rect.height()-(10+self.msg_ui.boundingRect().height()))
        if self.bkrectChanged or str(self.msg_ui.toPlainText())!=self.msg:
            self.msg_ui.setPlainText(self.msg)
            self.msg_ui.setTextWidth(rect.width()-20)
            self.msg_ui.setPos(rect.x()+10, rect.y()+rect.height()-(10+self.msg_ui.boundingRect().height()))
            self.bkrectChanged = False
        if self == self.main.scene: return
        '''draw legend'''
        l = ["Hollow Node: File Node",\
             "Solid Node: Folder Node",\
             "Outline Color: Access to object itself",\
             "Filled Color: Access to child objects",\
             "-"*50
             ]
        c = [
             "All accessible",\
             "Partially accessible",\
             "Not accessible"
            ]
        colors = [QColor(43,140,190),\
                  QColor(158,202,225),\
                  Qt.gray]
        if self.scaleOffset>0:
            painter.setFont(QFont( "Arial", 18/self.scaleOffset, QFont.Normal))
        else: 
            painter.setFont(QFont( "Arial", 18, QFont.Normal))
        dist = painter.fontMetrics().boundingRect(l[0]).height()
        x = rect.x()+10
        y = rect.y()+dist+10
        maxwidth = 0
        startX, startY = x,y-dist
        for ll in l:
            painter.drawText(QPointF(x,y), ll)
            y += dist
            w = painter.fontMetrics().boundingRect(ll).width()
            maxwidth = max(maxwidth, w)
        for i in xrange(len(c)):
            painter.setPen(QPen(colors[i]))
            painter.setBrush(QBrush(colors[i]))
            painter.drawRect(QRect(x,y-15,15,15))
            xx = x+25
            painter.setPen(QPen(Qt.black))
            painter.setBrush(QBrush(Qt.black))
            painter.drawText(QPoint(xx,y-2), c[i])
            y += dist+5
            w = 25+painter.fontMetrics().boundingRect(c[i]).width()
            maxwidth = max(maxwidth, w)
        painter.setBrush(Qt.NoBrush)
        painter.drawRect(QRect(startX-5, startY-5, maxwidth+10,y-startY-5))
        self.update()
        
#     def contextMenuEvent(self, evt):
#         QGraphicsScene.contextMenuEvent(self, evt)
#         if not self.main.radioBtn_roleView.isChecked(): return
#         if not self.roleHier: return 
#         menu = QMenu()
# #         role_add_action = menu.addAction('Add Role')
# #         role_del_action = menu.addAction('Delete Role')
#         menu.addSeparator()
# #         role_user_assign_action = menu.addAction('Assign User to Role')
# #         role_perm_modif_action = menu.addAction('Modify Permission of Role')
# #         role_inher_action = menu.addAction('Modify Role Inheritance')
#         role_SOD_action = menu.addAction('Add Separation of Duties')
# #         role_add_action.triggered.connect(self.main.ui.addRoleDialog.show)
# #         role_del_action.triggered.connect(self.main.ui.delRoleDialog.show)
# #         role_user_assign_action.triggered.connect(self.main.ui.assignUser2RoleDialog.show)
# #         role_perm_modif_action.triggered.connect(self.main.ui.modifRolePermDialog.show)
# #         role_inher_action.triggered.connect(self.main.ui.modifRoleInheritanceDialog.show)
#         role_SOD_action.triggered.connect(self.main.ui.addRoleSODDialog.show)
#         menu.exec_(evt.screenPos())
            
    def mousePressEvent(self, evt):
        QGraphicsScene.mousePressEvent(self, evt)
        self.main.activeScene = self
