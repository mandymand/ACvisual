'''
Created on Oct 29, 2018

@author: manw
'''

'''Store the content of current policy'''

root_dir = '/'
'''UOA'''
uoa_user_set = set()
uoa_object_set = set()
user_rule_list = []
user_rule_list_dup = []
user_obj_perm_mat = {}
'''RBAC'''
rbac_usr_role_mat = {}
rbac_role_res_mat = {}
rbac_role_inher_mat = {}
'''UNIX'''
unix_object_set = set()
oscrawlfile = None

def init():
    global user_rule_list, user_rule_list_dup
    global root_dir, uoa_user_set, uoa_object_set, user_obj_perm_mat
    global rbac_usr_role_mat, rbac_role_res_mat, rbac_role_inher_mat
    global unix_object_set, oscrawlfile

    '''UOA'''
    uoa_user_set = set()
    uoa_object_set = set()
    user_obj_perm_mat = {}
    user_rule_list = []
    user_rule_list_dup = []
    '''RBAC'''
    rbac_usr_role_mat = {}
    rbac_role_res_mat = {}
    rbac_role_inher_mat = {}
    '''UNIX'''
    unix_object_set = set()
    oscrawlfile = None
