'''
Created on Apr 11, 2018

@author: manw
'''
import subprocess
import PolicyContent
from policyUOAIO import *

PERMISSIONS = {'r': 'read', 'w': 'write', 'x': 'execute'}

def readFile(filename):
    f = open(filename, 'r') 
    print f.readlines()

def findAllCyclicCheckBetweenRoles():
    listofChecks = []
    for senior in PolicyContent.rbac_role_inher_mat.keys():
        for junior in PolicyContent.rbac_role_inher_mat[senior].children:
            acheck = findCyclicCheckBetweenRoles(senior, junior)
            if acheck: listofChecks.append(acheck)
    return listofChecks

def findCyclicCheckBetweenRoles(senior, junior):
    '''senior inherited the junior'''
    '''Need to check whether the junior has access to any of the objects that only the senior can access'''
    sPerm = PolicyContent.rbac_role_res_mat[senior]
#     jPerm = PolicyContent.rbac_role_res_mat[junior]
    if not sPerm: return None
    obj = sPerm.keys()[0]
    perm = sPerm[obj]
    return [junior, obj.replace('/', '_'), \
            None if not perm.recur_perms else list(perm.recur_perms)[0], \
            None if not perm.nonrecur_perms else list(perm.nonrecur_perms)[0]]
#     if(sPerm.keys()!=jPerm.keys()):
#         for obj in sPerm.keys():
#             if obj not in jPerm.keys():
#                 return [junior, obj.replace('/', '_'), list(sPerm[obj].perms)[0]]
#     else:
#         for obj in sPerm.keys():
#             diffp = sPerm[obj].perms-jPerm[obj].perms
#             if diffp:
#                 return [junior, obj.replace('/', '_'), list(diffp)[0]]

def getRolePermRules(r):
    listofPerms = []
    roles = set([r])
    while roles:
        curr = roles.pop()
        roles = roles.union(PolicyContent.rbac_role_inher_mat[curr].children)
        for obj, perms in PolicyContent.rbac_role_res_mat[curr].items():
            for p in perms.recur_perms:
                obj = obj.replace('.', '__')
                item = (r, obj.replace('/', '_'), PERMISSIONS[p], True)
                if item not in listofPerms:
                    listofPerms.append(item)
            for p in perms.nonrecur_perms:
                obj = obj.replace('.', '__')
                item = (r, obj.replace('/', '_'), PERMISSIONS[p], False)
                if item not in listofPerms:
                    listofPerms.append(item)
    return listofPerms

def addParentInRoleHier(r, p):
    if r not in PolicyContent.rbac_role_inher_mat.keys():
        PolicyContent.rbac_role_inher_mat.update({r:makerolenode(parents=set([p]))})
    else:
        PolicyContent.rbac_role_inher_mat[r].parents.add(p)
            
def addChildInRoleHier(r, c):
    if r not in PolicyContent.rbac_role_inher_mat.keys():
        PolicyContent.rbac_role_inher_mat.update({r:makerolenode(children=set([c]))})
    else:
        PolicyContent.rbac_role_inher_mat[r].children.add(c)
        
def deleteParentInRoleHier(r, p):
    if r in PolicyContent.rbac_role_inher_mat.keys():
        PolicyContent.rbac_role_inher_mat[r].parents.remove(p)
    
def deleteChildInRoleHier(r, c):
    if r in PolicyContent.rbac_role_inher_mat.keys():
        PolicyContent.rbac_role_inher_mat[r].children.remove(c)
        
def checkElementExistInList(element, listName):
    if element in listName:
        return True
    else:
        return False

# def addRule2SMVPolicy(filename, senior, junior, isRoleRule=True):
#     filename = filename+'.smv'
#     '''There are two types of rules: 1) role rule; 2) user rule'''
#     if isRoleRule:
#         '''first add all permission of junior to the senior role in smv'''
#         listOfPerms = []
#         sperms = getRolePermWithInheritance(senior)
#         jperms = getRolePermWithInheritance(junior)
#         for obj, perms in jperms.iteritems():
#             obj = obj.replace('.', '__')
#             if obj not in sperms.iteritems():
#                 for p in perms.perms:
#                     item = (senior, obj.replace('/', '_'), PERMISSIONS[p])
#             else:
#                 pp = jperms[obj].perms-sperms[obj].perms
#                 for p in pp:
#                     item = (senior, obj.replace('/', '_'), PERMISSIONS[p])
#             if item not in listOfPerms:
#                 listOfPerms.append(item)
#         f = open(filename, "r")
#         contents = f.readlines()
#         f.close()
#         index = contents.index("MODULE RBAC_RolePerms( ROLES, OBJECTS, OPERATIONS)\n")
#         for l in listOfPerms:
#             permstr = "\tROLES = "+l[0]+" & OBJECTS = "+ l[1]\
#                                 +" & OPERATIONS = "+l[2]+" : Permit;\n"
#             contents.insert(index+6, permstr)
#         '''then add the hierarchy''' 
#         addChildInRoleHier(senior, junior)
#         addParentInRoleHier(junior, senior)
#         '''add property check'''
# #         listOfChecks = []
# #         for junior in PolicyContent.rbac_role_inher_mat[senior].children:
# #             acheck = findCyclicCheckBetweenRoles(senior, junior)
# #             if acheck:
# #                 listOfChecks.append(acheck)
# #         index = contents.index("-- Cyclic inheritance property\n")
# #         for l in listOfChecks:
# #             spec = "SPEC AG ((ROLES = "+l[0]+") & (OBJECTS = "+l[1]+") & (OPERATIONS = "+PERMISSIONS[l[2]]+\
# #                 ") -> AF decision = Deny)\n"
# #             contents.insert(index+1, spec)
#         f = open(filename, "w")
#         contents = "".join(contents)
#         f.write(contents)
#         f.close()
#     else:
#         return 

def genSMVHeader():
    lines = ['MODULE main\n']
    lines.append('VAR\n')
    '''List of users'''
    lines.append("\tUSERS: {dummy, "+', '.join(PolicyContent.uoa_user_set)+'};\n')
    '''List of roles'''
    lines.append("\tROLES: {dummy, "+', '.join(PolicyContent.rbac_role_res_mat.keys())+'};\n')
    '''List of objects'''
    objstr = "\tOBJECTS: {dummy"
    for o in PolicyContent.uoa_object_set:
        while o[-1]=='/' and o!='/': o = o[:len(o)-1]
        o = o.replace('.', '__')
        objstr+=', '+o.replace('/', '_')
    lines.append(objstr+'};\n')
    lines.append("\tOPERATIONS: {dummy, read, write, execute};\n")
    lines.append("\tRECURSIVE: {true, false};\n")
    return lines

def genSMVRolePerms():
    lines = ["MODULE RBAC_RolePerms( ROLES, OBJECTS, OPERATIONS, RECURSIVE)\n"]
    lines.append("VAR\n")
    lines.append("\tdecision:\t{Permit, Deny};\n")
    lines.append("ASSIGN\n")
    lines.append("\tinit(decision)\t:= Deny;\n")
    lines.append("\tnext(decision)\t:= case\n")
#     '''
#     with recursive permission
#     for each role, use dict to store access to each object
#         1. the perms from that role and the roles it inherits
#         2. the perms from recursive permissions
#     '''
#     objsetSMV = set()
#     for o in PolicyContent.unix_object_set:
#         obj = o.replace('.', '__')
#         obj = obj.replace('/', '_')
#         objsetSMV.add(obj)
#     for r in PolicyContent.rbac_role_res_mat.keys():
#         rolePermDict = {}
#         listOfPerms = getRolePermRules(r)
#         for l in listOfPerms:
#             obj = l[1]
#             permission = l[2]
#             rolePermDict.setdefault(obj, set([])).add(permission)
#             if l[3]:
#                 for o in objsetSMV:
#                     if obj in o:
#                         if o not in rolePermDict.keys():
#                             rolePermDict[o] = set([permission])
#                         else:
#                             rolePermDict[o].add(permission)
#         for o, p in rolePermDict.iteritems():
#             for pp in p:
#                 permstr = "\tROLES = "+r+" & OBJECTS = "+o \
#                                     +" & OPERATIONS = "+pp+" : Permit;\n"
#                 lines.append(permstr)
    '''with no recursive'''
    for r in PolicyContent.rbac_role_res_mat.keys():
        listOfPerms = getRolePermRules(r)
        for l in listOfPerms:
            recursive = 'true' if l[3] else 'false'
            permstr = "\tROLES = "+l[0]+" & OBJECTS = "+l[1] \
                                +" & OPERATIONS = "+l[2]+" & RECURSIVE = "+recursive+" : Permit;\n"
            lines.append(permstr)
    lines.append("\tTRUE: Deny;\n")
    lines.append("\tesac;\n")
    
    '''Property Check'''
    lines += genSMVRoleCyclicCheck()
    lines += genSMVRoleAutonomyCheck(listOfPerms)
    return lines

# def genSMVRolePerms():
#     lines = ["MODULE RBAC_RolePerms( ROLES, OBJECTS, OPERATIONS)\n"]
#     lines.append("VAR\n")
#     lines.append("\tdecision:\t{Permit, Deny};\n")
#     lines.append("ASSIGN\n")
#     lines.append("\tinit(decision)\t:= Deny;\n")
#     lines.append("\tnext(decision)\t:= case\n")
# #     '''
# #     with recursive permission
# #     for each role, use dict to store access to each object
# #         1. the perms from that role and the roles it inherits
# #         2. the perms from recursive permissions
# #     '''
# #     objsetSMV = set()
# #     for o in PolicyContent.unix_object_set:
# #         obj = o.replace('.', '__')
# #         obj = obj.replace('/', '_')
# #         objsetSMV.add(obj)
# #     for r in PolicyContent.rbac_role_res_mat.keys():
# #         rolePermDict = {}
# #         listOfPerms = getRolePermRules(r)
# #         for l in listOfPerms:
# #             obj = l[1]
# #             permission = l[2]
# #             rolePermDict.setdefault(obj, set([])).add(permission)
# #             if l[3]:
# #                 for o in objsetSMV:
# #                     if obj in o:
# #                         if o not in rolePermDict.keys():
# #                             rolePermDict[o] = set([permission])
# #                         else:
# #                             rolePermDict[o].add(permission)
# #         for o, p in rolePermDict.iteritems():
# #             for pp in p:
# #                 permstr = "\tROLES = "+r+" & OBJECTS = "+o \
# #                                     +" & OPERATIONS = "+pp+" : Permit;\n"
# #                 lines.append(permstr)
#     '''with no recursive'''
#     for r in PolicyContent.rbac_role_res_mat.keys():
#         listOfPerms = getRolePermRules(r)
#         for l in listOfPerms:
#             permstr = "\tROLES = "+l[0]+" & OBJECTS = "+l[1] \
#                                 +" & OPERATIONS = "+l[2]+" : Permit;\n"
#             lines.append(permstr)
#     lines.append("\tTRUE: Deny;\n")
#     lines.append("\tesac;\n")
#     
#     '''Property Check'''
#     lines += genSMVRoleCyclicCheck()
#     lines += genSMVRoleAutonomyCheck(listOfPerms)
#     return lines
def genSMVRoleCyclicCheck():
    '''Role cyclic property'''
    lines = ["-- Cyclic inheritance property\n"]
    listOfChecks = findAllCyclicCheckBetweenRoles()
    for l in listOfChecks:
        if l[2]:
            lines.append("SPEC AG ((ROLES = "+l[0]+") & (OBJECTS = "+l[1].replace('.', '__')+") & (OPERATIONS = "+PERMISSIONS[l[2]]+\
                    ") & (RECURSIVE = true) -> AF decision = Deny)\n")
        elif l[3]:
            lines.append("SPEC AG ((ROLES = "+l[0]+") & (OBJECTS = "+l[1].replace('.', '__')+") & (OPERATIONS = "+PERMISSIONS[l[3]]+\
                    ") & (RECURSIVE = false) -> AF decision = Deny)\n")
    return lines

# def genSMVRoleCyclicCheck():
#     '''Role cyclic property'''
#     lines = ["-- Cyclic inheritance property\n"]
#     listOfChecks = findAllCyclicCheckBetweenRoles()
#     for l in listOfChecks:
#         if not l: continue
#         lines.append("SPEC AG ((ROLES = "+l[0]+") & (OBJECTS = "+l[1].replace('.', '__')+") & (OPERATIONS = "+PERMISSIONS[l[2]]+\
#                 ") -> AF decision = Deny)\n")
#     return lines
        
def genSMVRoleAutonomyCheck(listOfPerms):
    '''Autonomy property'''
    lines = ["-- Conflict check for access to object\n"]
    for l in listOfPerms:
        recursive = 'true' if l[3] else 'false'         
        lines.append("SPEC AG ((ROLES = "+l[0]+") & (OBJECTS = "+l[1].replace('.', '__')+") & (OPERATIONS = "+l[2]+\
                ") & (RECURSIVE = "+recursive+" ) -> AF decision = Permit)\n")
    return lines
        
def genSMVUserPerms(rulelist, isFromTable=False):
    lines = ["MODULE RBAC_UserPerms( USERS, OBJECTS, OPERATIONS, RECURSIVE)\n"]
    lines.append("VAR\n")
    lines.append("\tdecision:\t{Permit, Deny};\n")
    lines.append("ASSIGN\n")
    lines.append("\tinit(decision)\t:= Deny;\n")
    lines.append("\tnext(decision)\t:= case\n")
    listOfUserPerms = []
    objAccessibleDict = {}
    userAccessibleDict = {}
    for u in PolicyContent.uoa_user_set:
        if u in PolicyContent.rbac_usr_role_mat.keys():
            for r in PolicyContent.rbac_usr_role_mat[u]:
                listOfPermsForUser = getRolePermRules(r)
                for l in listOfPermsForUser:
                    aup = [u, l[1], l[2]]
                    permstr = "\tUSERS = "+u+" & OBJECTS = "+ l[1]\
                         +" & OPERATIONS = "+l[2]+" : Permit;\n"
                    listOfUserPerms.append(aup)
                    lines.append(permstr)
                    objAccessibleDict.setdefault(l[1], set()).add(l[2])
                    userAccessibleDict.setdefault(u, set()).add(l[2])
    for obj, permset in objAccessibleDict.iteritems():
        for p in permset:
            permstr = "\tUSERS = dummy"+" & OBJECTS = "+ obj\
                             +" & OPERATIONS = "+p+" : Permit;\n"
            lines.append(permstr)
    for user, permset in userAccessibleDict.iteritems():
        for p in permset:
            permstr = "\tUSERS = "+ user +" & OBJECTS = dummy"\
                             +" & OPERATIONS = "+p+" : Permit;\n"
            lines.append(permstr)
    lines.append("\tTRUE: Deny;\n")
    lines.append("\tesac;\n")
    if isFromTable:
        '''User permission property check'''    
        for r in rulelist:
            user = 'dummy' if r[0]=='Any' else r[0]
            obj = r[1].replace('/', '_')
            obj = obj.replace('.', '__')
            if obj=='Any': obj = 'dummy'
            if r[2] == 'r': op = 'read'
            elif r[2] == 'w': op = 'write'
            else: op = 'execute'
            lines.append("SPEC AG ((USERS = "+user+") & (OBJECTS = "+ obj\
                +") & (OPERATIONS = "+op+") -> AF decision = "+ r[3] + ")\n")
    else:
        '''User permission property check'''
        lines.append("-- User permission check\n")
        for l in listOfUserPerms:
            lines.append("SPEC AG ((USERS = "+l[0]+") & (OBJECTS = "+l[1]+") & (OPERATIONS = "+l[2]+\
                    ") -> AF decision = Permit)\n")
    return lines

def generateSMVPolicy(filename):
    filename = filename[:filename.rfind('.')]
    lines = genSMVHeader()
    lines.append("\tRBAC_RolePerms: RBAC_RolePerms( ROLES, OBJECTS, OPERATIONS, RECURSIVE);\n")
    lines.append("\tRBAC_UserPerms: RBAC_UserPerms( USERS, OBJECTS, OPERATIONS, RECURSIVE);\n")
    lines.append("ASSIGN\n")
    lines.append("\tnext(USERS)\t\t:=\tUSERS;\n")
    lines.append("\tnext(ROLES)\t\t:=\tROLES;\n")
    lines.append("\tnext(OBJECTS)\t:=\tOBJECTS;\n")
    lines.append("\tnext(OPERATIONS):=\tOPERATIONS;\n")
    lines.append("\tnext(RECURSIVE):=\tRECURSIVE;\n\n")
    '''Role detail part'''
    lines += genSMVRolePerms()
    '''User detail part'''
    lines += genSMVUserPerms(None)
    f = open(filename+'.smv', 'w')
    for l in lines:
        f.write(l)
    f.close()
#     print "object with no rule"
#     findObjWithNoRule()
#     findUserWithNoRole()

# def generateSMVPolicy(filename):
#     filename = filename[:filename.rfind('.')]
#     lines = genSMVHeader()
#     lines.append("\tRBAC_RolePerms: RBAC_RolePerms( ROLES, OBJECTS, OPERATIONS);\n")
#     lines.append("\tRBAC_UserPerms: RBAC_UserPerms( USERS, OBJECTS, OPERATIONS);\n")
#     lines.append("ASSIGN\n")
#     lines.append("\tnext(USERS)\t\t:=\tUSERS;\n")
#     lines.append("\tnext(ROLES)\t\t:=\tROLES;\n")
#     lines.append("\tnext(OBJECTS)\t:=\tOBJECTS;\n")
#     lines.append("\tnext(OPERATIONS):=\tOPERATIONS;\n\n")
#     '''Role detail part'''
#     lines += genSMVRolePerms()
#     '''User detail part'''
#     lines += genSMVUserPerms(None)
#     f = open(filename+'.smv', 'w')
#     for l in lines:
#         f.write(l)
#     f.close()
# #     print "object with no rule"
# #     findObjWithNoRule()
# #     findUserWithNoRole()

# def generateSMVPolicy(filename):
#     filename = filename[:filename.rfind('.')]
#     f = open(filename+'.smv', 'w')
#     f.write('MODULE main\n')
#     f.write('VAR\n')
#     '''List of users'''
#     f.write("\tUSERS: {dummy, "+', '.join(PolicyContent.uoa_user_set)+'};\n')
#     '''List of roles'''
#     f.write("\tROLES: {dummy, "+', '.join(PolicyContent.rbac_role_res_mat.keys())+'};\n')
#     '''List of objects'''
#     objstr = "\tOBJECTS: {dummy"
#     for o in PolicyContent.uoa_object_set:
#         while o[-1]=='/': o = o[:len(o)-1]
#         o = o.replace('.', '__')
#         objstr+=', '+o.replace('/', '_')
#     f.write(objstr+'};\n')
#     f.write("\tOPERATIONS: {dummy, read, write, execute};\n")
#     f.write("\tRBAC_RolePerms: RBAC_RolePerms( ROLES, OBJECTS, OPERATIONS);\n")
#     f.write("\tRBAC_UserPerms: RBAC_UserPerms( USERS, OBJECTS, OPERATIONS);\n")
#     f.write("ASSIGN\n")
#     f.write("\tnext(USERS)\t\t:=\tUSERS;\n")
#     f.write("\tnext(ROLES)\t\t:=\tROLES;\n")
#     f.write("\tnext(OBJECTS)\t:=\tOBJECTS;\n")
#     f.write("\tnext(OPERATIONS):=\tOPERATIONS;\n")
#     '''Role detail part'''
#     f.write("\n")
#     f.write("MODULE RBAC_RolePerms( ROLES, OBJECTS, OPERATIONS)\n")
#     f.write("VAR\n")
#     f.write("\tdecision:\t{Permit, Deny};\n")
#     f.write("ASSIGN\n")
#     f.write("\tinit(decision)\t:= Deny;\n")
#     f.write("\tnext(decision)\t:= case\n")
#     for r in PolicyContent.rbac_role_res_mat.keys():
#         listOfPerms = getRolePermRules(r)
#         for l in listOfPerms:
#             permstr = "\tROLES = "+l[0]+" & OBJECTS = "+l[1] \
#                                 +" & OPERATIONS = "+l[2]+" : Permit;\n"
#             f.write(permstr)
#     f.write("\tTRUE: Deny;\n")
#     f.write("\tesac;\n")
#     '''Property Check'''
#     #Role cyclic property
#     f.write("-- Cyclic inheritance property\n")
#     listOfChecks = findAllCyclicCheckBetweenRoles()
#     for l in listOfChecks:
#         if not l: continue
#         f.write("SPEC AG ((ROLES = "+l[0]+") & (OBJECTS = "+l[1].replace('.', '__')+") & (OPERATIONS = "+PERMISSIONS[l[2]]+\
#                 ") -> AF decision = Deny)\n")
#     ''''''
#     #Autonomy property
#     f.write("-- Conflict check for access to object\n")
#     for l in listOfPerms:
#         f.write("SPEC AG ((ROLES = "+l[0]+") & (OBJECTS = "+l[1].replace('.', '__')+") & (OPERATIONS = "+l[2]+\
#                 ") -> AF decision = Permit)\n")
#     '''User detail part'''
#     f.write("MODULE RBAC_UserPerms( USERS, OBJECTS, OPERATIONS)\n")
#     f.write("VAR\n")
#     f.write("\tdecision:\t{Permit, Deny};\n")
#     f.write("ASSIGN\n")
#     f.write("\tinit(decision)\t:= Deny;\n")
#     f.write("\tnext(decision)\t:= case\n")
#     listOfUserPerms = []
#     for u in PolicyContent.uoa_user_set:
#         if u in PolicyContent.rbac_usr_role_mat.keys():
#             for r in PolicyContent.rbac_usr_role_mat[u]:
#                 listOfPermsForUser = getRolePermRules(r)
#                 for l in listOfPermsForUser:
#                     aup = [u, l[1], l[2]]
#                     permstr = "\tUSERS = "+u+" & OBJECTS = "+ l[1]\
#                          +" & OPERATIONS = "+l[2]+" : Permit;\n"
#                     listOfUserPerms.append(aup)
#                     f.write(permstr)
#     f.write("\tTRUE: Deny;\n")
#     f.write("\tesac;\n")
#     '''Property check'''
#     #User permission property
#     f.write("-- User permission check\n")
#     for l in listOfUserPerms:
#         f.write("SPEC AG ((USERS = "+l[0]+") & (OBJECTS = "+l[1]+") & (OPERATIONS = "+l[2]+\
#                 ") -> AF decision = Permit)\n")
#     f.close()
# #         PolicyContent.uoa_user_set = set()
# #         user_obj_perm_mat = {}
# #         PolicyContent.rbac_usr_role_mat = {}
# #     print "object with no rule"
# #     findObjWithNoRule()
# #     findUserWithNoRole()

def findObjWithNoRule():
    freeObj = copy.deepcopy(PolicyContent.uoa_object_set)
    for v in PolicyContent.rbac_role_res_mat.values():
        for o in v.keys():
            if o in freeObj:
                freeObj.remove(o)
    return freeObj

def findUserWithNoRole():
    freeUser = copy.deepcopy(PolicyContent.uoa_user_set)
    for u, r in PolicyContent.rbac_usr_role_mat.items():
        if r: 
            if u in freeUser:
                freeUser.remove(u)
    return freeUser

class PolicyVerifier(object):
 
    def __init__(self, main):
        self.main = main
        self.child_pid = None
         
    def callSMV(self, tableNotEmpty):
        if tableNotEmpty: filename = './policies/temp.smv'
        else: filename = self.main.uoapolicy[:self.main.uoapolicy.rfind('.')]+'.smv'
        self.main.ui.policyRatifierWidget.ratifierTextEdit.clear()
        proc = subprocess.Popen(["NuSMV-2.6.0-Darwin/bin/NuSMV", filename], stdout=subprocess.PIPE)
        self.child_pid = proc.pid
        output = proc.communicate()[0]
        self.main.ui.policyRatifierWidget.ratifierTextEdit.appendPlainText(output)
        self.main.ui.policyRatifierWidget.ratifierTextEdit.repaint()
        return output
    
if __name__ == '__main__':
    policyUOAIO('./policies/InheritanceFlat.uoa')
    convert2RBAC()
#     print findCyclicCheckBetweenRoles('role2', 'role4')
#     print findCyclicCheckBetweenRoles('role1', 'role0')
    generateSMVPolicy('./policies/InheritanceFlat')
#     readFile('./policies/InheritanceFlat.smv')
