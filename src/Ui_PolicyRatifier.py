'''
Created on Oct 20, 2018

@author: manw
'''
from PyQt4.QtGui import *
from PyQt4.Qt import *
import PolicyContent
from PolicyVerifier import *

class Ui_PoliRatifier_AddProperty(QDialog):
    '''
    add property the check against:
    - Enter user, role, object, operation, decision
    - Confirm
    '''
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi()
        
    def createComboBox(self, content=None):
        w = QComboBox()
        w.setEditable(True)
        if content:
            w.addItems(content)
        return w

    def setupUi(self):
        self.resize(300, 100)
        layout = QGridLayout()
        groupSub = QGroupBox('User')
        self.sublist = self.createComboBox()
        l1 = QVBoxLayout()
        l1.addWidget(self.sublist)
        groupSub.setLayout(l1)
        layout.addWidget(groupSub, 0,0,1,1)
        
#         groupRole = QGroupBox('Role')
#         self.rolelist = self.createComboBox()
#         l2 = QVBoxLayout()
#         l2.addWidget(self.rolelist)
#         groupRole.setLayout(l2)
#         layout.addWidget(groupRole, 1,0,1,1)
        
        groupObj = QGroupBox('Object')
        self.objlist = self.createComboBox()
        l3 = QVBoxLayout()
        l3.addWidget(self.objlist)
        groupObj.setLayout(l3)
        layout.addWidget(groupObj, 1,0,1,1)
        
#         self.recurCheck = QCheckBox('Recursive')
#         self.recurCheck.setCheckable(True)
#         self.recurCheck.setChecked(False)
#         layout.addWidget(self.recurCheck, 2,0,1,1)
        
        groupDec = QGroupBox('Decision')
        self.declist = self.createComboBox(['Permit', 'Deny'])
        self.declist.setEditable(True)
        l3 = QVBoxLayout()
        l3.addWidget(self.declist)
        groupDec.setLayout(l3)
        layout.addWidget(groupDec, 2,0,1,1)
        
        groupAct = QGroupBox('Operations')
        ll = QGridLayout()
        ll.setContentsMargins(0, 0, 0, 0)
        
        g2 = QGroupBox()
        l2 = QVBoxLayout()
        self.cb_permR = QCheckBox('Read')
        self.cb_permW = QCheckBox('Write')
        self.cb_permX = QCheckBox('Execute')
        l2.addWidget(self.cb_permR)
        l2.addWidget(self.cb_permW)
        l2.addWidget(self.cb_permX)
        g2.setLayout(l2)
        ll.addWidget(g2, 0,0,1,1)
        groupAct.setLayout(ll)
        layout.addWidget(groupAct, 0,1,3,1)
        
        self.ratifier_addPropBtn = QPushButton('Add')
        layout.addWidget(self.ratifier_addPropBtn, 3,0,1,2)
        self.setLayout(layout)
    
    def clearInterface(self):
        self.sublist.clear()
        self.objlist.clear()
        
    def updateInterface(self):
        self.sublist.clear()
        self.objlist.clear()
        self.sublist.addItem('Any')
        for u in PolicyContent.uoa_user_set:
            self.sublist.addItem(u)
        self.objlist.addItem('Any')
        for u in PolicyContent.uoa_object_set:
            self.objlist.addItem(u)
#         self.sublist.addItems(PolicyContent.uoa_user_set.union(set(['Any'])))
#         self.objlist.addItems(PolicyContent.uoa_object_set.union(set(['Any'])))
        
    def retrievePropInfo(self):
        permissions = set()
        if self.cb_permR.isChecked(): permissions.add('r')
        if self.cb_permW.isChecked(): permissions.add('w')
        if self.cb_permX.isChecked(): permissions.add('x')
        perms = ','.join(permissions)
        if not perms: return None
        user = str(self.sublist.currentText())
        obj = str(self.objlist.currentText())
        decision = str(self.declist.currentText())
#         recursive = self.recurCheck.isChecked()
        return [user, obj, perms, decision, '-']
        
class Ui_Ratifier_PropertyTable(QTableWidget):
    
    def __init__(self, main, rowCount=0, colCount=5, parent=None):
        QTableWidget.__init__(self, rowCount, colCount, parent)
        self.main = main
        labels = ['User', 'Object', 'Action', 'Decision', 'Result']
        self.setHorizontalHeaderLabels(labels)
        self.horizontalHeader().setResizeMode(1, QHeaderView.Stretch)
        self.setStyleSheet("QTableView{ selection-background-color: rgba(255, 0, 0, 50); selection-color: black}")
        self.setUpdatesEnabled(True)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)

    def readContent(self):
        rules = []
        for i in xrange(self.rowCount()):
            arule = []
            for j in xrange(self.columnCount()):
                arule.append(str(self.item(i,j).text()))
            rules.append(arule)
        return rules

    def delProperty(self):
        rows = set()
        for i in self.selectedItems():
            rows.add(i.row())
        for r in rows:
            self.removeRow(r)
        self.repaint()

class Ui_PolicyRatifier(QSplitter):

    def __init__(self, orientation, parent, main):
        QSplitter.__init__(self, orientation, parent)
        self.main = main
        self.setupUi()

    def setupUi(self):
        gbRatifier_property = QGroupBox('Properties')
        layout = QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.ratifier_propertyTableWidget = Ui_Ratifier_PropertyTable(self.main)
        layout.addWidget(self.ratifier_propertyTableWidget, 0,0,1,4)
        self.ratifier_addPropBtn, self.ratifier_delPropBtn = QPushButton('Add'), QPushButton('Delete')
        self.ratifier_runPropBtn, self.ratifier_stopRunPropBtn = \
            QPushButton('Run'), QPushButton('Stop')
        layout.addWidget(self.ratifier_addPropBtn, 1,0,1,1)
        layout.addWidget(self.ratifier_delPropBtn, 1,1,1,1)
        layout.addWidget(self.ratifier_runPropBtn, 1,2,1,1)
        layout.addWidget(self.ratifier_stopRunPropBtn, 1,3,1,1)
        gbRatifier_property.setLayout(layout)
        '''to be enriched'''
        gbRatifier_result = QGroupBox('Ratifier Result')
        l = QVBoxLayout()
        self.ratifierTextEdit = QPlainTextEdit()
        self.ratifierTextEdit.setReadOnly(True)
        l.addWidget(self.ratifierTextEdit)
        gbRatifier_result.setLayout(l)
        self.addWidget(gbRatifier_property)
        self.addWidget(gbRatifier_result)
        self.ratifier_addProperty = Ui_PoliRatifier_AddProperty()
        self.ratifier_addProperty.ratifier_addPropBtn.clicked.connect(self.addProperty)
        self.ratifier_addPropBtn.clicked.connect(self.ratifier_addProperty.show)
        self.ratifier_delPropBtn.clicked.connect(self.ratifier_propertyTableWidget.delProperty)
        self.ratifier_runPropBtn.clicked.connect(self.runPropCheck)
        self.ratifier_stopRunPropBtn.clicked.connect(self.stopRunningPropCheck)

    def clearInterface(self):
        self.ratifier_propertyTableWidget.model().removeRows(0, self.ratifier_propertyTableWidget.rowCount())
        self.ratifierTextEdit.clear()
        self.ratifier_addProperty.clearInterface()
        
    def addProperty(self, prop):
        prop = self.ratifier_addProperty.retrievePropInfo()
        if not prop: 
            QMessageBox.warning(self.main, '', 'Please select at least one permission.')
            return
        perms = prop[2].split(',')
        for p in perms:
            row = self.ratifier_propertyTableWidget.rowCount() 
            col = 0
            self.ratifier_propertyTableWidget.insertRow(row)
            for i in xrange(len(prop)):
                item = QTableWidgetItem(p) if i==2 else QTableWidgetItem(prop[i])
                item.setTextAlignment(Qt.AlignCenter)
                self.ratifier_propertyTableWidget.setItem(row, col, item)
                col += 1
        self.ratifier_addProperty.hide()
        self.ratifier_propertyTableWidget.repaint()

    def genSMVFromPropertyRules(self, userPermRules):
        '''
        write to file for smv to call
        only contains the use-perm-rules
        '''
        lines = genSMVHeader()
        lines.append("\tRBAC_UserPerms: RBAC_UserPerms( USERS, OBJECTS, OPERATIONS, RECURSIVE);\n")
        lines.append("ASSIGN\n")
        lines.append("\tnext(USERS)\t\t:=\tUSERS;\n")
        lines.append("\tnext(ROLES)\t\t:=\tROLES;\n")
        lines.append("\tnext(OBJECTS)\t:=\tOBJECTS;\n")
        lines.append("\tnext(OPERATIONS):=\tOPERATIONS;\n\n")
        '''User detail part'''
        lines += genSMVUserPerms(userPermRules, True)
        f = open('./policies/temp.smv', 'w')
        for l in lines:
            f.write(l)
        f.close()
    
    def extractResultFromSMV(self, output):
        import re
        specs = {}
        for l in output.split('\n'):
            if 'specification' in l:
                user = re.sub('[()]', '', re.sub(r'USERS = (\S+).*', r'\1', l).split()[-1])
                obj = re.sub('[()]', '', re.sub(r'OBJECTS = (\S+).*', r'\1', l).split()[-1])
                perm = re.sub('[()]', '', re.sub(r'OPERATIONS = (\S+).*', r'\1', l).split()[-1])
                dec = re.sub('[()]', '', re.sub(r'decision = (\S+).*', r'\1', l).split()[-1])
                result = l[l.rfind(' ')+1:]
                if perm == 'execute': perm = 'x'
                elif perm == 'read': perm = 'r'
                else: perm = 'w'
                if user not in specs.keys():
                    specs[user] = {obj: [[perm, dec, result]]}
                else:
                    specs[user].setdefault(obj, []).append([perm, dec, result])
        table = self.ratifier_propertyTableWidget
        for i in xrange(table.rowCount()):
            user = str(table.item(i, 0).text())
            if user == 'Any': user = 'dummy'
            obj = str(table.item(i, 1).text()).replace('.', '__')
            obj = obj.replace('/', '_')
            if obj == 'Any': obj = 'dummy'
            perm = str(table.item(i, 2).text())
            dec = str(table.item(i, 3).text())
            for s in specs[user][obj]:
                if s[0] == perm and s[1] == dec:
                    result = s[2][0].upper()+s[2][1:]
                    item  = table.item(i, 4)
                    if result == 'False':
                        item.setForeground(Qt.red)
                    item.setText(result)
                    
    def runPropCheck(self):
        rules = self.ratifier_propertyTableWidget.readContent()
        self.genSMVFromPropertyRules(rules)
        result = self.main.policyVerifier.callSMV(self.ratifier_propertyTableWidget.rowCount())
        self.extractResultFromSMV(result)
    
    def stopRunningPropCheck(self):
        pid = self.main.policyVerifier.child_pid
        if pid is None: return False
        """ Check For the existence of a unix pid. """
        import os
        try:
            os.kill(pid, 0)
        except OSError:
            self.main.policyVerifier.child_pid = None
            return False
        else:
            self.main.policyVerifier.child_pid = None
            return True
