'''
Created on Aug 15, 2016

@author: manw
'''
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from Ui_ACvisual import *
from DiagramScene import DiagramScene
from DiagramView import DiagramView
from VisAnimation import VisAnimation
from PolicyVerifier import *
from PolicyVisTextFeedback import *
from Ui_Switch import Ui_Switch
import os, PolicyContent, Commons
from policyUOAIO import *
from SpecDialog import SpecDialog

class UserListWidget(QListWidget):
    def __init__(self, parent=None):
        QListWidget.__init__(self, parent) 
        self.main = parent
        self.visAnimation = parent.visAnimation
        p = self.palette()
        self.setPalette(p)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.itemClicked.connect(self.user_item_Clicked)
        
    def resetItemsVisual(self):
        for i in xrange(self.count()):
            item = self.item(i)
            item.setSelected(False)
            self.setItemTextBoldProp(item, False)
        self.repaint()
        
    def setItemTextBoldProp(self, item, isBold):
        font = item.font()
        font.setBold(isBold)
        item.setFont(font)
        
    def updateUserListWidget(self):
        self.clear()
        for u in PolicyContent.uoa_user_set:
            item = QListWidgetItem(u)
            self.addItem(item)
        
    def addItemText(self, text):
        item = QListWidgetItem(text)
        self.addItem(item)
        
    def delItemByText(self, text):
        for i in xrange(self.count()):
            content = str(self.item(i).text())
            if content==text: self.removeItem(i)
        
    def user_item_Clicked(self, item):
        if self.visAnimation.model == self.visAnimation.UNIX:
            self.visAnimation.initializeUNIX()
        else:
            self.setEditTriggers(QAbstractItemView.NoEditTriggers)
            self.visAnimation.resetRoleEdges()
            self.visAnimation.toggleEnableAnimSwitch()
            if self.visAnimation.perspective == self.visAnimation.USERVIEW:
                self.visAnimation.selectedUser = str(item.text())
                self.visAnimation.rbac_roles_direct = PolicyContent.rbac_usr_role_mat[self.visAnimation.selectedUser]
                self.visAnimation.initRBAC_userView()
                self.visAnimation.playStep = 0
                self.visAnimation.init = True
                self.visAnimation.animationStep()
            elif self.visAnimation.perspective == self.visAnimation.OBJVIEW: 
                self.visAnimation.target = None
                self.visAnimation.selectedUser = None
                if not self.visAnimation.selectedObj and len(self.selectedItems())>0:
                    self.clearSelection()
                    QMessageBox.warning(self.main, '', 'Please select an object first.')
                    return
                self.visAnimation.target = str(item.text())
                self.visAnimation.selectedUser = self.visAnimation.target
                self.visAnimation.playStep = 0
                self.visAnimation.init = False
                self.visAnimation.animationStep()
            else:
                user = str(item.text())
                self.visAnimation.resetRoleHighlight()
                self.visAnimation.selectedUser = user
                self.visAnimation.rbac_roles_direct = PolicyContent.rbac_usr_role_mat[self.visAnimation.selectedUser]
                nodes = set()
                for r in self.visAnimation.rbac_roles_direct:
                    rn = self.main.scene.roleHier.data2RoleNode[r]
                    nodes.add(rn)
                    rn.view.highlited = True
                    for ir in self.main.scene.roleHier.findAllChildren_BFS(rn):
                        ir.view.inhHighlighted = True
                self.main.visAnimation.accessibleObjectsRBAC = {}
                self.main.visAnimation.getUserRolePermForTextEdit(user)
                self.main.scene.update()

class MainWindow(QMainWindow):
    FONT_SIZE = 20
    def __init__(self):
        QMainWindow.__init__(self)
        os.chdir('./')
        self.userHighlightColors = [QColor(255,141,98, 127), QColor(0, 153, 255, 127)]
        self.setupUi()
        self.initParam()
        self.policyVerifier = PolicyVerifier(self)
        self.scene.setSceneRect(QRectF(self.view.geometry()))
        self.scene_obj.setSceneRect(QRectF(self.view_obj.geometry()))
        self.operationModeChanged(self.ui.actionPolicy_Composer)
        self.scene.resetScene()
        self.scene_obj.resetScene()
        self.activeScene = self.scene
        '''For visualization animation'''
        self.filter_perm = set(['r','w','x'])
        self.filter_permRelation = 0 # default being 0 means default to or initially
        self.visAnimation.reset()
        self.visAnimSettingChange()
        self.animSwitch.clicked.connect(self.visAnimSettingChange)
        self.animPrevBtn.clicked.connect(self.visAnimation.animationBackward)
        self.animNextBtn.clicked.connect(self.visAnimation.animationForward)
        self.animStopBtn.clicked.connect(self.visAnimation.reset)
        self.specDialog = SpecDialog(self)
        
    def updateAllItemPos(self):
        self.scene.updateCanvasDimension()
        self.scene_obj.updateCanvasDimension()
        self.scene.updateItemPos()
        self.scene_obj.updateItemPos()
      
    def addSearchBox(self):
        self.confirmSearchBtn = QPushButton('OK')
        self.searchLineEdit = QLineEdit()
        self.confirmSearchBtn.clicked.connect(self.confirmSearch)
        groupSearch = QGroupBox('Object Search')
        Commons.groupBoxTitle(groupSearch)
        groupSearch.setAlignment(Qt.AlignLeft)
        layout = QHBoxLayout(groupSearch)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(self.searchLineEdit)
        layout.addWidget(self.confirmSearchBtn)
        return groupSearch
        
    def addSODInterface(self):
        groupSOD = QGroupBox('Separation of Duties')
        Commons.groupBoxTitle(groupSOD)
        groupSOD.setAlignment(Qt.AlignLeft)
        layout = QGridLayout()
        layout.setContentsMargins(0,0,0,0)
        lab = QLabel('Exclusive Roles')
        layout.addWidget(lab, 0,0,1,1)
        self.SODTextEdit = QTextEdit()
        layout.addWidget(self.SODTextEdit, 1,0,1,1)
        lab = QLabel('Use one line for a group of roles')
        lab1 = QLabel('Use "," to separate exclusive roles within a group')
        layout.addWidget(lab,2,0,1,1)
        layout.addWidget(lab1,3,0,1,1)
        self.addSODBtn = QPushButton('Update')
        layout.addWidget(self.addSODBtn, 4,0,1,1)
        groupSOD.setLayout(layout)
        self.addSODBtn.clicked.connect(self.addRoleSOD)
        self.SODInterface = groupSOD
        self.SODInterface.setEnabled(False)
        return groupSOD
    
    def changeVisPerspective(self):
        self.visAnimation.reset()
        self.userListWidget.clearSelection()
        if self.radioBtn_userView.isChecked():
            self.visAnimation.perspective = self.visAnimation.USERVIEW
        elif self.radioBtn_objView.isChecked():
            self.visAnimation.perspective = self.visAnimation.OBJVIEW
        else:
            self.visAnimation.perspective = self.visAnimation.ROLEVIEW
        self.animSwitch.setEnabled(not self.radioBtn_roleView.isChecked())
        self.visualizationSplitter.widget(0).setVisible(self.animSwitch.isChecked())
        self.visualizationSplitter.widget(1).show()
        self.visualizerTextEdit.setColumnHidden(0, self.visAnimation.perspective == self.visAnimation.ROLEVIEW)
        self.SODInterface.setEnabled(self.visAnimation.perspective == self.visAnimation.ROLEVIEW)
        self.addRolePermBtn.setVisible(self.visAnimation.perspective == self.visAnimation.ROLEVIEW)
        self.delRolePermBtn.setVisible(self.visAnimation.perspective == self.visAnimation.ROLEVIEW)
        if self.visAnimation.perspective == self.visAnimation.ROLEVIEW:
            self.visualizationSplitter.widget(0).show()
            self.visualizationSplitter.widget(1).hide()
        self.visAnimation.toggleEnableAnimSwitch()
        self.resizeViews()

    def updateFilter(self):
        '''
        Store the setting of target permission to check against in vis
        1. OR relation: the set bits are stored as individual elements of the set
        2. AND relation: the set bits are combined as one single element of the set
        '''
        self.filter_perm = set()
        if self.filter_permsR.isChecked():
            self.filter_perm.add('r')
        if self.filter_permsW.isChecked():
            self.filter_perm.add('w')
        if self.filter_permsX.isChecked():
            self.filter_perm.add('x')
        self.filter_permRelation = self.filter_permCombineAND.isChecked()
        self.visAnimation.playStep = 0
        self.visAnimation.init = True
        self.visAnimation.animationStep()
        
    def checkInputPath(self, path):
        if len(path)>1 and path[-1] == '/':
            path = path[0:len(path)-1]
            self.searchLineEdit.setText(path)
        if path not in self.scene_obj.graph.nodeDict.keys():
            QMessageBox.warning(self, '', 'The specified object does not exist!')
            return None
        return path
    
    def confirmSearch(self):
        path = str(self.searchLineEdit.text())
        path = self.checkInputPath(path)
        if not path: return
        self.scene_obj.layout.setRoot(path)
        self.resizeViews()

    def setupToolbar(self):
        self.ui.toolBar.addAction(self.ui.actionNew)
        self.ui.actionNew.setIcon(QIcon(QPixmap('./icons/NewFile.png')))
        self.ui.toolBar.addAction(self.ui.actionImport)
        self.ui.actionImport.setIcon(QIcon(QPixmap('./icons/Importfile.png')))
        self.ui.toolBar.addAction(self.ui.actionExport)
        self.ui.actionExport.setIcon(QIcon(QPixmap('./icons/Exportfile.png')))
        
        self.ui.toolBar.addSeparator()
        self.ui.toolBar.addAction(self.ui.actionSpecification)
        self.ui.actionSpecification.setIcon(QIcon(QPixmap('./icons/spec.png')))

        self.ui.toolBar.addSeparator()
        self.ui.toolBar.addAction(self.ui.actionPolicy_Composer)
        self.ui.toolBar.addSeparator()
        
        self.ui.toolBar.addAction(self.ui.actionPolicy_Ratifier)
        self.ui.toolBar.addSeparator()
        self.ui.toolBar.addAction(self.ui.actionPolicy_VisAnalyzer)
        self.ui.toolBar.addSeparator()
        
        self.ui.actionZoomIn.setIcon(QIcon(QPixmap('./icons/Zoom-In.png')))
        self.ui.actionZoomOut.setIcon(QIcon(QPixmap('./icons/Zoom-Out.png')))
        self.ui.actionPan.setIcon(QIcon(QPixmap('./icons/pan.png')))
#         self.ui.toolBar.addAction(self.ui.actionPan)
        self.ui.toolBar.addAction(self.ui.actionZoomIn)
        self.ui.toolBar.addAction(self.ui.actionZoomOut)
        self.ui.toolBar.addSeparator()
        
        self.viewActionGroup = QActionGroup(self)
        self.viewActionGroup.addAction(self.ui.actionPolicy_Composer)
        self.viewActionGroup.addAction(self.ui.actionPolicy_Ratifier)
        self.viewActionGroup.addAction(self.ui.actionPolicy_VisAnalyzer)
        self.viewActionGroup.triggered.connect(self.operationModeChanged)
        self.ui.actionZoomIn.triggered.connect(self.zoomIn)
        self.ui.actionZoomOut.triggered.connect(self.zoomOut)
        self.ui.actionPan.triggered.connect(self.panObjgraph)
        self.ui.actionSpecification.triggered.connect(self.displayPolicy)
        self.ui.actionNew.triggered.connect(self.newPolicy)
        self.ui.actionImport.triggered.connect(self.importPolicy)
        self.ui.actionExport.triggered.connect(self.exportPolicy)
#         self.ui.addRoleDialog.addRoleBtn.clicked.connect(self.addRole)
#         self.ui.delRoleDialog.delRoleBtn.clicked.connect(self.delRole)
#         self.ui.assignUser2RoleDialog.assignBtn.clicked.connect(self.assignUser2Role)
#         self.addSODBtn.clicked.connect(self.addRoleSOD)
        
#     def addRole(self):
#         rolename = str(self.ui.addRoleDialog.addRoleLineEdit.text())
#         self.ui.addRoleDialog.hide()
#         
#     def delRole(self):
#         rolename = str(self.ui.delRoleDialog.delRoleCB.currentText())
#         self.ui.delRoleDialog.hide()
#         
#     def assignUser2Role(self, user, role):
#         user = str(self.ui.assignUser2RoleDialog.userCB.currentText())
#         role = str(self.ui.assignUser2RoleDialog.roleCB.currentText())
#         self.ui.assignUser2RoleDialog.hide()
        
    def addRoleSOD(self):
        self.visAnimation.resetRoleHighlight()
        self.visAnimation.resetRoleXorColorIndex()
        xroles = str(self.SODTextEdit.toPlainText()).strip()
        if xroles.count(',')==0: 
            self.userListWidget.resetItemsVisual()
            self.scene.update()
            return
        groups = xroles.split('\n')
        if not groups: 
            self.userListWidget.resetItemsVisual()
            self.scene.update()
            return
        xorRoleGroups = []
        SODUsers = set()
        i = 0
        for g in groups:
            gset = set(g.split(','))
            xorRoleSet = set()
            for r in gset:
                r = r.strip()
                self.scene.roleHier.data2RoleNode[r].view.xorColorIndex = i
                xorRoleSet.add(r)
            xorRoleGroups.append(xorRoleSet)
        '''assign same colors to roles within a xor group'''
        '''show the users that violates the SOD's'''
        for u in PolicyContent.uoa_user_set:
            if u in PolicyContent.rbac_usr_role_mat.keys():
                drole = PolicyContent.rbac_usr_role_mat[u]
                roles = set()
                for r in drole:
                    roles = roles.union(set([rr.data for rr in self.scene.roleHier.findAllChildren_BFS(self.scene.roleHier.data2RoleNode[r])]))
                    roles.add(r)
                roles = list(roles)
                for g in xorRoleGroups:
                    '''if there are two roles exist in one group, then it is violated'''
                    for i in xrange(len(roles)):
                        for j in xrange(i+1, len(roles)):
                            if roles[i] in g and roles[j] in g:
                                SODUsers.add(u)
        '''
        show users that violates the SOD in table:
        bold text of the users
        allow clicking the users, SOD roles should be in xor color still
        '''
        self.userListWidget.resetItemsVisual()     
        for i in xrange(self.userListWidget.count()):
            item = self.userListWidget.item(i)
            if str(item.text()) in SODUsers:
                self.userListWidget.setItemTextBoldProp(item, True)
        self.userListWidget.repaint()
        self.scene.update()
    
    def zoomIn(self):
        self.activeScene.bkrectChanged = True
        self.activeScene.scaleOffset *= 1.1
        if self.activeScene == self.scene:
            self.view.scale(1.1, 1.1)
            self.scene.update()
        elif self.activeScene == self.scene_obj:
            self.view_obj.scale(1.1, 1.1)
            self.scene_obj.update()
        
    def zoomOut(self):
        self.activeScene.bkrectChanged = True
        self.activeScene.scaleOffset *= 0.9
        if self.activeScene == self.scene:
            self.view.scale(0.9, 0.9)
            self.scene.update()
        elif self.activeScene == self.scene_obj:
            self.view_obj.scale(0.9, 0.9)
            self.scene_obj.update()
            
    def panObjgraph(self):
        pass
#         self.view_obj.translate(self.scene_obj.translateX, )
        
    def setupUi(self):
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        '''MAIN INTERFACE'''
        self.setupToolbar()
        
        self.view = DiagramView()
        self.scene = DiagramScene(self)
        self.view.scene = self.scene
        self.view.setScene(self.scene)
        self.view.setRenderHints(QPainter.Antialiasing)
        self.roleViewWidget = QWidget()
        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.view)
        self.roleViewWidget.setLayout(layout)
        '''object views'''
        self.view_obj = DiagramView()
        self.scene_obj = DiagramScene(self)
        self.view_obj.scene = self.scene_obj
        self.view_obj.setScene(self.scene_obj)
        self.view_obj.setRenderHints(QPainter.Antialiasing)
        self.objViewWidget = QWidget()
        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.view_obj)
        self.objViewWidget.setLayout(layout)
        
        self.visAnimation = VisAnimation(self)
        
        self.visualizerSplitter = QSplitter(Qt.Vertical, self.centralWidget())
        self.userListWidget = UserListWidget(self)
        
        groupUser = QGroupBox("User List")
        Commons.groupBoxTitle(groupUser)
        groupUser.setAlignment(Qt.AlignLeft)
        layout = QVBoxLayout(groupUser)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(self.userListWidget)
        self.groupUser = groupUser
        
        groupPerspective = QGroupBox("Perspective")
        Commons.groupBoxTitle(groupPerspective)
        groupPerspective.setAlignment(Qt.AlignLeft)
        layout = QVBoxLayout(groupPerspective)
        layout.setContentsMargins(0,0,0,0)
        self.radioBtn_userView = QRadioButton('User View')
        self.radioBtn_objView = QRadioButton('Object View')
        self.radioBtn_roleView = QRadioButton('Role View')
        layout.addWidget(self.radioBtn_userView)
        layout.addWidget(self.radioBtn_objView)
        layout.addWidget(self.radioBtn_roleView)
        self.radioBtn_userView.setChecked(True)
        self.radioBtn_userView.clicked.connect(self.changeVisPerspective)
        self.radioBtn_objView.clicked.connect(self.changeVisPerspective)
        self.radioBtn_roleView.clicked.connect(self.changeVisPerspective)
        
        groupFilter = QGroupBox("Filter")
        Commons.groupBoxTitle(groupFilter)
        groupFilter.setAlignment(Qt.AlignLeft)
        layout = QVBoxLayout(groupFilter)
        layout.setContentsMargins(0,0,0,0)
        
        group_filter_perm = QGroupBox('Permission Settings')
        layoutPerm = QVBoxLayout(group_filter_perm)
        layoutPerm.setContentsMargins(0,0,0,0)
        group_filter_permCombine = QGroupBox('Relation')
        layout3 = QHBoxLayout(group_filter_permCombine)
        layout3.setContentsMargins(0,0,0,0)
        self.filter_permCombineOR = QRadioButton('Or')
        self.filter_permCombineOR.setChecked(True)
        self.filter_permCombineAND = QRadioButton('And')
        layout3.addWidget(self.filter_permCombineOR)
        layout3.addWidget(self.filter_permCombineAND)
        
        group_filter_permPerms = QGroupBox('Permissions')
        layout4 = QVBoxLayout(group_filter_permPerms)
        layout4.setContentsMargins(0,0,0,0)
        self.filter_permsR = QCheckBox('Read')
        self.filter_permsW = QCheckBox('Write')
        self.filter_permsX = QCheckBox('Execute')
        self.filter_permsR.setChecked(True)
        self.filter_permsW.setChecked(True)
        self.filter_permsX.setChecked(True)
        layout4.addWidget(self.filter_permsR)
        layout4.addWidget(self.filter_permsW)
        layout4.addWidget(self.filter_permsX)
        
        layoutPerm.addWidget(group_filter_permCombine)
        layoutPerm.addWidget(group_filter_permPerms)
        self.group_filter_perm = group_filter_perm
        layout.addWidget(group_filter_perm)
        self.filter_update = QPushButton('Update')
        self.filter_update.clicked.connect(self.updateFilter)
        layout.addWidget(self.filter_update)
        
        '''Animation Settings'''
        animationGroup = QGroupBox()
        layout = QGridLayout()
        layout.setContentsMargins(0,0,0,0)
        animLabel = QLabel("Animation")
        self.animSwitch = Ui_Switch(thumb_radius=11, track_radius=8)
#         self.animSwitch.toggled.connect(self.visAnimation.toggleEnableAnimSwitch)
        groupAnimCtrl = QGroupBox()
        self.animPrevBtn = QPushButton('Prev')
        self.animStopBtn = QPushButton('STOP')
        self.animNextBtn = QPushButton('Next')
        ll = QHBoxLayout(groupAnimCtrl)
        ll.setContentsMargins(0,0,0,0)
        ll.addWidget(self.animPrevBtn)
        ll.addWidget(self.animNextBtn)
#         ll.addWidget(self.animStopBtn)
        layout.addWidget(animLabel, 0,0,1,1)
        layout.addWidget(self.animSwitch, 0,1,1,1)
        layout.addWidget(groupAnimCtrl, 1,0,1,2)
        animationGroup.setLayout(layout)
        
        self.groupControlWidget = QWidget()
        self.groupControlWidget.setMaximumWidth(320)
        self.groupControlWidget.setMinimumWidth(320)
        l1 = QHBoxLayout()
        l1.setContentsMargins(0, 0, 0, 0)
        self.groupControl = QGroupBox()
        self.scroll = QScrollArea(self.groupControlWidget)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setWidget(self.groupControl)
        layout = QVBoxLayout()
        layout.setContentsMargins(0,0,0,0)
        layout.setSizeConstraint( QLayout.SetFixedSize)
        layout.addWidget(groupPerspective)
        layout.addWidget(self.addLineToLayout())
        layout.addWidget(groupFilter)
        layout.addWidget(self.addLineToLayout())
        layout.addWidget(self.addSearchBox())
        layout.addWidget(self.addLineToLayout())
        layout.addWidget(animationGroup)
        layout.addWidget(self.addLineToLayout())
        layout.addWidget(self.addSODInterface())
        self.groupControl.setLayout(layout)
        l1.addWidget(self.scroll)
        self.groupControlWidget.setLayout(l1)
        
        group1 = QGroupBox("Role Hierarchy")
        Commons.groupBoxTitle(group1)
        group1.setAlignment(Qt.AlignLeft)
        gridLayout = QGridLayout()
        gridLayout.setContentsMargins(0,0,0,0)
        gridLayout.addWidget(self.roleViewWidget)
        group1.setLayout(gridLayout)
        group2 = QGroupBox("Directory Tree")
        Commons.groupBoxTitle(group2)
        group2.setAlignment(Qt.AlignLeft)
        gridLayout = QGridLayout()
        gridLayout.setContentsMargins(0,0,0,0)
        gridLayout.addWidget(self.objViewWidget)
        group2.setLayout(gridLayout)
        
        self.visualizationSplitter = QSplitter(Qt.Horizontal)
        self.visualizationSplitter.addWidget(group1)
        self.visualizationSplitter.addWidget(group2)
        
        self.splitter1 = QWidget(self.visualizerSplitter)
        l1 = QHBoxLayout(self.splitter1)
        l1.setContentsMargins(0,0,0,0)
        self.splitter2 = QSplitter(Qt.Horizontal, self.splitter1)
        l1.addWidget(self.groupControlWidget)
        l1.addWidget(self.splitter2)
        self.splitter2.addWidget(groupUser)
        self.splitter2.addWidget(self.visualizationSplitter)
        self.visualizerSplitter.addWidget(self.splitter1)
        self.visualizerTextEdit = PolicyVisTextFeedback(self)
        visualizerWidget = QWidget()
        layout = QGridLayout(visualizerWidget)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(self.visualizerTextEdit, 0,0,1,2)
        self.addRolePermBtn = QPushButton('Add Permission')
        self.delRolePermBtn = QPushButton('Delete Permission')
        self.addRolePermBtn.setVisible(False)
        self.delRolePermBtn.setVisible(False)
        layout.addWidget(self.addRolePermBtn, 1,0,1,1)
        layout.addWidget(self.delRolePermBtn, 1,1,1,1)
        visualizerWidget.setLayout(layout)
        self.visualizerSplitter.addWidget(visualizerWidget)
        self.addRolePermDialog = Ui_RolePermAdd()
        self.addRolePermBtn.clicked.connect(self.addRolePermDialog.show)
        self.delRolePermBtn.clicked.connect(self.visualizerTextEdit.delRolePerm)
        self.addRolePermDialog.vis_addRolePermBtn.clicked.connect(self.visualizerTextEdit.addRolePerm)
        
    def visAnimSettingChange(self):
        self.visAnimation.toggleEnableAnimSwitch()
        flag = self.animSwitch.isChecked()
        self.visualizationSplitter.widget(0).setVisible(flag)
        self.visAnimation.playStep = 0
        self.visAnimation.init = False
        if self.scene.roleHier:
            self.visAnimation.resetAnimVisual()
            self.visAnimation.animationStep()
        self.resizeViews()
        self.visAnimation.updateScenes()
        
    def addLineToLayout(self):
        lineA = QFrame()
        lineA.setFrameShape(QFrame.HLine)
        lineA.setFrameShadow(QFrame.Sunken)
        return lineA
  
    def resizeEvent(self, event):
        QMainWindow.resizeEvent(self, event)
        self.resizeViews()
    
    def resizeViews(self):
        self.ui.policyRatifierWidget.setGeometry(0, 0, self.centralWidget().geometry().width(), self.centralWidget().geometry().height())
        ratioVisRatifier = 0.5
        currentSizes = self.ui.policyRatifierWidget.sizes()
        currentSizes[0] = ratioVisRatifier*self.centralWidget().geometry().height()
        currentSizes[1] = (1-ratioVisRatifier)*self.centralWidget().geometry().height()
        self.ui.policyRatifierWidget.setSizes(currentSizes)
        self.ui.policyComposerWidget.setGeometry(0, 0, self.centralWidget().geometry().width(), self.centralWidget().geometry().height())
        self.visualizerSplitter.setGeometry(0, 0, self.centralWidget().geometry().width(), self.centralWidget().geometry().height())
        ratioVisualizer = 0.75
        currentSizes = self.visualizerSplitter.sizes()
        currentSizes[0] = ratioVisualizer*self.centralWidget().geometry().height()
        currentSizes[1] = (1-ratioVisualizer)*self.centralWidget().geometry().height()
        self.visualizerSplitter.setSizes(currentSizes)
        raitoRole = 0.1
        currentSizes[0] = raitoRole*self.centralWidget().geometry().width()
        currentSizes[1] = (1-raitoRole)*self.centralWidget().geometry().width()
        self.splitter2.setSizes(currentSizes)
        ratioVisualization = 0.34
        currentSizes[0] = ratioVisualization*self.visualizationSplitter.geometry().width()
        currentSizes[1] = (1-ratioVisualization)*self.visualizationSplitter.geometry().width()
        self.visualizationSplitter.setSizes(currentSizes)
        self.updateAllItemPos()
        self.scene_obj.bkrectChanged = True
        
    def createPolicyComposerUNIXGroup(self):
        leftwidget = QWidget()
        rightwidget = QWidget()
        self.composerUNIXSplitter.addWidget(leftwidget)
        self.composerUNIXSplitter.addWidget(rightwidget)
    
    def closeEvent(self, evt):
        QApplication.closeAllWindows()
        
    def initParam(self):
        '''policy content'''
        self.currentDir = os.path.abspath('./policies')
        self.uoapolicy = self.currentDir+'/'
        self.policyChanged = False
        self.root_dir = '/'
        PolicyContent.init()
        
    def displayPolicy(self):
        if self.uoapolicy:
            self.specDialog.readSpecToDialog(self.uoapolicy)
            self.specDialog.show()
        else:
            QMessageBox.warning(self, '', 'Please import a policy file first.')
            
    def newPolicy(self):
        self.initParam()
        self.scene.resetScene()
        self.scene_obj.resetScene()
        self.activeScene = self.scene
        self.visAnimation.reset()
        self.clearInterface()
        self.uoapolicy = ''
            
    def importPolicy(self):
#         addRule2SMVPolicy('./policies/InheritanceFlat', 'role4', 'role3')
#         addRule2SMVPolicy('./policies/InheritanceFlat', 'role0', 'role1')
        filename = QFileDialog.getOpenFileName(self, 'Import from UOA specification file', directory=self.currentDir, filter='Policy File (*.uoa)')
        if filename:
            self.initParam()
            self.scene.resetScene()
            self.scene_obj.resetScene()
            self.activeScene = self.scene
            self.visAnimation.reset()
            self.clearInterface()
            self.uoapolicy = str(filename)
            try:
                policyUOAIO(self.uoapolicy)
                convert2RBAC()
            except Exception as e:
                QMessageBox.critical(self, 'Error', str(e))
                return -1
            self.setWindowTitle(os.path.basename(self.uoapolicy))
            self.ui.policyComposerWidget.pushPolicyInfo2UI()
            self.scene.createRoleGraph()
            self.scene_obj.createObjGraph(PolicyContent.oscrawlfile)
            generateSMVPolicy(self.uoapolicy)
            self.updateInterface()
            self.policyChanged = False
            return 0
        
    def exportPolicy(self):
        filename = QFileDialog.getSaveFileName(self, 'Export to UOA specification file', directory=self.currentDir, filter='(*.uoa);;All Files(*.*)')
        if not filename.isEmpty():
            try:
                userset, objset, rulelist = self.ui.policyComposerWidget.retrievePolicyInfoFromUI()
                write2UOAFile(str(filename), rulelist)
            except Exception as e:
                QMessageBox.critical(self, 'Error', str(e))
                return
            
    def updatePolicy(self):
        if self.uoapolicy or self.policyChanged:
            temppolicy = self.uoapolicy[:self.uoapolicy.rfind('.')]+'_temp.uoa' if self.uoapolicy \
                        else self.currentDir+'/'+'_temp.uoa'
            PolicyContent.init()
            PolicyContent.uoa_user_set, PolicyContent.uoa_object_set, rules = self.ui.policyComposerWidget.retrievePolicyInfoFromUI()
            write2UOAFile(temppolicy, rules)
            policyUOAIO(temppolicy)
            convert2RBAC()
            '''update role and object graph'''
            self.scene.clear()
            self.scene_obj.clear()
            self.scene.createRoleGraph()
            self.scene_obj.createObjGraph()
            generateSMVPolicy(self.uoapolicy)
            
    def updatePolicyAndInterface(self):
        if self.policyChanged:
            self.updatePolicy()
            self.updateInterface()
            self.policyChanged = False
    
    def updateInterface(self):
        self.ui.policyRatifierWidget.ratifier_addProperty.updateInterface()
        self.userListWidget.updateUserListWidget()
    
    def clearInterface(self):
        self.ui.policyComposerWidget.clearInterface()
        self.ui.policyRatifierWidget.clearInterface()
        self.userListWidget.clear()
        self.addRolePermDialog.clearInterface()
        self.SODTextEdit.clear()
        
    def operationModeChanged(self, action):
        self.ui.policyComposerWidget.setVisible(action == self.ui.actionPolicy_Composer)
        self.ui.policyRatifierWidget.setVisible(action == self.ui.actionPolicy_Ratifier)
        self.visualizerSplitter.setVisible(action == self.ui.actionPolicy_VisAnalyzer)
        self.updatePolicyAndInterface()
        self.resizeViews()
