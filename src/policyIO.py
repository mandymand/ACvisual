"""
file:   policyIO.py
author: manwang
description:
        Functions for converting an access control policy into a 
        set of data structures representing the policy.
"""
from collections import namedtuple
from policyParse import policyParse

'''RBAC'''
Permset = namedtuple('Permset', ('perms', 'recursive'))
RoleNode = namedtuple('RoleNode', ('parents', 'children'))

'''Unix'''
ObjectCred = namedtuple('ObjCred', ('directory', 'user', 'group'))
ObjectPermUnix = namedtuple('ObjectPermUnix', ('recursive', 'userperm', 'groupperm', 'otherperm'))

root_dir = '/'
'''UNIX'''
unix_usr_grp_mat = {}
unix_res_own_mat = {}
unix_res_perm_mat = {}
'''RBAC'''
rbac_usr_role_mat = {}
rbac_role_res_mat = {}
rbac_role_inher_mat = {}
    
def makepermset(iterable=None, recursive=False):
    perms = set() if iterable is None else set(iterable)
    return Permset(perms, recursive)

def makerolenode(parents=None, children=None):
    parentset = set() if parents is None else set(parents)
    childset = set() if children is None else set(children)
    return RoleNode(parentset, childset)

def makeobjectcred(directory=False, user=None, group=None):
    return ObjectCred(directory, user, group)

def makeobjpermunix(recursive=False, userperm=[], groupperm=[], otherperm=[]):
    return ObjectPermUnix(recursive, userperm, groupperm, otherperm)

def formatForUNIX(stmt, obj_cred_mat, obj_perm_mat):
    permall = stmt[0]
    if permall[0] == 'd':
        is_dir = True
    else:
        is_dir = False
    permuser = permall[1]
    permgroup = permall[2]
    permother = permall[3] 
    user = stmt[1]
    group = stmt[2]
    if stmt[3] == '-r':
        is_recur = True
    else:
        is_recur = False
    obj = stmt[-1]
    obj_cred_mat[obj] = makeobjectcred(is_dir, user, group)
    obj_perm_mat[obj] = makeobjpermunix(is_recur, permuser, permgroup, permother)

                
def parseUNIXStmt(stmt):
    keyword =  stmt[0]
    if keyword == 'user:':
        unix_user_list = stmt[1]
        for u in unix_user_list:
            unix_usr_grp_mat[u] = set([])
    elif keyword == 'group:':
        group = stmt[1]
        users = stmt[2]
        for u in users:
            unix_usr_grp_mat[u].add(group)
    elif keyword == 'object:':
        formatForUNIX(stmt[1:], unix_res_own_mat, unix_res_perm_mat)
        
def parseRBACStmt(stmt):
    stmttype = stmt[0]
    if stmttype == 'user:':
        roles, users = stmt[1:]
        for user in users:
            try:
                rbac_usr_role_mat[user].update(roles)
            except KeyError:
                rbac_usr_role_mat[user] = set(roles)
    elif stmttype == 'free_user:':
        users = stmt[1]
        for user in users:
            rbac_usr_role_mat[user] = set() 
    elif stmttype == 'object:':
        if isinstance(stmt[1], str):
            resource = stmt[1]
            role = 'None'
            permset = makepermset(set(), recursive=False)
            rbac_role_res_mat[role] = {resource: permset}
        else:
            roles, perms, resource = stmt[1], stmt[2], stmt[-1]
            if resource[-1]=='/' and resource!='/':
                resource = resource[:-1]
            if len(stmt) == 5:
                permset = makepermset(perms, recursive=True)
            else:
                permset = makepermset(perms, recursive=False)
            for role in roles:
                try:
                    resourcedict = rbac_role_res_mat[role]
                    resourcedict[resource] = permset
                except KeyError:
                    # role has no resources
                    rbac_role_res_mat[role] = {resource: permset}
    elif stmttype == 'inheritance:':
        if len(stmt) == 4:
            parent, child = stmt[1], stmt[3]
        else:
            parent = stmt[1]
            child = None
        try:
            parentnode = rbac_role_inher_mat[parent]
            parentnode.children.add(child)
        except KeyError:
            rbac_role_inher_mat[parent] = makerolenode(children=(child,))
            try:
                childnode = rbac_role_inher_mat[child]
                childnode.parents.add(parent)
            except KeyError:
                rbac_role_inher_mat[child] = makerolenode(parents=(parent,))
    else:
        raise ValueError('unknown statement type: {0}'.format(stmttype))
           
def policyIO(source):
    '''
    Initialize an RBAC policy representation.
    Args:
        source (file or filename): The RBAC policy file
        
    Returns: 
        root_dir (string): root directory.
            the directory to start from to extract information on the real OS
        For UNIX model:
            unix_usr_grp_mat (dict): user-to-group assignment.
                unix_usr_grp_mat['user'] returns the groups the user is assigned to
            unix_res_own_mat (dict): contains the directory flag and the user and group owner info of objects.
                unix_res_own_mat['/path/to/obj'] returns a namedtuple with fields of a flag for whether the object 
                    is a directory, the user owner, and the group owner of the object.
            unix_res_perm_mat (dict): contains the permission bits of the objects.
                unix_res_perm_mat['/path/to/obj'] returns a namedtuple with fields of a flag for the permission 
                    recursiveness, the permission bits for identities in user, group and other categories.
                    The permission bits for each category are represented in list in the order of rwx.
        For RBAC model:
            rbac_usr_role_mat (dict): user-to-role assignment.
                rbac_usr_role_mat['role'] returns the list of users assigned to the role.
            rbac_role_res_mat (dict): role permissions on objects.
                rbac_role_res_mat['role']: returns the permission the role has to objects. The values 
            user permissions on objects.
                user_obj_perm_mat['user']['/path/to/obj']: returns a namedtuple that has flag of directory,
                    recursiveness of the permission and the permission list.
                group_obj_perm_mat['group']['/path/to/obj']: returns a namedtuple that has flag of directory,
                    recursiveness of the permission and the permission list.
            rbac_role_inher_mat (dict): 
    '''
    try:
        text = source.read()
    except AttributeError:
        with open(source, 'r') as f:
            text = f.read()
    stmts = policyParse(text)
    
    for policy in stmts:
        if policy[0] == 'root:':
            root_dir = policy[1]
        if policy[0] == 'UNIX':
            for i in xrange(1, len(policy)):
                parseUNIXStmt(policy[i])
        if policy[0] == 'RBAC':
            for i in xrange(1, len(policy)):
                parseRBACStmt(policy[i])
#     print root_dir
#     print unix_usr_grp_mat
#     print unix_res_own_mat
#     print unix_res_perm_mat  
#     printUserRoleAssign()
#     printRolePerm()
#     printRoleInheritance()
    return root_dir, unix_usr_grp_mat, unix_res_own_mat, unix_res_perm_mat, \
        rbac_usr_role_mat, rbac_role_res_mat, rbac_role_inher_mat

def printUserRoleAssign():
    print '== RBAC User Role Assignment Matrix =='
#     print rbac_usr_role_mat
    for key, value in rbac_usr_role_mat.items():
        print 'user = {0}'.format(key)
        print '    roles = {0}'.format(','.join(value))
        
def printRolePerm():
    print '== RBAC Role Object Permission matrix =='
    print rbac_role_res_mat
    for key, value in rbac_role_res_mat.items():
        print 'role = {0}'.format(key)
        for kk, vv in value.items():
            print '    res = {0}'.format(kk)
            print '        recursive = {0}'.format(vv.recursive)
            print '        perm = {0}'.format(','.join(vv.perms))
            
def printRoleInheritance():
    print '== RBAC Role Inheritance matrix =='
    print rbac_role_inher_mat
    for key, value in rbac_role_inher_mat.items():
        print 'role = {0}'.format(key)
        print '    parents = {0}'.format(','.join(value.parents))
        print '    children = {0}'.format(','.join(value.children))
        
if __name__ == '__main__':
    import sys
#     if len(sys.argv)<2:
#         source = sys.stdin
#     else:
#         source = sys.argv[1]
#     root, unix_usr_grp_mat, unix_res_own_mat, unix_res_perm_mat, \
#     rbac_usr_role_mat, rbac_role_res_mat, rbac_role_inher_mat = policyIO('./policies/ZNOUSE_test.ac')
    policyIO('./policies/ZNOUSE_test.ac')
    print '== Root directory =='
    print root_dir
    print '== User assignment to group =='
    for key, value in unix_usr_grp_mat.items():
        print 'user = {0}'.format(key)
        print '    groups = {0}'.format(','.join(value))
#     print unix_usr_grp_mat
    print '== Object credential matrix =='
#     print unix_res_own_mat
    for res, creddict in unix_res_own_mat.items():
        print 'res = {0}'.format(res)
        print '    directory = {0}'.format(creddict.directory)
        print '        user = {0}'.format(creddict.user)
        print '        group = {0}'.format(creddict.group)
    print '== Object permission matrix =='
#     print unix_res_perm_mat
    for res, permdict in unix_res_perm_mat.items():
        print 'res = {0}'.format(res)
        print '    recursive = {0}'.format(permdict.recursive)
        print '     permUser = {0}'.format(','.join(permdict.userperm))
        print '     permGroup = {0}'.format(','.join(permdict.groupperm))
        print '     permOther = {0}'.format(','.join(permdict.otherperm))
    print '=============================='
    printUserRoleAssign()
    printRolePerm()
    printRoleInheritance()
