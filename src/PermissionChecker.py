'''
Created on Jul 21, 2018

@author: manw
'''
import os, grp, PolicyContent
from collections import namedtuple

RBACPermInfo = namedtuple('RBACPermInfo',('user', 'role', 'obj', 'permset', 'recursive','original'))
UNIXPermInfo  = namedtuple('UNIXPermInfo', ('obj', 'user', 'group', 'permbits'))
UOARuleInfo = namedtuple('UOARuleInfo',('user', 'obj', 'permset', 'recursive', 'line'))

def getGroupsOfUser(user):
    import subprocess
    output = subprocess.Popen(["groups", user], \
                              stdout=subprocess.PIPE).communicate()[0]
    startIndex = output.find(':')
    if  startIndex != -1:
        groups = output[startIndex+2:-1].split(' ')
    else:
        groups = output[:-1].split(' ')
    return groups

def getUserAndGroupListOnSystem(rootdir, progDir):
    from sys import platform as _platform
    userset, groupset, user_group_sys_mat = set(), set(), {}
    if _platform == "darwin":
        import pwd
        for p in pwd.getpwall():
            if p[0].find('_')==-1:
                userset.add(p[0])
        groups = grp.getgrall()
        counter = 0
        for group in groups:
            g = group[0]
            if g.find('_')==-1:
                groupset.add(g)
            for user in group[3]:
                if user.find('_')==-1:
                    userset.add(user)
            counter+=1
        userset.remove('junt') # to remove
        for u in userset:
            usergroup = getGroupsOfUser(u)
            toRemove = set()
            for g in usergroup:
                if g.find('_')!=-1:
                    toRemove.add(g)
            for r in toRemove:
                usergroup.remove(r)
            if usergroup:
                user_group_sys_mat.update({u: set(usergroup)})
    else:
        import subprocess
        '''get all users'''
        output = subprocess.Popen(["cut", "-d:", "-f1", "/etc/passwd"], \
                                  stdout=subprocess.PIPE).communicate()[0]
        userset = set(output.split('\n'))
        userset = set(x for x in userset if x != '')
        userset.remove('junt') # to remove
        '''get all groups'''
        output = subprocess.Popen(["cut", "-d:", "-f1", "/etc/group"], \
                                  stdout=subprocess.PIPE).communicate()[0]
        groupset = set(output.split('\n'))
        groupset = set(x for x in groupset if x != '')
        '''assign user to groups'''
        for u in userset:
            usergroup = getGroupsOfUser(u)
            if usergroup:
                user_group_sys_mat.update({u: set(usergroup)})
    os.chdir(progDir)
    return list(userset), list(groupset), user_group_sys_mat

def permissionAllowed(filterPermset, filterRelation,  permset):
    if filterRelation==0: #Or relation
        if filterPermset & permset:
            return True
        else: 
            return False
    elif filterRelation==1: #And relation
        return permset.issuperset(filterPermset)

def removeDupRule_user(user_rule_mat):
    user_rule_mat_dup = {}
    for k, v in user_rule_mat.iteritems():
        print v
    return user_rule_mat_dup
    
def removePermissionUnnecessrayInInheritance(permission):
    '''remove the inherited rules if no additional permission is added using that role'''
    permission.sort(key=lambda x: x.original, reverse=True)
    permissionCurrent = set()
    toRemove = []
    for p in permission:
        if p.permset - permissionCurrent:
            permissionCurrent = permissionCurrent.union(p.permset)
        else:
            toRemove.append(p)
    for t in toRemove:
        permission.remove(t)
    return permission
                
def retrieveRBACPermInfo(user, role, orig, obj=None, ignoreRole=False):
    perms = []
    if not isinstance(role, basestring):
        role = role.data
    if role in PolicyContent.rbac_role_res_mat.keys():
        for path, perm in PolicyContent.rbac_role_res_mat[role].iteritems():
            if (obj==None) or (path==obj) or (path in obj and perm.recur_perms):
                if ignoreRole:
                    if perm.recur_perms:
                        perms.append(RBACPermInfo(user, '', path, perm.recur_perms, True, orig))
                    if perm.nonrecur_perms:
                        perms.append(RBACPermInfo(user, '', path, perm.nonrecur_perms, False, orig))
                else:
                    if perm.recur_perms:
                        perms.append(RBACPermInfo(user, role, path, perm.recur_perms, True, orig))
                    if perm.nonrecur_perms:
                        perms.append(RBACPermInfo(user, role, path, perm.nonrecur_perms, False, orig))

    return perms

def retrieveRBACUserInfo(obj, role, permset, recursive, orig):
    perms = []
    if not isinstance(role, basestring):
        role = role.data
    for user, roles in PolicyContent.rbac_usr_role_mat.iteritems():
        if role in roles:
            perms.append(RBACPermInfo(user, role, obj, permset, recursive, orig))
    return perms

if __name__ == '__main__':
    '''test for or'''
#     filterPermset =set(['x'])
#     permset = set(['w'])
#     permset1 = set(['w','x'])
#     permset2 = set(['r','x'])
#     print permissionAllowed(filterPermset, 0, permset)
#     print permissionAllowed(filterPermset, 0, permset1)
#     print permissionAllowed(filterPermset, 0, permset2)
    filterPermset =set(['x'])
    permset = set(['r','w'])
    permset1 = set(['w','x'])
    permset2 = set(['r','x'])
    print permissionAllowed(filterPermset, 1, permset)
    print permissionAllowed(filterPermset, 1, permset1)
    print permissionAllowed(filterPermset, 1, permset2)
