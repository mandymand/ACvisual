'''
Created on Jul 21, 2018

@author: manw
'''
from PyQt4.QtGui import QTableWidget, QTableWidgetItem, QHeaderView, QAbstractItemView
from PyQt4.Qt import *
import PolicyContent, policyUOAIO, copy

class Ui_RolePermAdd(QDialog):
    
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi()
        
    def createComboBox(self, content=None):
        w = QComboBox()
        if content:
            w.addItems(content)
        return w

    def setupUi(self):
        self.resize(300, 100)
        layout = QGridLayout()
        
        groupObj = QGroupBox('Object')
        self.objlist = self.createComboBox()
        l2 = QVBoxLayout()
        l2.addWidget(self.objlist)
        groupObj.setLayout(l2)
        layout.addWidget(groupObj, 0,0,1,1)
        
        self.recurCheck = QCheckBox('Recursive')
        self.recurCheck.setCheckable(True)
        self.recurCheck.setChecked(False)
        layout.addWidget(self.recurCheck, 1,0,1,1)
        
        groupAct = QGroupBox('Action')
        ll = QGridLayout()
        ll.setContentsMargins(0, 0, 0, 0)
        
        g2 = QGroupBox()
        l2 = QVBoxLayout()
        self.cb_permR = QCheckBox('Read')
        self.cb_permW = QCheckBox('Write')
        self.cb_permX = QCheckBox('Execute')
        l2.addWidget(self.cb_permR)
        l2.addWidget(self.cb_permW)
        l2.addWidget(self.cb_permX)
        g2.setLayout(l2)
        ll.addWidget(g2, 0,0,1,1)
        groupAct.setLayout(ll)
        layout.addWidget(groupAct, 0,1,1,1)
        
        self.vis_addRolePermBtn = QPushButton('Add')
        layout.addWidget(self.vis_addRolePermBtn, 2,0,1,2)
        self.setLayout(layout)
        
    def retrieveAddRolePermInfo(self):
        permissions = set()
        if self.cb_permR.isChecked(): permissions.add('r')
        if self.cb_permW.isChecked(): permissions.add('w')
        if self.cb_permX.isChecked(): permissions.add('x')
        perms = ','.join(permissions)
        if not permissions: return None
        obj = str(self.objlist.currentText())
        recursive = 'True' if self.recurCheck.isChecked() else 'False'
        return [obj, perms, recursive], permissions
    
    def clearInterface(self):
        self.objlist.clear()
        
class PolicyVisTextFeedback(QTableWidget):
    def __init__(self, main, rowCount=0, colCount=6, parent=None):
        QTableWidget.__init__(self, rowCount, colCount, parent)
        self.scene = main.scene
        self.main = main
        self.setColumnHidden(4,True)
        self.setColumnHidden(5,True)
        labels = ['Object', 'User Owner', 'Group Owner', 'Permission Bits']
        self.setHorizontalHeaderLabels(labels)
        self.setStyleSheet("QTableView{ selection-background-color: rgba(255, 0, 0, 50); selection-color: black}")
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        
    def fillinPermDetail(self, inputPerms={}, isUNIX=True):
        self.setColumnHidden(4,isUNIX)
        self.setColumnHidden(5,isUNIX)
        if isUNIX:
            self.fillinPermDetailUNIX(inputPerms)
            labels = ['Object', 'User Owner', 'Group Owner', 'Permission Bits']
            self.horizontalHeader().setResizeMode(0, QHeaderView.Stretch)
        else:
            self.fillinPermDetailRBAC(inputPerms)
            labels = ['User', 'Role', 'Resource', 'Permissions', 'Recursive', 'Inheritance']
            self.horizontalHeader().setResizeMode(2, QHeaderView.Stretch)
        self.setHorizontalHeaderLabels(labels)
    
    def fillinPermDetailUNIX(self, inputPerms):
        accessibleObjects = inputPerms
        self.clearContents()
        rowCount = 0
        for u in accessibleObjects.keys():
            rowCount += len(accessibleObjects[u])
        self.setRowCount(rowCount)
        row = 0
        for u in accessibleObjects.keys():
            for p in accessibleObjects[u]:
                item = QTableWidgetItem(p.obj)
                item.setTextAlignment(Qt.AlignCenter)
                self.setItem(row, 0, item)
                item = QTableWidgetItem(p.user)
                item.setTextAlignment(Qt.AlignCenter)
                self.setItem(row, 1, item)
                item = QTableWidgetItem(p.group)
                item.setTextAlignment(Qt.AlignCenter)
                self.setItem(row, 2, item)
                item = QTableWidgetItem(p.permbits)
                item.setTextAlignment(Qt.AlignCenter)
                self.setItem(row, 3, item)
                row += 1
                
    def fillinPermDetailRBAC(self, inputPerms):
        accessibleObjects = inputPerms
        self.clearContents()
        rowCount = 0
        for u in accessibleObjects.keys():
            rowCount += len(accessibleObjects[u])
        self.setRowCount(rowCount)
        row = 0
        for u in accessibleObjects.keys():
            for p in accessibleObjects[u]:
                item = QTableWidgetItem(p.user)
                item.setTextAlignment(Qt.AlignCenter)
                self.setItem(row, 0, item)
                item = QTableWidgetItem(p.role)
                item.setTextAlignment(Qt.AlignCenter)
                self.setItem(row, 1, item)
                item = QTableWidgetItem(p.obj)
                item.setTextAlignment(Qt.AlignCenter)
                self.setItem(row, 2, item)
                item = QTableWidgetItem(QTableWidgetItem(','.join(sorted(p.permset))))
                item.setTextAlignment(Qt.AlignCenter)
                self.setItem(row, 3, item)
                item = QTableWidgetItem(QTableWidgetItem(str(p.recursive)))
                item.setTextAlignment(Qt.AlignCenter)
                self.setItem(row, 4, item)
                item = QTableWidgetItem(QTableWidgetItem("Original" if p.original else "Inherited"))
                item.setTextAlignment(Qt.AlignCenter)
                self.setItem(row, 5, item)
                row += 1

    def addRolePerm(self):
        aperm, permset = self.main.addRolePermDialog.retrieveAddRolePermInfo()
        role = self.scene.clickedRoleNode.name
        row = self.rowCount()
        col = 1
        aperm.insert(0, role)
        aperm.append('Original')
        self.insertRow(row)
        for p in aperm:
            item = QTableWidgetItem(p)
            item.setTextAlignment(Qt.AlignCenter)
            self.setItem(row, col, item)
            col += 1
        self.repaint()
        self.main.addRolePermDialog.hide()
        '''deal with policy content'''
        roles = PolicyContent.rbac_role_res_mat.keys()
        '''get new role's perm '''
        rolenode = self.scene.clickedRoleNode.rnode
        rolePerm = copy.deepcopy(PolicyContent.rbac_role_res_mat[role])
        self.scene.computePermForRoleWithInheritance(rolePerm, rolenode)
        if aperm[3]=='True':
            rolePerm = policyUOAIO.combinePermsForOne(rolePerm, aperm[1], \
                policyUOAIO.makepermset(permset, None))
        else:
            rolePerm = policyUOAIO.combinePermsForOne(rolePerm, aperm[1], \
                policyUOAIO.makepermset(None, permset))
        PolicyContent.rbac_role_res_mat.update({role: rolePerm})
        '''Construct role hierarchy'''
        PolicyContent.rbac_role_inher_mat = self.scene.contructRoleInheritanceWithCycle(roles, rolePerm)
        policyUOAIO.removeInheritedPerm()
        policyUOAIO.removeRedundantPerm()
#         policyUOAIO.printRoleInheritance()
        '''create role graph'''
        self.scene.createRoleGraph()
        self.scene.updateScene()
        
    def delRolePerm(self):
        rows = set()
        for i in self.selectedItems():
            rows.add(i.row())
        aperm = []
        for r in rows:
            for c in xrange(2, self.columnCount()-1):
                aperm.append(str(self.item(r,c).text()))
            self.removeRow(r)
        self.repaint()
        '''deal with policy content'''
        obj, recursive = aperm[0], aperm[2]
        perms = set([x.strip() for x in aperm[1].split(',')])
        delPerm = policyUOAIO.makepermset(perms, None) if recursive == 'True' \
                    else policyUOAIO.makepermset(None, perms)
        role = self.scene.clickedRoleNode.name
        rolenode = self.scene.clickedRoleNode.rnode
        rolesPerm = {}
        rolePerm = copy.deepcopy(PolicyContent.rbac_role_res_mat[role])
        '''get role's new perm first'''
        self.scene.computePermForRoleWithInheritance(rolePerm, rolenode)
        rolesPerm[role] = policyUOAIO.removePermsForOne(rolePerm, obj, delPerm)
        PolicyContent.rbac_role_res_mat.update({role: rolePerm})
        '''update role inheritance'''
        roles = PolicyContent.rbac_role_res_mat.keys()
        '''Construct role hierarchy'''
        PolicyContent.rbac_role_inher_mat = self.scene.contructRoleInheritanceWithCycle(roles, rolePerm)
        policyUOAIO.removeInheritedPerm()
        policyUOAIO.removeRedundantPerm()
        '''create role graph'''
        self.scene.createRoleGraph()
        self.scene.updateScene()