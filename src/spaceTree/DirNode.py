'''
Created on Aug 21, 2018

@author: manw
'''

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import math

def getLabel(node):
    return node.path.split('/')[-1]

class DirNode(QGraphicsObject):
    def __init__(self, scene=None, path=''):
        QGraphicsObject.__init__(self)
        self.main = scene.main
        self.scene = scene
        
#         self.textFont = QFont("Courier", 13)#Helvetica"
        self.rad = 6
        self.width, self.height = 2*self.rad, 2*self.rad
        self.exist = True  # Flag to display the node or not
        self.drawn = False # Flag for the visually drawing of the node on canvas. True means it is already drawn
        self.selected = False 
        ''' for UNIX directory traversal animation '''
        self.accessible = False
        '''
        -1: all children not accessible; 
        0: children partially accessible; 
        1: all children accessible
        '''
        self.childrenAccessible = -1 
        self.passed = False
        ''''''
        
        self.ignore = False
        
        self.path = path
        self.label = '/'+getLabel(self)
        
        self.parent = None
        self.children = set()
        self.depth = -1
        self.angle = 0
        self.expanded = False
        self.highlighted = False
        self.relativeX, self.relativeY = 0,0
        self.xy = [0,0]
        self.startPos = [0,0] #x,y,alpha
        self.endPos = [0,0]
        
        self.setZValue(1)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setVisible(False)
        self.setToolTip(self.path)
        
    def __getitem__(self,key):
        return getattr(self,key)

    def boundingRect(self):
        return QRectF(-self.rad, -self.rad, self.width, self.width)
            
    def paint(self, painter, option, widget=None):
        painter.setRenderHint(QPainter.Antialiasing)
        '''
        =SET FILL COLOR=
        In order to reduce the clutter of the directory tree
        Nodes don't need expansion:
            nodes with all its descendants that can be accessed by the user
            nodes with all its descendants that can not be accessed by the user
        Nodes need expansion and more attension:
            nodes with descendants that can and cannot be accessed by the user
        '''
        if not self.children:
            painter.setBrush(QBrush(Qt.white))
        else:
            if self.childrenAccessible==-1:
                painter.setBrush(QBrush(Qt.gray))
            elif self.childrenAccessible==0:
                painter.setBrush(QBrush(QColor(158,202,225)))
            elif self.childrenAccessible==1:
                painter.setBrush(QBrush(QColor(43,140,190)))
        '''
        draw outline
        '''
        if self.selected:
            painter.setPen(QPen(Qt.red,3))
        else:
            if self.accessible:
                painter.setPen(QPen(QColor(43,140,190),3))
            else:
                '''
                grey out the node if there is no permission to the node
                '''
                painter.setPen(QPen(Qt.gray,3))
        painter.drawEllipse(self.boundingRect()) 
        '''
        display self.label as label
        '''
        painter.setPen(QPen(Qt.black))
        if self == self.scene.layout.root:
            self.displayname = self.path
        else:
            self.displayname = self.label
        
        if self==self.scene.layout.root or self.depth==self.scene.layout.config.levelsToShow:
            namelength = self.displayname
        else:
            namelength = 'this is a test file string that canb'
        self.labelRect = painter.fontMetrics().boundingRect(namelength)
        '''top'''
#         x = int(self.boundingRect().bottomLeft().x())
#         y = int(self.boundingRect().topLeft().y()-self.labelRect.height())
        '''right side'''
        x = int(self.boundingRect().bottomLeft().x()+(self.boundingRect().width()+5))
        y = int(self.boundingRect().topLeft().y()-0.5*self.boundingRect().height()+0.25*self.labelRect.height())
        self.paintLabel(painter, x, y)
                
    def paintLabel(self, painter, x, y):
        rect = self.labelRect
        rect.moveTo(x, y)
        painter.drawText(rect, Qt.AlignLeft, self.displayname)
    
    def setAbsolutePos(self, x, y):
        self.xy[0], self.xy[1] = x,y
        self.relativeX = (self.xy[0]-self.scene.canvasX)/self.scene.canvasW
        self.relativeY = (self.xy[1]-self.scene.canvasY)/self.scene.canvasH
        QGraphicsObject.setPos(self, QPointF(self.xy[0],self.xy[1]))
        
    def setPos(self, rx, ry):
        self.relativeX, self.relativeY = rx, ry
        self.xy[0], self.xy[1] = self.scene.canvasX + rx*self.scene.canvasW, self.scene.canvasY + ry*self.scene.canvasH
        QGraphicsObject.setPos(self, QPointF(self.xy[0],self.xy[1]))
        
    def mousePressEvent(self, event):
        QGraphicsObject.mousePressEvent(self, event)
        if event.buttons() == Qt.LeftButton:
            if QApplication.keyboardModifiers() == Qt.ShiftModifier:
                self.scene.layout.selectPath(self)
                if not self.expanded:
                    self.scene.layout.expandNode(self, True)
                    self.main.visAnimation.playStep = 0
                    self.main.visAnimation.init = False
                    self.main.visAnimation.animationStep()
                else:
                    self.scene.layout.collapseNode(self,True)
            else:
                self.main.visAnimation.resetAnimVisual()
                '''User View'''
                if self.main.visAnimation.perspective == self.main.visAnimation.USERVIEW:
                    if not self.main.visAnimation.selectedUser:
                        QMessageBox.warning(self.main, '', 'Please select a user first.')
                        return
                    '''choose the node as the object of interest'''
                    if self.main.visAnimation.target != self:
                        self.main.visAnimation.target = self
                        self.scene.layout.selectPath(self)
                        self.main.visAnimation.init = False
                        self.main.visAnimation.animationStep()
                    else:
                        self.main.visAnimation.target = None
                        '''show all accessible nodes'''
                        self.scene.layout.deselectPath()
                        if self.main.visAnimation.model == self.main.visAnimation.RBAC:
                            self.main.visualizerTextEdit.fillinPermDetail(self.main.visAnimation.accessibleObjectsRBAC, self.main.visAnimation.model == self.main.visAnimation.UNIX)
                else:
                    '''Object View'''
                    if self.main.visAnimation.model == self.main.visAnimation.RBAC:
                        '''choose a user node as the object of interest'''
                        if self.main.visAnimation.selectedObj:
                            self.main.visAnimation.selectedObj.selected = False
                        if self.main.visAnimation.selectedObj != self:
                            self.main.visAnimation.selectedObj = self
                            self.selected = True
                            self.main.visAnimation.init = False
                            self.main.visAnimation.initRBAC_objView()
                        else:
                            self.main.visAnimation.selectedObj = None
                            self.main.userListWidget.clearSelection()
                            self.main.visualizerTextEdit.fillinPermDetail()
            self.main.visAnimation.updateScenes()
