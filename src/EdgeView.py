'''
Created on Apr 17, 2014

@author: manw
'''
from PyQt4.QtGui import QColor, QPolygonF, QPainterPath, QPainter, QPen, QBrush
from PyQt4.QtCore import Qt, QLineF, QPointF
from PyQt4.QtGui import QGraphicsLineItem, QGraphicsItem
from grandalf.layouts import DummyVertex
from RoleNode import RoleNode
import math

class EdgeView(QGraphicsLineItem):
    ROLE_HIERARCH_CONN = 0
    
    def __init__(self, start, end, main):
        QGraphicsLineItem.__init__(self)

        self.main = main
        self.arrowSize = 15
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.v = [start, end]
        self.setZValue((-1000.0))
        self.highlighted = False
        self.setVisible(True)
        self.lineWidth = 1.0

        self.color = Qt.black
        self.arrowHead = QPolygonF()
        
    def setpath(self,pts):
        self._pts = pts
        
    def shape(self):
        angle = self.line().angle()
        if angle < 0:
            angle += 360
        angle -= 90
        angle = math.pi * angle / 180.0
        xTrans = 4 * math.cos(angle)
        yTrans = 4 * math.sin(angle)
        lower_line = self.line().translated(-xTrans,-yTrans)
        upper_line = self.line().translated(xTrans, yTrans)
        path = QPainterPath(upper_line.p1())
        path.lineTo(lower_line.p1())
        path.lineTo(lower_line.p2())
        path.lineTo(upper_line.p2())
        path.lineTo(upper_line.p1())
        path.addPolygon(self.arrowHead)
        return path
    
    def paint(self, painter, option, widget=None):
        painter.setRenderHint(QPainter.HighQualityAntialiasing, True)
        
        if self.highlighted:
            pen = QPen(Qt.red)
            brush = QBrush(Qt.red)
#             pen = QPen(QColor(245, 176, 65))
#             brush = QBrush(QColor(245, 176, 65))
        else:
            pen = QPen(self.color)
            brush = QBrush(self.color)
        
        pen.setWidth(self.lineWidth)
        painter.setPen(pen)
        painter.setBrush(brush)

        if isinstance(self.v[1], RoleNode) or isinstance(self.v[1], DummyVertex):
            centerline = QLineF(self.v[0].view.pos(), self.v[1].view.pos())
            startRect = self.v[1].view.boundingRect()
            endRect = self.v[1].view.boundingRect()
            
            # ZNOUSE_test the intersection between centerline and startRect
            p1 = startRect.topLeft() + self.v[0].view.pos()
            p2 = startRect.bottomLeft() + self.v[0].view.pos()
            intersection1 = QPointF()
            intersectType = QLineF(p1,p2).intersect(centerline, intersection1)
            if intersectType != QLineF.BoundedIntersection:
                p1 = p2
                p2 = startRect.bottomRight() + self.v[0].view.pos()
                intersectType = QLineF(p1,p2).intersect(centerline, intersection1)
                if intersectType != QLineF.BoundedIntersection:
                    p1 = p2
                    p2 = startRect.topRight() + self.v[0].view.pos()
                    intersectType = QLineF(p1, p2).intersect(centerline, intersection1)
                    if intersectType != QLineF.BoundedIntersection:
                        p1 = p2
                        p2 = startRect.topLeft() + self.v[0].view.pos()
                        intersectType = QLineF(p1, p2).intersect(centerline, intersection1)
                        if intersectType != QLineF.BoundedIntersection:
                            return
             
            intersection1 = (p1+p2) / 2.0  

            # ZNOUSE_test the intersection between centerline and endRect
            p1 = endRect.topLeft() + self.v[1].view.pos()
            p2 = endRect.bottomLeft() + self.v[1].view.pos()
            intersection2 = QPointF()
            intersectType = QLineF(p1,p2).intersect(centerline, intersection2)
            if intersectType != QLineF.BoundedIntersection:
                p1 = p2
                p2 = endRect.bottomRight() + self.v[1].view.pos()
                intersectType = QLineF(p1,p2).intersect(centerline, intersection2)
                if intersectType != QLineF.BoundedIntersection:
                    p1 = p2
                    p2 = endRect.topRight() + self.v[1].view.pos()
                    intersectType = QLineF(p1, p2).intersect(centerline, intersection2)
                    if intersectType != QLineF.BoundedIntersection:
                        p1 = p2
                        p2 = endRect.topLeft() + self.v[1].view.pos()
                        intersectType = QLineF(p1, p2).intersect(centerline, intersection2)
                        if intersectType != QLineF.BoundedIntersection:
                            return
            
            intersection2 = (p1+p2) / 2.0  
            self.setLine(QLineF(self.v[0].view.pos(), intersection2))
            
            verts = []
            verts.append(intersection2)
            if self.line().length() == 0:
                angle = 0
            else:
                angle = math.acos(self.line().dx() / self.line().length())
            if self.line().dy() >= 0:
                angle = 2*math.pi - angle
                
            verts.append( self.line().p2() +   QPointF(-math.cos(angle + math.pi / 12) * self.arrowSize,
                                                     math.sin(angle + math.pi / 12) * self.arrowSize)
                         )
            verts.append( self.line().p2() + QPointF(-math.cos(angle - math.pi / 12) * self.arrowSize,
                                                     math.sin(angle - math.pi / 12) * self.arrowSize)
                         )
            
            self.arrowHead = QPolygonF(verts)
            painter.drawPolygon(self.arrowHead)   
        # drawing
        painter.drawLine(self.line())
        
    def updatePosition(self):
        line = QLineF(self.mapFromItem(self.v[0].view, QPointF(0, 0)), self.mapFromItem(self.v[1].view, QPointF(0, 0)))
        self.setLine(line)
