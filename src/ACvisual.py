'''
Created on Aug 15, 2016

@author: manw
'''
import sys
from PyQt4.QtGui import QApplication, QMainWindow, QDialog
from PyQt4.QtCore import QCoreApplication, Qt
from MainWindow import MainWindow
from  Ui_Greeting import Ui_Greeting
from policyParse import policyParse
  
class GreetingWindow(QDialog):
    def __init__(self, main):
        QDialog.__init__(self)
        self.ui = Ui_Greeting()
        self.ui.setupUi(self)
        flags = Qt.Dialog | Qt.WindowStaysOnTopHint
        self.setWindowFlags(flags)
        self.ui.pushButton.clicked.connect(self.close)
                      
def main():
    app = QApplication(sys.argv)
    QCoreApplication.setApplicationName('ACvisual')
    QCoreApplication.setApplicationVersion('1.0')
    app.setStyleSheet("""QToolTip {
                        background-color:white;
                        color:black;
    }""")
    mainw = MainWindow()
#     mainw.greetWin = GreetingWindow(mainw)
#     mainw.greetWin.show()
    mainw.show()
      
    exitcode = app.exec_()
    '''destructor'''
    del mainw.view
    sys.exit(exitcode)

if __name__ == '__main__':
    main()
