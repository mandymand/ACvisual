"""
file:   policyUOAIO.py
author: manwang
description:
        Functions for converting an access control policy in form of user object action 
        into a RBAC policy.
"""
import PolicyContent, copy
from policyUOAParse import policyUOAParse
from collections import namedtuple
# from PolicyContent import rbac_role_inher_mat
import PermissionChecker

'''RBAC'''
Permset = namedtuple('Permset', ('recur_perms', 'nonrecur_perms'))
RoleNode = namedtuple('RoleNode', ('parents', 'children'))
lineIndex = 0

def makepermset(recur_iterable=None, nonrecur_iterable=None):
    recur_perms = set() if recur_iterable is None else set(recur_iterable)
    nonrecur_perms = set() if nonrecur_iterable is None else set(nonrecur_iterable)
    return Permset(recur_perms, nonrecur_perms)

def makerolenode(parents=None, children=None):
    parentset = set() if parents is None else set(parents)
    childset = set() if children is None else set(children)
    return RoleNode(parentset, childset)

def combinePermsForOne(permdict, res, permset):
    if res in permdict.keys():
        permission = permdict[res]
        tempperm = makepermset(permission.recur_perms.union(permset.recur_perms),\
                               permission.nonrecur_perms.union(permset.nonrecur_perms))
        tempperm2 = makepermset(tempperm.recur_perms, tempperm.nonrecur_perms-tempperm.recur_perms)
        permdict.update({res: tempperm2})
    else:
        isCovered = False
        for obj, permission in permdict.iteritems():
            '''check recursive coverage'''
            if (obj in res) and permission.recur_perms:
                if permset.recur_perms.issubset(permission.recur_perms) or\
                    permset.nonrecur_perms.issubset(permission.recur_perms):
                    isCovered = True
                    break
        if not isCovered:
            permdict.update({res: permset})
    return permdict

def removePermsForOne(permdict, res, permset):
    if res not in permdict.keys(): return permdict
    permission = permdict[res]
    recur_perms = permission.recur_perms - permset.recur_perms
    nonrecur_perms = permission.nonrecur_perms - permset.nonrecur_perms - recur_perms
    if (not recur_perms) and (not nonrecur_perms): 
        permdict.pop(res)
    else: 
        tempperm = makepermset(recur_perms, nonrecur_perms)
        permdict.update({res: tempperm})
    return permdict
    
        
def parseUOAStmt(stmt):
    global lineIndex
    stmttype = stmt[0]
    if stmttype == 'oscrawlfile:':
        PolicyContent.oscrawlfile = stmt[1]
    elif stmttype == 'root:':
        root = stmt[1]
        if root!='/' and root[-1]=='/': root = root[:-1]
        PolicyContent.root_dir = root
    elif stmttype == 'user:':
        for i in stmt[1]:
            PolicyContent.uoa_user_set.add(i)
    elif stmttype == 'object:':
        for i in stmt[1]:
            if i!='/' and i[-1]=='/': i = i[:-1]
            PolicyContent.uoa_object_set.add(i)
    elif stmttype == 'rule:':
        user, perms, recursive, objs = stmt[1], set(stmt[2]), stmt[3], stmt[-1]#pathlist
        if isinstance(recursive, basestring) and (recursive.strip()=='-r'):
            recursive = True
            permset = makepermset(perms, None)
        else:
            recursive = False
            permset = makepermset(None, perms)
        for resource in objs:
            if resource[-1]=='/' and resource!='/':
                resource = resource[:-1]
            '''store in user_obj_perm matrix'''
            dup = False
            if user not in PolicyContent.user_obj_perm_mat.keys():
                PolicyContent.user_obj_perm_mat[user] = {resource: permset}
            else:
                if resource not in PolicyContent.user_obj_perm_mat[user].keys():
                    PolicyContent.user_obj_perm_mat[user][resource] = permset
                else:
                    permpair = PolicyContent.user_obj_perm_mat[user][resource]
                    '''if there is some permissions that does not exist in the previous permissions'''
                    allprevperm = permpair.recur_perms.union(permpair.nonrecur_perms)
                    if not ((permset.recur_perms - permpair.recur_perms) or\
                        (permset.nonrecur_perms - allprevperm)): dup = True
                    if not dup:
                        recur_perms = permpair.recur_perms.union(permset.recur_perms)
                        nonrecur_perms = permpair.nonrecur_perms.union(permset.nonrecur_perms) - recur_perms
                        permpair = makepermset(recur_perms, nonrecur_perms)
            '''find useful rules and duplicate rules'''
            rule = PermissionChecker.UOARuleInfo(user, resource, perms, recursive, lineIndex)
            lineIndex += 1
            if dup:
                PolicyContent.user_rule_list_dup.append(rule)
            else:
                PolicyContent.user_rule_list.append(rule)
    else:
        raise ValueError('unknown statement type: {0}'.format(stmttype))
           
def policyUOAIO(source):
    '''
    Args:
        source (file or filename): A User-object-action policy file
    
    Returns:
        root_dir (string): root directory.
            the directory to start from to extract information on the real OS
        For UOA model:
            PolicyContent.uoa_user_set (set): user set.
            PolicyContent.uoa_object_set (set): object set.
            PolicyContent.user_obj_perm_mat (dict): contains the permission a user has to the objects.
                PolicyContent.user_obj_perm_mat['user']['/path/to/obj'] returns a namedtuple with fields of a flag for directory, permission 
                    recursiveness, the permission list of that specific user.
                    The permission bits for each category are represented in list in the order of rwx.
                    USED IN RBAC DIRECTLY.
            PolicyContent.user_obj_perm_mat_dup (dict): similar to PolicyContent.user_obj_perm_mat. But only with unnecessary duplicated rules 
        For RBAC model:
            PolicyContent.rbac_usr_role_mat (dict): user-to-role assignment.
                PolicyContent.rbac_usr_role_mat['role'] returns the list of users assigned to the role.
            PolicyContent.rbac_role_res_mat (dict): role permissions on objects.
                PolicyContent.rbac_role_res_mat['role']: returns the permission the role has to objects. The values 
            user permissions on objects.
                ''PolicyContent.user_obj_perm_mat['user']['/path/to/obj']: returns a namedtuple that has flag of directory,
                    recursiveness of the permission and the permission list.
                PolicyContent.user_obj_perm_mat['user']['/path/to/obj']: returns a tuple (permset1, permset2). 
                    permset1: permission set for recursive permissions; permset2: permission set for non-recursive permissions.
                group_obj_perm_mat['group']['/path/to/obj']: returns a namedtuple that has flag of directory,
                    recursiveness of the permission and the permission list.
            PolicyContent.rbac_role_inher_mat (dict): 
    '''
    try:
        text = source.read()
    except AttributeError:
        with open(source, 'r') as f:
            text = f.read()
    
    stmts = policyUOAParse(text)
    for stmt in stmts:
        parseUOAStmt(stmt)
    '''check further for parent directory's recursive permissions'''
#     import pprint
#     pprint.pprint(PolicyContent.user_rule_list)
#     print '-'*20
    root = PolicyContent.root_dir
    rootLevel = root.count('/')
    permsofar = set()
    duplicateRules = []
    for r in PolicyContent.user_rule_list:
        user, obj, permset = r.user, r.obj, r.permset
        curdir = obj[:obj.rfind('/')]
        dup = False
        while curdir.count('/')>=rootLevel:
            if curdir not in PolicyContent.user_obj_perm_mat[user].keys():
                curdir = curdir[:curdir.rfind('/')]
                continue
            permission = PolicyContent.user_obj_perm_mat[user][curdir].recur_perms
            '''if all recursive permissions from ancestors combined together is a superset of the 
            perm set in the rule for the object, then the rule is redundant'''
            permsofar = permsofar.union(permission)
            if permset.issubset(permsofar): 
                dup = True
                break
            curdir = curdir[:curdir.rfind('/')]
        if dup:
            duplicateRules.append(r)
    for r in duplicateRules:
        PolicyContent.user_rule_list.remove(r)
        PolicyContent.user_rule_list_dup.append(r)
#     pprint.pprint(PolicyContent.user_rule_list)
#     print '-'*20
#     pprint.pprint(PolicyContent.user_rule_list_dup)

def parseRBACStmt(stmt):
    stmttype = stmt[0]
    if stmttype == 'user:':
        roles, users = stmt[1:]
        for user in users:
            try:
                PolicyContent.rbac_usr_role_mat[user].update(roles)
            except KeyError:
                PolicyContent.rbac_usr_role_mat[user] = set(roles)
    elif stmttype == 'free_user:':
        users = stmt[1]
        for user in users:
            PolicyContent.rbac_usr_role_mat[user] = set()
    elif stmttype == 'object:':
        if isinstance(stmt[1], str):
            resource = stmt[1]
            role = 'None'
            permset = makepermset(set(), recursive=False)
            PolicyContent.rbac_role_res_mat[role] = {resource: permset}
        else:
            roles, perms, resource = stmt[1], stmt[2], stmt[-1]
            if resource[-1]=='/' and resource!='/':
                resource = resource[:-1]
            if len(stmt) == 5:
                permset = makepermset(perms, recursive=True)
            else:
                permset = makepermset(perms, recursive=False)
            for role in roles:
                try:
                    resourcedict = PolicyContent.rbac_role_res_mat[role]
                    resourcedict[resource] = permset
                except KeyError:
                    # role has no resources
                    PolicyContent.rbac_role_res_mat[role] = {resource: permset}
    elif stmttype == 'inheritance:':
        if len(stmt) == 4:
            parent, child = stmt[1], stmt[3]
        else:
            parent = stmt[1]
            child = None
        try:
            parentnode = PolicyContent.rbac_role_inher_mat[parent]
            parentnode.children.add(child)
        except KeyError:
            PolicyContent.rbac_role_inher_mat[parent] = makerolenode(children=(child,))
            try:
                childnode = PolicyContent.rbac_role_inher_mat[child]
                childnode.parents.add(parent)
            except KeyError:
                PolicyContent.rbac_role_inher_mat[child] = makerolenode(parents=(parent,))

def isSameDict(dict1, dict2):
    if dict1.keys()!=dict2.keys(): return False
    if dict1.values()!=dict2.values(): return False
    return True

def getUsersOfRole(role):
    users = set()
    for u, rs in PolicyContent.rbac_usr_role_mat.items():
        if role in rs:
            users.add(u)
    return users

def findNthOccuranceOf(s, pattern, n):
    import re
    indxs = [m.start() for m in re.finditer(pattern,s)]
    if n>=len(indxs): return -1
    return indxs[n-1]

def getRolePerm(role, rolePermDict):
    permission = rolePermDict[role]
    return permission

# def getRolePermWithInheritance(role):
#     permission = PolicyContent.rbac_role_res_mat[role]
#     for r in PolicyContent.rbac_role_inher_mat[role].children:
#         if r in PolicyContent.rbac_role_res_mat.keys():
#             for k, v in PolicyContent.rbac_role_res_mat[r].iteritems():
#                 if k in permission.keys():
#                     tempPerm = permission[k].perms.union(v.perms)
#                     tempRecur = permission[k].recursive
#                     if v.recursive == False:
#                         tempRecur = False
#                     permission[k] = makepermset(tempPerm, tempRecur)
#                 else:
#                     permission[k] = v
#                     '''include inherent directory hierarchy'''
#                     level = k.count('/')
#                     for dd in PolicyContent.uoa_object_set:
#                         ending = findNthOccuranceOf(dd, '/', level+1)
#                         if ending == -1 or dd[:ending]!=k: continue
#                         if dd in permission.keys():
#                             tempPerm = permission[dd].perms.union(v.perms)
#                             permission[dd] = makepermset(tempPerm, permission[dd].recursive)
#                         else:
#                             permission[dd] = v
#     return permission

def permRelationBtwnRoles(role1, role2):
#     print role1, getRolePerm(role1, PolicyContent.rbac_role_res_mat)
#     print role2, getRolePerm(role2, PolicyContent.rbac_role_res_mat)
    #0: no relation; 1: subset; -1: superset; 2: cyclic inheritance
    _1subset2 = isPermSubsetOfRole(role1, role2, PolicyContent.rbac_role_res_mat)
    _2subset1 = isPermSubsetOfRole(role2, role1, PolicyContent.rbac_role_res_mat)
    if _1subset2 and _2subset1: return 2
    elif _1subset2: return 1 #role1 is subset of role2
    elif _2subset1: return -1
    else: return 0

def isPermSubset(obj1, perm1, obj2, perm2):
    '''check if the permission to obj1 is totally covered by that to obj2'''
    '''if perm2 have recursive permissions
        return False if perm1 recursive permission has more than perm2 recursive permissions
    '''
    if obj2 not in obj1: return False
    recur_good = False
    if perm2.recur_perms:
        if obj2 in obj1:
            if perm1.recur_perms.issubset(perm2.recur_perms): 
                recur_good = True
            else: return False
    if not recur_good: return False
    '''check non-recursive permissions'''
    recurpermCheck = perm1.nonrecur_perms - perm2.nonrecur_perms
    if (not recurpermCheck) or (recurpermCheck.issubset(perm2.recur_perms)): 
        return True
    
def isPermSubsetOfRole(role1, role2, rolePermDict):
    permission1 = getRolePerm(role1, rolePermDict)
    permission2 = getRolePerm(role2, rolePermDict)
#     print role1, permission1
#     print role2, permission2
    return isPermSubsetOfRole2(permission1, permission2)

def isPermSubsetOfRole2(permission1, permission2):
    '''check if role1's perm is a subset of role2' perm (subset: 1)
    For role1's obj: check if that obj is in role2's
        for recursive, check if the obj is a descendant of the obj in role2
        for non-recursive, check equality
    If the above check passed, check if the perm is a subset of the perm of role2
    '''
    for obj1, perm1 in permission1.iteritems():
        found = False
        for obj2, perm2 in permission2.iteritems():
            if isPermSubset(obj1, perm1, obj2, perm2):
                found = True
                break
        if not found: 
#             print 'False'
            return False
#     print 'True'
    return True

def convert2RBAC():
    #root_dir is the same
    '''Start by assigning each user to an individual role'''
    '''Merge roles with the same permissions into one'''
    numUser = len(PolicyContent.user_obj_perm_mat)
    users = list(PolicyContent.user_obj_perm_mat.keys())
    roles = []
    for i in xrange(1, numUser+1):
        role = 'role_'+users[i-1]#str(i)
        user1 = users[i-1]
        if user1 not in PolicyContent.rbac_usr_role_mat.keys():
            roles.append(role)
            try:
                PolicyContent.rbac_usr_role_mat[user1].append(role)
            except KeyError:
                PolicyContent.rbac_usr_role_mat[user1] = [role]
            PolicyContent.rbac_role_res_mat[role] = copy.deepcopy(PolicyContent.user_obj_perm_mat[user1])  
        for j in xrange(i+1, numUser+1):
            user2 = users[j-1]
            if isSameDict(PolicyContent.user_obj_perm_mat[user1], PolicyContent.user_obj_perm_mat[user2]):
                try:
                    PolicyContent.rbac_usr_role_mat[user2].append(role)
                except KeyError:
                    PolicyContent.rbac_usr_role_mat[user2] = [role]
    '''add the rest of users to the rbac_usr_role_mat'''
    for u in PolicyContent.uoa_user_set:
        if u not in PolicyContent.rbac_usr_role_mat.keys():
            PolicyContent.rbac_usr_role_mat[u] = []
    '''Construct role hierarchy'''
    PolicyContent.rbac_role_inher_mat = contructRoleInheritanceNoCycle(roles)
    removeTransitiveRoleInheritance(PolicyContent.rbac_role_inher_mat)
    '''remove inherited permissions from the senior role'''
    removeInheritedPerm()
    removeRedundantPerm()
#     printUserRole()
#     printRoleRes()
#     printRoleInheritance()
    
def buildConnection(parent, child, role_inher_mat):
    try:
        parentnode = role_inher_mat[parent]
        parentnode.children.add(child)
    except KeyError:
        role_inher_mat[parent] = makerolenode(children=(child,))
    try:
        childnode = role_inher_mat[child]
        childnode.parents.add(parent)
    except KeyError:
        role_inher_mat[child] = makerolenode(parents=(parent,))

def findRoleDescendants(role):
    role_inher = PolicyContent.rbac_role_inher_mat
    toVisit = list(role_inher[role].children)
    visited = set([role])
    while toVisit:
        n = toVisit.pop(0)
        if n not in visited:
            visited.add(n)
            toVisit.extend(role_inher[n].children)
    return visited - set([role])
    
def findCycles(roles):
    connCompsLookup = {} #key: rolename, value: id of group
    connComps = [] # index is id of group, each connected component is a set
    for i in xrange(len(roles)):
        role1 = roles[i]
        for j in xrange(i+1, len(roles)):
            #if satisfied, role1 inherits role2
            subset = permRelationBtwnRoles(roles[j], role1)
            role2 = roles[j]
#             print role1, role2, subset
#             print '========'
            if subset == 2: # this pair of roles are in connected component, no connection for now
                idnum = len(connComps)
                if role1 in connCompsLookup.keys(): idnum = connCompsLookup[role1]
                if role2 in connCompsLookup.keys(): idnum = connCompsLookup[role2]
                connCompsLookup[role1], connCompsLookup[role2] = idnum, idnum
                if idnum == len(connComps): connComps.append(set([role1, role2]))
                else: 
                    connComps[idnum].add(role1)
                    connComps[idnum].add(role2)
    return connComps

def findNearestAncestorNChild2Conn(comp):
    numChildrenMax = -1
    ancestor = None
    numChildrenMin = 1000
    child = None
    for c in comp:
        x = len(findRoleDescendants(c))
        if x>numChildrenMax:
            ancestor = c
            numChildrenMax = x
        if x<numChildrenMin:
            child = c
            numChildrenMin = x
    return child, ancestor
        
def contructRoleInheritanceNoCycle(roles, cycleRoles=set()):
    rbac_role_inher_mat = {}
    root = PolicyContent.root_dir
    '''check if pseudo root role is needed to guarantee single role tree'''
    createRootRole = True
    for r in roles:
        accesses = PolicyContent.rbac_role_res_mat[r]
        if root in accesses.keys():
            perm = accesses[root]
            if perm.recur_perms==set('rwx'):
                createRootRole = False
                break
            
    if createRootRole:
        roles.append('role0')
        PolicyContent.rbac_role_res_mat['role0'] = {root:makepermset(set('rwx'))}
    '''initiate all roles in the inheritance mat'''
    for r in roles:
        rbac_role_inher_mat[r] = makerolenode()
    for i in xrange(len(roles)):
        role1 = roles[i]
        for j in xrange(i+1, len(roles)):
            #if satisfied, role1 inherits role2
            role2 = roles[j]
            if role1 in cycleRoles and role2 in cycleRoles: continue
            subset = permRelationBtwnRoles(role2, role1)
#             print subset
#             print '========'
            if subset==2 or subset==0: continue
            if subset == 1:
                parent = role1
                child = role2
            elif subset == -1:
                child = role1
                parent = role2
            buildConnection(parent, child, rbac_role_inher_mat)
    return rbac_role_inher_mat

def removeTransitiveRoleInheritance(role_inher_mat, cycleRoles=set()):
    '''
    for each role, do BFS to check if a child role has been reached before
    is reached again along some paths, then direct connection with this role should be removed
    '''
    for r in role_inher_mat.keys():
        children  = role_inher_mat[r].children
        toRemoveC = set()
        toVisit = list(children)
        visited = set()
        while toVisit:
            n = toVisit.pop(0)
            if n in visited:
                toRemoveC.add(n)
            else:
                visited.add(n)
                toVisit.extend(role_inher_mat[n].children)
#         print 'transitive', r, toRemoveC
        for t in toRemoveC:
            if (r in cycleRoles) and (t in cycleRoles): continue
            if t in children:
                role_inher_mat[r].children.remove(t)
                role_inher_mat[t].parents.remove(r)
#         print role_inher_mat[r].children
        
# def removeTransitiveRoleInheritance():
#     for r in PolicyContent.rbac_role_inher_mat.keys():
#         children  = PolicyContent.rbac_role_inher_mat[r].children
#         toRemoveC = set()
#         for c in children:
#             for cc in children:
#                 if c!=cc and cc in PolicyContent.rbac_role_inher_mat[c].children:
#                     toRemoveC.add(cc)
# #         print r, toRemoveC
#         for t in toRemoveC:
#             children.remove(t)
#             PolicyContent.rbac_role_inher_mat[t].parents.remove(r)

def removeRedundantPerm():
    for r in PolicyContent.rbac_role_inher_mat.keys():
        permdict = PolicyContent.rbac_role_res_mat[r]
        removeRedundantPermForARole(permdict)
        
def removeRedundantPermForARole(permdict):
    redundant = set() # store the object permission that is fully covered by other recursive permissions
    for resrc, perm in permdict.iteritems():
        permdict[resrc] = makepermset(perm.recur_perms, perm.nonrecur_perms-perm.recur_perms)
    for resrc1, perm1 in permdict.iteritems():
        '''check whether resrc2 is redundant'''
        for resrc2, perm2 in permdict.iteritems():
            if resrc1== resrc2 and perm1 == perm2: continue
            if resrc1 in resrc2 and perm1.recur_perms:
                recur_diff = perm2.recur_perms - perm1.recur_perms
                nonrecur_diff = perm2.nonrecur_perms - perm1.recur_perms
                if (not recur_diff) and (not nonrecur_diff): redundant.add(resrc2)
                else: # if not fully covered by recursive permissions, then only store the permissions not covered 
                    permdict[resrc2] = makepermset(recur_diff, nonrecur_diff)
    for resrc in redundant:
        permdict.pop(resrc)
        
def removeInheritedPermForARole(senior):
        sPerm = PolicyContent.rbac_role_res_mat[senior]
        jPerms = {}
        for junior in PolicyContent.rbac_role_inher_mat[senior].children:
            '''gather all permissions from all children'''
            for resrc, jPerm in PolicyContent.rbac_role_res_mat[junior].iteritems():
                if resrc not in jPerms.keys():
                    jPerms[resrc] = jPerm
                else:
                    recur_perms, nonrecur_perms = jPerms[resrc].recur_perms, jPerms[resrc].nonrecur_perms
                    nonrecur_add = jPerm.nonrecur_perms-jPerms[resrc].nonrecur_perms
                    jPerms[resrc] = makepermset(recur_perms.union(jPerm.recur_perms),\
                                                nonrecur_perms.union(nonrecur_add))
        inherited = set()
        for resrc1, perm1 in sPerm.iteritems():
            for resrc2, perm2 in jPerms.iteritems():
                if resrc1 == resrc2:
                    recur_diff = perm1.recur_perms - perm2.recur_perms
                    nonrecur_diff = perm1.nonrecur_perms - perm2.nonrecur_perms
                    if (not recur_diff) and (not nonrecur_diff): inherited.add(resrc2)
                    else: # if not fully covered by recursive permissions, then only store the permissions not covered 
                        sPerm[resrc1] = makepermset(recur_diff, nonrecur_diff)
        for resrc in inherited:
            sPerm.pop(resrc)
        
def removeInheritedPerm():
    for senior in PolicyContent.rbac_role_inher_mat.keys():
        removeInheritedPermForARole(senior)
                    
def write2UOAFile(filename, rulelist):
    content = ['root: '+PolicyContent.root_dir]
    content.append('user: '+', '.join(PolicyContent.uoa_user_set))
    content.append('object: '+', '.join(PolicyContent.uoa_object_set))
    for rule in rulelist:
        recur = '-r' if rule[3]=='True' else ''
        arule = 'rule: '+rule[0] + '\t'+rule[2]+'\t'+recur+'\t'+rule[1]
        content.append(arule)
    f = open(filename, 'w')
    for line in content:
        f.writelines(line+'\n')
    f.close()

def printUserRole():
    print '== RBAC User Role Assignment Matrix =='
    for key, value in PolicyContent.rbac_usr_role_mat.items():
        print 'user = {0}'.format(key)
        print '    roles = {0}'.format(','.join(value))
        
def printRoleRes():
    print '== RBAC Role Object Permission matrix =='
    for key, value in PolicyContent.rbac_role_res_mat.items():
        print 'role = {0}'.format(key)
        for kk, vv in value.iteritems():
            print '    res = {0}'.format(kk)
            print '        recur-perms = {0}'.format(','.join(vv[0]))
            print '        nonrecur-perms = {0}'.format(','.join(vv[1]))

def printRoleDiff(resDict1, resDict2):
    print '== RBAC Role Object Permission Diff =='
    for role, value in resDict1.iteritems():
        if role in resDict2.keys() and resDict1[role] == resDict2[role]:
            print 'role = {0}'.format(role), 'same'
        else:
            print 'role = {0}'.format(role)
            value2 = {} if role not in resDict2.keys() else resDict2[role]
            print '1'*50
            for kk, vv in value.iteritems():
                print '    res = {0}'.format(kk)
                print '        recur-perms = {0}'.format(','.join(vv[0]))
                print '        nonrecur-perms = {0}'.format(','.join(vv[1]))
            print '2'*50
            for kk, vv in value2.iteritems():
                print '    res = {0}'.format(kk)
                print '        recur-perms = {0}'.format(','.join(vv[0]))
                print '        nonrecur-perms = {0}'.format(','.join(vv[1]))
        print '~'*50
                
def printRoleInheritance():
    print '== RBAC Role Inheritance matrix =='
    for key, value in PolicyContent.rbac_role_inher_mat.items():
        print 'role = {0}'.format(key)
        print '    parents = {0}'.format(', '.join(value.parents))
        print '    children = {0}'.format(', '.join(value.children))
          
def printUOAPolicyContent():
    print '== Root directory =='
    print PolicyContent.root_dir
    print '== User set=='
    print PolicyContent.uoa_user_set
    print '== Object set=='
    print PolicyContent.uoa_object_set
    print '== User object perm matrix =='
    for user, permdict in PolicyContent.user_obj_perm_mat.items():
        printthing('user = %s'%user)
        for obj, perm in permdict.items():
            printthing('object = %s'%obj, 2)
            printthing('recursive perm = %s'%perm.recur_perms, 3)
            printthing('nonrecursive perm = %s'%perm.nonrecur_perms, 3)

def printthing(thing, depth=1):
    print '{0}{1}'.format(' '*4*depth, thing)

if __name__ == '__main__':
#     import sys
#     if len(sys.argv)<2:
#         source = sys.stdin
#     else:
#         source = sys.argv[1]
#     root, unix_usr_grp_mat, unix_res_own_mat, unix_res_perm_mat, \
#     PolicyContent.rbac_usr_role_mat, PolicyContent.rbac_role_res_mat, PolicyContent.rbac_role_inher_mat = policyIO('./policies/ZNOUSE_test.ac')
    policyUOAIO('./policies/InheritanceFlat.uoa')
    printUOAPolicyContent()
    convert2RBAC()
    printUserRole()
    printRoleRes()
    printRoleInheritance()
