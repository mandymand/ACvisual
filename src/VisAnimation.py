'''
Created on Sep 20, 2018

@author: manw
'''
from PyQt4.QtCore import  QCoreApplication
from PyQt4.QtGui import QMessageBox, QColor, QAbstractItemView
from spaceTree.DirNode import DirNode
import PermissionChecker, PolicyContent
from collections import namedtuple

UserPerm = namedtuple('UserPerm', ('user', 'permset'))
ObjPerm = namedtuple('ObjPerm', ('obj', 'permset'))

class VisAnimation(object):
    '''
    control the highlighting and analysis animation in the Policy Analysis section
    '''
    UNIX, RBAC = 0, 1
    UNIX_ANIM_LASTSTEP, RBAC_ANIM_LASTSTEP = 0, 3
    USERVIEW, OBJVIEW, ROLEVIEW = 0,1,2
    XOR_ROLE_COLORS = [QColor(215,25,28), QColor(253,174,97), QColor(255,255,191), \
                        QColor(171,221,164), QColor(43,131,186)]
    def __init__(self, main):
        '''
        Constructor
        '''
        self.main = main
        self.scene = main.scene
        self.scene_obj = main.scene_obj
        self.perspective = self.USERVIEW
        self.reset()
        
    def resetObjAccess(self):
        for d in self.scene_obj.graph.nodeDict.values(): 
            d.accessible = False
            d.childrenAccessible = -1
    
    def resetRoleXorColorIndex(self):
        for r in self.scene.roleHier.V(): 
            r.view.xorColorIndex = -1
            
    def resetRoleHighlight(self):
        self.scene.clickedRoleNode = None
        if not self.scene.roleHier: return
        for r in self.scene.roleHier.V(): 
            r.view.highlighted = False
            r.view.inhHighlighted = False
            r.view.roleInhHighlighted = False
        
    def resetRoleAccess(self):
        if not self.scene.roleHier: return
        for r in self.scene.roleHier.V(): r.view.roleAccess = 0
        
    def resetRoleEdges(self):
        if not self.scene.roleHier: return
        for e in self.scene.roleHier.E(): e.view.highlighted = False
        
    def reset(self):
        self.target = None
        self.init = True
        self.selectedUser = None
        self.selectedObj = None
        self.playStep = -1
        self.animatingFocus = False  
        self.accessibleObjectsRBAC = {}
        self.accessibleObjectsUNIX = {}
        '''flags to find which type of animation it is'''
        self.model = self.RBAC
        self.unix_pathNdList = []
        self.unix_rules = {}
        self.rbac_rules_withfilter = {}
        self.rbac_roles_direct = set() # all direct roles
        self.rbac_direcRolePerm, self.rbac_inhRolePerm = {}, {} # useful ones
        self.rbac_pathDict = {}
        if self.scene.roleHier:
            self.resetRoleHighlight()
            self.resetRoleEdges()
            self.resetRoleAccess()
            self.resetObjAccess()
            self.main.userListWidget.resetItemsVisual()
            self.resetAnimVisual()
            self.scene_obj.layout.deselectPath()

    def resetAnimVisual(self):          
        '''clear all graphical marks'''
        self.rbac_accuPerm = set()
        self.resetRoleAccess()
        self.resetRoleEdges()
        self.main.visualizerTextEdit.fillinPermDetail()
        self.scene_obj.msg = ''
        self.playStep = 0
        self.toggleEnableAnimSwitch()
        self.updateScenes()
         
    def toggleEnableAnimSwitch(self):
        flag = self.main.animSwitch.isEnabled() and self.main.animSwitch.isChecked()
        self.main.animPrevBtn.setEnabled(flag)
        self.main.animNextBtn.setEnabled(flag)
        
    def animationBackward(self):
        if not self.target:
            QMessageBox.warning(self.main, '', 'Please click on an object for User Perspective,\n'+
                                'or a user for Object Perspective.')
            return
        if self.playStep > 0: 
            self.playStep -= 1
        self.animationStep()
        self.main.animNextBtn.setEnabled(((self.model==self.RBAC) and (self.playStep<self.RBAC_ANIM_LASTSTEP) \
                                          or (self.model==self.UNIX and self.playStep<self.UNIX_ANIM_LASTSTEP)))
        self.main.animPrevBtn.setEnabled(self.playStep > 0)
        
    def animationForward(self):
        if not self.target:
            QMessageBox.warning(self.main, '', 'Please click on an object for User Perspective,\n'+
                                'or a user for Object Perspective.')
            return
        '''if at the last step of UNIX, switch to RBAC'''
        if ((self.model==self.RBAC) and (self.playStep<self.RBAC_ANIM_LASTSTEP) \
            or (self.model==self.UNIX and self.playStep<self.UNIX_ANIM_LASTSTEP)):
            self.playStep += 1
        self.animationStep()
        self.main.animNextBtn.setEnabled(((self.model==self.RBAC) and (self.playStep<self.RBAC_ANIM_LASTSTEP)\
                                          or (self.model==self.UNIX and self.playStep<self.UNIX_ANIM_LASTSTEP)))
        self.main.animPrevBtn.setEnabled(self.playStep > 0)
        
    def animationStep(self):
        if self.model == self.RBAC:
            self.RBAC_Animation()
            '''make sure the chosen object node is visible'''
            if self.perspective == self.USERVIEW: self.scene_obj.layout.makeNodeVisible(self.target)
        self.updateScenes()
    '''
    = Prepare the info needed for the RBAC visualization = 
    1. Filter the pre-computed rule info using set filter permission and/or target object
    2. Update accessible object nodes visually
    3. Prepare role information for visualization
    '''
    def computeRBACPermInfo_userView(self):
        self.rbac_rules_withfilter = self.updateRBACRuleInfor_userView()
        self.updateAccessibleNodes()
        if self.target: 
            self.updateRolesVisual()
    
    def computeRBACPermInfo_objView(self):
        if self.accessibleUserRBAC: self.initRBAC_objView()
        self.rbac_rules_withfilter = self.updateRBACRuleInfor_objView()
        self.updateUsersAccessible()
        if self.target:
            self.updateRolesVisual()
    
    '''
    = Prepare role information for visualization =
    1. Find all roles that have the set permission to the target, store in directRoles, inhRoles
    2. Find path between Directly assigned roles and the roles in red frames that it inherited from
    '''
    def updateRolesVisual(self):
        if self.perspective == self.USERVIEW:
            self.rbac_direcRolePerm, self.rbac_inhRolePerm = self.getRoleInfoForAnimation_userView()
            self.rbac_roles_direct = set(PolicyContent.rbac_usr_role_mat[self.selectedUser]).union(self.rbac_direcRolePerm.keys())
            self.rbac_pathDict = self.findDirecRolePath(self.rbac_inhRolePerm.keys(), self.rbac_roles_direct)
        elif self.perspective == self.OBJVIEW:
            self.rbac_direcRolePerm, self.rbac_inhRolePerm = self.getRoleInfoForAnimation_objView()
            self.rbac_roles_direct = set(PolicyContent.rbac_usr_role_mat[self.selectedUser]).union(self.rbac_direcRolePerm.keys())
            self.rbac_pathDict = self.findDirecRolePath(self.rbac_inhRolePerm.keys(), \
                                                        self.rbac_roles_direct)
#             print 'direcRolePerm, inhRolePerm', self.rbac_direcRolePerm, self.rbac_inhRolePerm
        
    def updateRBACRuleInfor_userView(self):
        allPerms = self.accessibleObjectsRBAC
        relatedPerms = {}
        if self.target: #show related rules with the permission filtered
            target = self.target.path
            relatedPerms = {target: []}
            if target in allPerms.keys():
                for p in allPerms[target]:
                    if PermissionChecker.permissionAllowed(\
                            self.main.filter_perm,self.main.filter_permRelation, p.permset):
                        relatedPerms[target].append(p)
                    elif p.permset.issubset(self.main.filter_perm):
                        '''
                        For cases, where the permission satisfied are not from one role
                        if only partial, still show in the table widget, need combine to check
                        '''
                        relatedPerms[target].append(p)
            else:
                '''
                need to deal with permissions from recursive rules
                go through all parent path to root to gather all permissions
                '''
                rules = []
                parent = target
                while parent and parent!=PolicyContent.root_dir:
                    index = parent.rfind('/')
                    parent = parent[:index]
                    if parent in allPerms.keys():
                        for rule in allPerms[parent]:
                            if rule[4] and rule not in rules:
                                rules.append(rule)
                if parent in allPerms.keys():
                    for rule in allPerms[parent]:
                        if rule[4] and rule not in rules:
                            rules.append(rule)
                relatedPerms[target] = rules
            relatedPerms[target] = PermissionChecker.removePermissionUnnecessrayInInheritance(relatedPerms[target])
        else: # show all rules of the user with the permission filtered
            for k, perms in allPerms.iteritems():
                for p in perms:
                    if PermissionChecker.permissionAllowed(\
                            self.main.filter_perm,self.main.filter_permRelation, p.permset):
                        relatedPerms.setdefault(k,[])
                        if p not in relatedPerms[k]:
                            relatedPerms[k].append(p)
        self.main.visualizerTextEdit.fillinPermDetail(relatedPerms, self.model==self.UNIX)
        return relatedPerms

    def updateRBACRuleInfor_objView(self):
        allPerms = self.accessibleUserRBAC
        relatedPerms = {}
        self.target = self.selectedUser
        if self.target: #show related rules with the permission filtered
            target = self.target
            relatedPerms = {target: []}
            if target in allPerms.keys():
                for p in allPerms[target]:
                    if PermissionChecker.permissionAllowed(\
                            self.main.filter_perm,self.main.filter_permRelation, p.permset):
                        relatedPerms[target].append(p)
                    elif p.permset.issubset(self.main.filter_perm):
                        '''
                        For cases, where the permission satisfied are not from one role
                        if only partial, still show in the tablewidget, need combine to check
                        '''
                        relatedPerms[target].append(p)
                '''
                need to deal with permissions from recursive rules
                go through all parent path to root to gather all permissions
                '''
                rules = relatedPerms[target]
                parent = self.selectedObj.path
                while parent and parent!=PolicyContent.root_dir:
                    index = parent.rfind('/')
                    parent = parent[:index]
                    if parent in allPerms.keys():
                        for rule in allPerms[parent]:
                            if rule[4] and rule[0] == target and rule not in rules:
                                rules.append(rule)
            relatedPerms[target] = PermissionChecker.removePermissionUnnecessrayInInheritance(relatedPerms[target])
        else: # show all rules of the user with the permission filtered
            for k, perms in allPerms.iteritems():
                for p in perms:
                    if PermissionChecker.permissionAllowed(\
                            self.main.filter_perm,self.main.filter_permRelation, p.permset):
                        relatedPerms.setdefault(k,[]).append(p)
                    elif p.permset.issubset(self.main.filter_perm):
                        relatedPerms.setdefault(k,[]).append(p)
        self.main.visualizerTextEdit.fillinPermDetail(relatedPerms, self.model==self.UNIX)
        return relatedPerms
       
    def findDirecRolePath(self, rolesWithPerm, direcRoles):
        '''
        = Find the path between directly assigned and useful inherited role =
        - Use DFS start from the inherited roles up to a directly assigned role
        - Return a node list for a path
        '''
        def find_role_path(lastRoleNode, dirRoles, apath, path):
            if (not lastRoleNode.parents) and lastRoleNode.data not in dirRoles:
                return
            if lastRoleNode.data in dirRoles:
                apath.append(lastRoleNode)
#                 print 'apath', '<-'.join(i.data for i in apath)
                rapath = apath[::-1]
                if rapath[0] not in path.keys():
                    path[rapath[0]]= [rapath[1:]]
                else:
                    path[rapath[0]].append(rapath[1:])
                apath.pop()
                return
            apath.append(lastRoleNode)
            for p in lastRoleNode.parents:
                find_role_path(p, dirRoles, apath, path)
            apath.pop()
        
        path = {} # key: direcRole, value: a list of nodes along the path
        for r in rolesWithPerm:
            apath = []
            rn = self.scene.roleHier.data2RoleNode[r]
            find_role_path(rn, direcRoles, apath, path)
#         for k, p in path.iteritems():
#             print k.data+":"
#             for pp in p:
#                 print '\t'+'->'.join(i.data for i in pp)
        return path
    
    def highlightRoleInhPathEdges(self):
        path = self.rbac_pathDict
        # highlight the edges in the path
        for dr, p in path.iteritems():
            paths = path[dr]
            for p in paths:
                startNode = dr
                for n in p:
                    endNode = n
                    e = startNode.edges[endNode]
                    e.view.highlighted = True
                    startNode = endNode
                    
    def getRoleInfoForAnimation_userView(self):
        target = self.target.path
        perms = self.rbac_rules_withfilter[target]
        direcRolePerm = {}
        inhRolePerm = {}
        '''highlight the roles that can be used to access the target object'''
        for p in perms:
            self.scene.roleHier.data2RoleNode[p.role].view.roleAccess = 1
            if p.original:
                direcRolePerm[p.role] = ObjPerm(p.obj, p.permset)
            else:
                inhRolePerm[p.role] = ObjPerm(p.obj, p.permset)
        return direcRolePerm, inhRolePerm
        
    def getRoleInfoForAnimation_objView(self):
        self.resetRoleHighlight()
        self.resetRoleEdges()
        target = self.target
        perms = self.rbac_rules_withfilter[target]
        direcRolePerm = {}
        inhRolePerm = {}
        '''highlight the roles that can be used to access the target object'''
        for p in perms:
            self.scene.roleHier.data2RoleNode[p.role].view.roleAccess = 1
            if p.original:
                self.scene.roleHier.data2RoleNode[p.role].view.highlight = True
                direcRolePerm[p.role] = UserPerm(p.user, p.permset)
            else:
                self.scene.roleHier.data2RoleNode[p.role].view.inhHighlighted = True
                inhRolePerm[p.role] = UserPerm(p.user, p.permset)
        return direcRolePerm, inhRolePerm
    
    def RBAC_Animation(self):
        animOn = self.main.animSwitch.isChecked()
        if self.perspective == self.USERVIEW:
            if self.playStep == 0:
                self.computeRBACPermInfo_userView()
            if not self.target: return
            if animOn:
                self.visRBACAnim_userView()
                self.highlightRBACRuleTable_userView()
            else:
                self.visRBACNoAnim()
        elif self.perspective == self.OBJVIEW:
            if self.playStep==0:
                self.computeRBACPermInfo_objView()
            if not self.target: return
            if animOn:
                self.visRBACAnim_objView()
                self.highlightRBACRuleTable_userView()
            else:
                self.visRBACNoAnim()
        self.updateScenes()
            
    def updateScenes(self):
        self.scene.update()
        self.scene_obj.update()
#         QCoreApplication.processEvents()
        
    '''
    =Show the necessary visual effects for RBAC with no animation=
    1. Red frame around all roles that have the set permission to the target
    2. Red path between Directly assigned roles and the roles in red frames that it inherited from
    3. Show related rules
    Show message step by step:
        - All directly assigned roles to the user
        - The useful roles directly assigned to the user (red framed)
        - The useful roles 

    4. Show message including:
        - Roles (if inherited by a directly assigned role, indicate "inherited by roleX") used to access the object
    '''
    def visRBACNoAnim(self):
        '''update message'''
        perms = set()
        for rules in self.rbac_rules_withfilter.values():
            for rule in rules:
                perms = perms.union(rule.permset)
        suff = PermissionChecker.permissionAllowed(self.main.filter_perm, \
                                                   self.main.filter_permRelation, perms)
        rolepaths = list(self.rbac_direcRolePerm)
        dirroles = [ k.data for k in self.rbac_pathDict.keys()]+(self.rbac_direcRolePerm.keys())
        for s, paths in self.rbac_pathDict.iteritems():
            for p in paths:
                rolepaths.append(s.data+'->'+'->'.join(n.data for n in p))
        if perms and suff:
            self.scene_obj.msg = 'User "%s" is assigned to role (%s), and obtained "%s" access through (%s).'%(self.selectedUser,\
                                                            ', '.join(set(dirroles)),\
                                                            ','.join(perms),\
                                                            '; '.join(rolepaths))
        elif perms and not suff:
            self.scene_obj.msg = 'User "%s" is assigned to role (%s), and obtained "%s" access through (%s).'\
                                %(self.selectedUser,\
                                ', '.join(set(dirroles)),\
                                ','.join(perms),\
                                '; '.join(rolepaths)),\
                                'This does not satisfy the set permission.'
        else:
            self.scene_obj.msg = 'User "%s" has no access to the object.'%self.selectedUser

    def highlightRBACRuleTable_userView(self):
        table = self.main.visualizerTextEdit
        table.selectionModel().clearSelection()
        table.setSelectionMode(QAbstractItemView.MultiSelection)
        if self.playStep == 1:
            lastCol = table.columnCount()-1
            for row in xrange(table.rowCount()):
                table_item = table.item(row, lastCol)
                if str(table_item.text())=='Original':
                    self.main.visualizerTextEdit.selectRow(row)
        elif self.playStep == 2:
            lastCol = table.columnCount()-1
            for row in xrange(table.rowCount()):
                table_item = table.item(row, lastCol)
                if str(table_item.text())=='Inherited':
                    self.main.visualizerTextEdit.selectRow(row)
        table.repaint()
        table.setSelectionMode(QAbstractItemView.ExtendedSelection)
    '''
    =Show the necessary visual effects for RBAC with animation=
    1. Red frame around all roles that have the set permission to the target
    2. Red path between Directly assigned roles and the roles in red frames that it inherited from
    3. Show related rules
    4. Show message step by step:
        - Step 0:  All directly assigned roles to the user (roles in color)
        - Step 1: The useful roles directly assigned to the user (red framed)
            with permission to the target
        - Step 2: The useful roles inherited by the direct roles (red edges)
            with permission to the target
        - Step 3: Show the overall permission to the target
    '''  
    def visRBACAnim_userView(self):
        target = self.target
        if self.perspective==self.USERVIEW:
            target = self.target.path  
        if self.playStep == 0:
            self.resetRoleHighlight()
            self.resetRoleAccess()
#             roles = self.rbac_direcRolePerm.keys()
#             for r in roles:
#                 self.scene.roleHier.data2RoleNode[r].view.highlighted = 1
            roles = self.rbac_roles_direct
            for r in roles:
                self.scene.roleHier.data2RoleNode[r].view.highlighted = 1
            self.scene_obj.msg = 'User "%s" is assigned to the colored role(s) "%s".'\
                            %(self.selectedUser, ', '.join(self.rbac_roles_direct))
                            
        elif self.playStep == 1:
            perms = set()
            roles = self.rbac_direcRolePerm.keys()
            for r in roles:
                self.scene.roleHier.data2RoleNode[r].view.roleAccess = 1
            
            for rule in self.rbac_rules_withfilter[target]:
                if rule.original:
                    perms = perms.union(rule.permset)
            self.rbac_accuPerm = self.rbac_accuPerm.union(perms)
            if perms:
                self.scene_obj.msg = 'Rules below show that the user obtains "%s" permissions through the role(s) in red frames.'\
                                %(', '.join(perms))
                                
            else:
                self.scene_obj.msg = 'However, there is no access to the object from directly assigned roles.'
        elif self.playStep == 2:
            for r in self.rbac_inhRolePerm.keys():
                n = self.scene.roleHier.data2RoleNode[r].view
                n.inhHighlighted = True
                n.roleAccess = True
            self.highlightRoleInhPathEdges()
            
            perms = set()
            for rule in self.rbac_rules_withfilter[target]:
                if not rule.original:
                    perms = perms.union(rule.permset)
            self.rbac_accuPerm = self.rbac_accuPerm.union(perms)
            rolepaths = []
            for s, paths in self.rbac_pathDict.iteritems():
                for p in paths:
                    rolepaths.append(s.data+'->'+'->'.join(n.data for n in p))
                
            if perms:
                self.scene_obj.msg = 'User "%s" obtained "%s" access to the object through role inheritance (%s).'\
                            %(self.selectedUser, ', '.join(perms), '; '.join(rolepaths))
            else:
                self.scene_obj.msg = 'There is no (additional) access to the object obtained through role inheritance.'
        elif self.playStep == 3:
            if self.rbac_accuPerm:
                self.scene_obj.msg = 'In summary, user "%s" has "%s" access to the object.'\
                                    %(self.selectedUser, ', '.join(self.rbac_accuPerm))
            else:
                self.scene_obj.msg = 'In summary, user "%s" has no access to the object.'%self.selectedUser
    
    def visRBACAnim_objView(self):
        target = self.target
        if self.playStep == 0:
            self.resetRoleHighlight()
            self.resetRoleAccess()
            roles = self.rbac_roles_direct
            for r in roles:
                self.scene.roleHier.data2RoleNode[r].view.highlighted = 1
            self.scene_obj.msg = 'User "%s" is assigned to the colored role(s) "%s".'\
                            %(self.selectedUser, ', '.join(self.rbac_roles_direct))
        elif self.playStep == 1:
            roles = self.rbac_direcRolePerm.keys()
            for r in roles:
                self.scene.roleHier.data2RoleNode[r].view.roleAccess = 1
            perms = set()
            for rule in self.rbac_rules_withfilter[target]:
                if rule.original:
                    perms = perms.union(rule.permset)
            self.rbac_accuPerm = self.rbac_accuPerm.union(perms)
            if perms:
                self.scene_obj.msg = 'Rules below show that the user obtains "%s" permissions through the role(s) in red frames.'\
                                %(', '.join(perms))
            else:
                self.scene_obj.msg = 'However, there is no access to the object from directly assigned roles.'
        elif self.playStep == 2:
            for r in self.rbac_inhRolePerm.keys():
                n = self.scene.roleHier.data2RoleNode[r].view
                n.inhHighlighted = True
                n.roleAccess = 1
            self.highlightRoleInhPathEdges()
            
            perms = set()
            for rule in self.rbac_rules_withfilter[target]:
                if not rule.original:
                    perms = perms.union(rule.permset)
            self.rbac_accuPerm = self.rbac_accuPerm.union(perms)
            rolepaths = []
            for s, paths in self.rbac_pathDict.iteritems():
                for p in paths:
                    rolepaths.append(s.data+'->'+'->'.join(n.data for n in p))
                
            if perms:
                self.scene_obj.msg = 'User "%s" obtained "%s" access to the object through role inheritance (%s).'\
                            %(self.selectedUser, ', '.join(perms), '; '.join(rolepaths))
            else:
                self.scene_obj.msg = 'There is no access to the object obtained through role inheritance.'
        elif self.playStep == 3:
            if self.rbac_accuPerm:
                self.scene_obj.msg = 'In summary, user "%s" has "%s" access to the object.'\
                                    %(self.selectedUser, ', '.join(self.rbac_accuPerm))
            else:
                self.scene_obj.msg = 'In summary, user "%s" has no access to the object.'%self.selectedUser
    
    def checkChildrenAccessibility(self):
        '''sort the nodes to draw from the leaf to the root level by level'''
        nodelist = list(self.scene_obj.layout.nodesDraw2)
        def levels(x,y):
            return x.path.count('/')>y.path.count('/')
        nodelist.sort(reverse = True, key=lambda x: x.path.count('/'))
        for n in nodelist:
            if not n.children: continue
            accessibleCount = 0
            descAccessible = False
            for nn in n.children:
                if nn.accessible: accessibleCount+=1
                if nn.childrenAccessible>=0: descAccessible = True
            if accessibleCount==len(n.children): n.childrenAccessible = 1
            elif accessibleCount==0: n.childrenAccessible = -1
            else: n.childrenAccessible = 0
            '''if there is a node in the subgraph rooted at n, set the fill color to light blue'''
            if n.childrenAccessible==-1 and descAccessible: n.childrenAccessible = 0
            if not self.init: continue
            if n.expanded and (n.childrenAccessible==-1 or n.childrenAccessible==1):
                self.scene_obj.layout.collapseNode(n, clickedNode=True, showAnim=False)
            if not n.expanded and n.childrenAccessible==0:
                self.scene_obj.layout.expandNode(n, clickedNode=True, showAnim=False)
                
    def getObjectsRecursive(self, ruleRecur):
        self.allAccessibleObjects = set()
#         for n in self.scene_obj.layout.nodesDraw2:
#             '''recursive'''
#             for r in ruleRecur:
#                 if r.obj in n.path:
#                     print r.obj, n.path
#                     n.accessible = True
#                     self.allAccessibleObjects.add(n.path)
#                     break
        for n in self.scene_obj.layout.nodesDraw2:
            '''recursive'''
            for r in ruleRecur:
                if r.obj in n.path:
                    n.accessible = True
                    self.allAccessibleObjects.add(n.path)
                    # if there is some permission that does not exist from the non-recursive assignments
                    permset = set()
                    if n.path not in self.accessibleObjectsRBAC.keys(): break
                    for rule in self.accessibleObjectsRBAC[n.path]:
                        if rule.user==self.selectedUser:
                            permset = permset.union(rule.permset)
                    if r.permset-permset:
                        self.accessibleObjectsRBAC[n.path].append(r)
                    break
                
    def getObjectsChildren(self, ruleRecur):
        '''get the children accessibility ready for future expansion'''
        for n in self.scene_obj.layout.nodesDraw2:
            if (not n.children) or (n.children and n.expanded): continue
            for nn in n.children:
                nn.accessible = False
                permset = set()
                for r in ruleRecur:
                    if r.obj in nn.path and r.user==self.selectedUser:
                        permset = permset.union(r.permset)
                if permset and PermissionChecker.permissionAllowed(\
                        self.main.filter_perm, self.main.filter_permRelation,\
                        permset):
                        nn.accessible = True
                if nn.accessible: continue
                obj = nn.path
                if obj in self.accessibleObjectsRBAC.keys():
                    permset = set()
                    for rule in self.accessibleObjectsRBAC[obj]:
                        if rule.user==self.selectedUser:
                            permset = permset.union(rule.permset)
                    if permset and PermissionChecker.permissionAllowed(\
                        self.main.filter_perm, self.main.filter_permRelation,\
                        permset):
                        nn.accessible = True
    
    def getUsersRecursive(self, ruleRecur):
        '''
        for the users that are assigned to roles that have
        recursive permissions to the ancestors of the object
        '''
        users = set()
        obj = self.selectedObj.path
        for rule in ruleRecur:
            if (rule.obj in obj) and \
                PermissionChecker.permissionAllowed(\
                self.main.filter_perm, self.main.filter_permRelation, rule.permset):
                users.add(rule.user)
        return users
            
    def updateAccessibleNodes(self, usePermFilter=True):
        '''grey out unaccessible nodes'''
        dirNodes = self.scene_obj.layout.nodesDraw2#self.scene_obj.graph.nodeDict.values()
        for d in dirNodes: 
            d.accessible = False
            d.childrenAccessible = -1
        ruleRecur = []
        for obj in self.accessibleObjectsRBAC.keys():
            if usePermFilter:
                permset = set()
                for rule in self.accessibleObjectsRBAC[obj]:
                    if rule.user==self.selectedUser:
                        permset = permset.union(rule.permset)
                if permset and PermissionChecker.permissionAllowed(\
                    self.main.filter_perm, self.main.filter_permRelation,\
                    permset):
                    self.scene_obj.graph.nodeDict[obj].accessible = True
                    self.allAccessibleObjects.add(obj)
                    if rule.recursive: ruleRecur.append(rule)
            else:
                self.scene_obj.graph.nodeDict[obj].accessible = True
                self.allAccessibleObjects.add(obj)
                for rule in self.accessibleObjectsRBAC[obj]:
                    if rule.recursive: ruleRecur.append(rule)
        '''retrieve the objects accessible using recursive rules'''
        self.getObjectsRecursive(ruleRecur)
        self.getObjectsChildren(ruleRecur)
        '''update children accessibility'''
        self.checkChildrenAccessibility()
#         print self.accessibleObjectsRBAC['/Users/manw/Documents/interview/cv_temp/cv.pdf']
        
    def updateUsersAccessible(self, usePermFilter=True):
        users2Highlight = set()
        allusers = self.accessibleUserRBAC.keys()
        if usePermFilter:
            for u in allusers:
                permset = set()
                for rule in self.accessibleUserRBAC[u]:
                    if rule.obj == self.selectedObj.path:
                        permset = permset.union(rule.permset)
                if permset and PermissionChecker.permissionAllowed(\
                    self.main.filter_perm, self.main.filter_permRelation,\
                    permset):
                    users2Highlight.add(u)
        else:
            users2Highlight = allusers
        '''show the users that can access the object with the permission in bold text'''
        for i in xrange(self.main.userListWidget.count()):
            itemm = self.main.userListWidget.item(i)
            self.main.userListWidget.setItemTextBoldProp(itemm, str(itemm.text()) in users2Highlight)

    '''
    Get all rule info regardless of filter perms (role highlight updated at the choice of user)
    User View: Has all information needed for one user, updated when user selection changes
    Object View: Has all information needed for one object, updated when a user is selected
    '''    
    def initRBAC_objView(self):
        self.target= None
        obj = self.selectedObj.path
        self.accessibleUserRBAC = {}
        '''
        Find all roles related and the role inheritance paths:
        3 cases of roles involved:
        - Original:
        1. Direct access to that exact object
        2. Through recursive permissions to the ancestor of the object
        - Inheritance:
        3. Direct assignment to roles that inherit the roles from case 1 and 2
        '''
        roleset = set()
        '''case 1 and 2'''
        for r, perminfo in PolicyContent.rbac_role_res_mat.iteritems():
            for objj, permset in perminfo.iteritems():
                if (objj == obj) or (objj in obj and permset.recur_perms):
                    rn = self.scene.roleHier.data2RoleNode[r]
                    roleset.add(rn)
                    rules = []
                    if permset.recur_perms:
                        rules.extend(PermissionChecker.retrieveRBACUserInfo(obj, r, permset.recur_perms, True, True))
                    if permset.nonrecur_perms:
                        rules.extend(PermissionChecker.retrieveRBACUserInfo(obj, r, permset.nonrecur_perms, False, True))
                    for rule in rules:
                        self.accessibleUserRBAC.setdefault(rule.user, []).append(rule)
        self.directRoles = roleset
        '''case 3: construct inherited role rules and role-inheritance-path'''
        self.inheritDirectRolePairs = {}
        for ir in self.directRoles:
            for rn in self.scene.roleHier.findAllParents_BFS(ir):
                '''find the users that are assigned to these parent roles'''
                r = rn.data
                for u, rlist in PolicyContent.rbac_usr_role_mat.iteritems():
                    if r in rlist:
                        rules = PermissionChecker.retrieveRBACPermInfo(u, ir.data, False, obj)
                        for rule in rules:
                            self.accessibleUserRBAC.setdefault(rule.user, []).append(rule)
        '''highlight related users'''
        self.updateUsersAccessible()
        '''fill rules'''
        self.main.visualizerTextEdit.fillinPermDetail(self.accessibleUserRBAC, self.model==self.UNIX)
        self.updateScenes()
        
    def getUserRolePermForTextEdit(self, user):
        '''find all roles related to the user'''
        roleset = set()
        ignoreR = (self.perspective == self.ROLEVIEW)
        for r in PolicyContent.rbac_usr_role_mat[user]:
            roleset.add(self.scene.roleHier.data2RoleNode[r])
        self.directRoles = roleset
        rules = []
        for r in roleset:
            r.view.highlighted = True
            rules.extend(PermissionChecker.retrieveRBACPermInfo(user, r, True, ignoreRole=ignoreR))
            for ir in self.scene.roleHier.findAllChildren_BFS(r):
                ir.view.inhHighlighted = True
                rules.extend(PermissionChecker.retrieveRBACPermInfo(user, ir, False, ignoreRole=ignoreR))
        for rule in rules:
            if rule.obj not in self.accessibleObjectsRBAC.keys():
                self.accessibleObjectsRBAC[rule.obj] = [rule]
            else:
                if rule not in self.accessibleObjectsRBAC[rule.obj]:
                    self.accessibleObjectsRBAC[rule.obj].append(rule)
#         import pprint
#         pprint.pprint(rules)
#         print '-'*10
#         pprint.pprint(self.accessibleObjectsRBAC)
#         '''fill rules'''
#         self.main.visualizerTextEdit.fillinPermDetail(self.accessibleObjectsRBAC, self.model==self.UNIX)
        
    def initRBAC_userView(self):
#         self.target = None
        '''contain text instead of graphical items'''
        self.accessibleObjectsRBAC = {} # only contain the objects in the rules
        self.allAccessibleObjects = set() # contain objects in rules and objects accessible through recursive assignment
        '''highlighted related roles'''
        self.resetRoleHighlight()
        self.getUserRolePermForTextEdit(self.selectedUser)
        self.updateAccessibleNodes(False)
        self.updateScenes()
